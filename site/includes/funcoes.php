<?php
function GerasenhaSegura($senha){
	$chave = "desenv#2012";
	$chave = md5($chave);
	$senha_escape = hash("sha512", sha1(hash("sha512", addslashes($chave.$senha.$chave))));
	return $senha_escape;
return $senha_escape;
}
function valida_arquivo($arq, $tipo) {
  if($tipo == "arq") {
	if (preg_match("/\.(doc|docx|ppt|pptx|pps|ppsx|pot|xls|xlsx|rar|zip|pdf|gif|bmp|png|jpg|jpeg|swf|txt|psd|cdr){1}$/i", $arq, $ext))
	  return true;
  } else
  if($tipo == "img") {
	if (preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $arq, $ext))
	  return true;
  } else
  if($tipo == "vid") {
	if (preg_match("/\.(flv|mpeg|avi|mp4|mkv|wmv){1}$/i", $arq, $ext))
	  return true;
  }else
  if($tipo == "all") {
	if (preg_match("/\.(doc|docx|ppt|pptx|pps|ppsx|pot|xls|xlsx|rar|zip|pdf|gif|bmp|png|jpg|jpeg|swf|txt|psd|cdr|gif|bmp|png|jpg|jpeg|flv|mpeg|avi|mp4|mkv|wmv){1}$/i", $arq, $ext))
	  return true;
  }
  return false;
}

function cortar($frase, $quantidade) {
    $tamanho = strlen($frase);
    if($tamanho > $quantidade)
        $frase = substr_replace($frase, "...", $quantidade, $tamanho - $quantidade);
    return $frase;
}
function trataYoutube($link){
	$tags = $link;
	$termo = 'watch';

	$pattern = '/' . $termo . '/';//Padrão a ser encontrado na string $tags

	if (preg_match($pattern, $tags)) {
		$link = explode("v=",$tags);
		return $link[1];
	} else {
		$link = explode("embed/",$tags);
		return $link[1];
	}
}

function cortatTXT($qtdPalavras,$palavra,$strComplete = null){

		$arrayPalavras = explode(" ",$palavra);
		if( count($arrayPalavras) < $qtdPalavras )
			$qtdPalavras = count($arrayPalavras);
		$palavraFinal = "";
		for( $i = 0; $i < $qtdPalavras; $i++ ){
			$palavraFinal .= $arrayPalavras[$i]." ";
		}
		$palavraFinal .= $strComplete;
		return $palavraFinal;

}

function cortaTexto($cont,$qtd) {
	  $list = explode(" ",$cont);
	  $concat = "";
		for( $i = 0; $i < $qtd; $i++ ){
			$concat .= $list[$i]." ";
		}
		if(count($list) > $qtd)
		   $concat.= "...";

	  return $concat;
}

function geraUrlLimpa($frase){
	$frase = strtolower($frase);
	  //REMOVE TODOS OS CARACTERES ESPECIAIS, ACENTOS E ACRESCENTA .HTM EM UMA STRING
	$a = array('À','Á','Â','Ã','Ä','Å','Æ','Ç','È','É','Ê','Ë','Ì','Í','Î','Ï','Ð','Ñ','Ò','Ó','Ô','Õ','Ö','Ø','Ù','Ú','Û','Ü','Ý','ß','à','á','â','ã','ä','å','æ','ç','è','é','ê','ë','ì','í','î','ï','ñ','ò','ó','ô','õ','ö','ø','ù','ú','û','ü','ý','ÿ','A','a','A','a','A','a','C','c','C','c','C','c','C','c','D','d','Ð','d','E','e','E','e','E','e','E','e','E','e','G','g','G','g','G','g','G','g','H','h','H','h','I','i','I','i','I','i','I','i','I','i','?','?','J','j','K','k','L','l','L','l','L','l','?','?','L','l','N','n','N','n','N','n','?','O','o','O','o','O','o','Œ','œ','R','r','R','r','R','r','S','s','S','s','S','s','Š','š','T','t','T','t','T','t','U','u','U','u','U','u','U','u','U','u','U','u','W','w','Y','y','Ÿ','Z','z','Z','z','Ž','ž','?','ƒ','O','o','U','u','A','a','I','i','O','o','U','u','U','u','U','u','U','u','U','u','?','?','?','?','?','?','/',',','(',')','?','"');
	$b = array('a','a','a','a','a','a','AE','C','E','E','E','E','I','I','I','I','D','N','O','O','O','O','O','O','U','U','U','U','Y','s','a','a','a','a','a','a','ae','c','e','e','e','e','i','i','i','i','n','o','o','o','o','o','o','u','u','u','u','y','y','A','a','A','a','A','a','C','c','C','c','C','c','C','c','D','d','D','d','E','e','E','e','E','e','E','e','E','e','G','g','G','g','G','g','G','g','H','h','H','h','I','i','I','i','I','i','I','i','I','i','','','J','j','K','k','L','l','L','l','L','l','L','l','l','l','N','n','N','n','N','n','n','O','o','O','o','O','o','OE','oe','R','r','R','r','R','r','S','s','S','s','S','s','S','s','T','t','T','t','T','t','U','u','U','u','U','u','U','u','U','u','U','u','W','w','Y','y','Y','Z','z','Z','z','Z','z','s','f','O','o','U','u','A','a','I','i','O','o','U','u','U','u','U','u','U','u','U','u','A','a','','','O','o','-','-','-','-','','');
	$string = utf8_decode(strtolower(preg_replace(array('/[^a-zA-Z0-9 -]/','/[ -]+/','/^-|-$/'),array('','-',''),str_replace($a,$b,$frase))));

	return str_replace(
        array('ccedil','aacute','acirc','agrave','atilde','auml','eacute','ecirc','egrave','euml','iacute','icirc','igrave','oacute','ocirc','ograve','otilde','ouml','uacute','ucirc','ugrave','uuml','ntilde','quot'),
        array('c'     ,'a'     ,'a'    ,  'a'   ,   'a'  , 'a'  ,   'e'  ,  'e'  ,  'e'   ,  'e' ,   'i'  ,   'i' ,  'i'   ,  'o'   , 'o'   , 'o'    ,  'o'   , 'o'  ,  'u'   , 'u'   ,  'u'   ,   'u',   'n'  ,  '"' ),
        strtolower($string)
    );
}

  function geraUrlLimpaBusca($frase){
	//REMOVE TODOS OS CARACTERES ESPECIAIS, ACENTOS E ACRESCENTA .HTM EM UMA STRING
	$frase = ereg_replace("[^a-zA-Z0-9+-.]", "", strtr($frase, "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ ", "aaaaeeiooouucAAAAEEIOOOUUC-"));
	$fraseParc = explode("+.",$frase);

	return strtolower($frase);
}

function paginacaoGeralBlog($totalPaginas, $pagina, $limite, $ordemCampo, $busca, $nomePage) {
	
	if($totalPaginas > 1)
	{
	
		$menos = $pagina - 1;
		$mais = $pagina + 1;
	
		if($totalPaginas > 1 )
		{
	
				 echo '
										  <ul class="pagination">
										  ';
	
			if($menos == 0) $menos =1;
	
			if (($pagina - 4) < 1 )
			{
				$anterior = 1;
			}
	
			else
			{
				$anterior = $pagina - 4;
			}
	
			if (($pagina + 4) > $totalPaginas)
			{
				$posterior = $totalPaginas;
			}
	
			else
			{
				$posterior = $pagina + 4;
			}
	
	
			for($i = $anterior; $i <= $posterior; $i++)
			{
				if($i != $pagina)
				{
					echo "<li class='page-item'><a class='page-link' href=\"".$busca.$i."/".$nomePage."\">".$i."</a></li>";
				}
				else
				{
					echo "<li class=\"active page-item\"><a class='page-link' href=\"".$busca.$i."/".$nomePage."\">".$i."</a></li>";
				}
			}
				echo '</ul>';
		}
	
	}
	
}

function paginacaoGeral($totalPaginas, $pagina, $limite, $ordemCampo, $busca, $nomePage) {
	if($totalPaginas > 1) {
  
	  $menos = $pagina - 1;
	  $mais = $pagina + 1;
  
	  if($totalPaginas > 1 ) {
	  echo '<ul class="pagination">';
		//   if($menos > 0){
		// 	  echo '<li class="page-item prev"><a class="page-link anterior" href="'.$busca.$menos.'/'.$nomePage. '?busca=' . $_GET['busca'] . '"\">Anterior</a></li>';
		//   }
		if($menos == 0) $menos =1;
		if (($pagina - 2) < 1 ) {
		  $anterior = 1;
		} else {
		  $anterior = $pagina - 2;
		}
  
		if (($pagina + 2) > $totalPaginas) {
		  $posterior = $totalPaginas;
		} else {
		  $posterior = $pagina + 2;
		}
  
  
		for($i = $anterior; $i <= $posterior; $i++) {
		   if($i != $pagina){
		  echo "<li class='page-item'><a class='page-link' href=\"".$busca.$i."/".$nomePage. "?busca=" . $_GET['busca'] . "\">".$i."</a></li>";
		   }else{
			  echo "<li class=\"page-item active\"><a class='page-link' href=\"".$busca.$i."/".$nomePage. "?busca=" . $_GET['busca'] ."\">".$i."</a></li>";
		   }
		}
		// if($mais <= $totalPaginas){
		//    echo ' <li class="page-item next"><a class="page-link Proxima" href="'.$busca.$mais.'/'.$nomePage. '?busca=' . $_GET['busca'] . '"\">Pr&oacute;xima</a> </li>';
		//   }
		  echo '</ul>
			  </span>';
		}
  
	  }
  
	}

function paginacaoGeralErro($totalPaginas, $pagina, $limite, $ordemCampo, $busca, $nomePage) {

	echo "<script language='JavaScript'>
			function SomenteNumero(e){
				var tecla=(window.event)?event.keyCode:e.which;
				var numero = document.getElementById('pg').value;
				var total = $totalPaginas;

				/*if(numero >= total){
					 return false;
				}else{
					return true;
				}*/

				if((tecla>47 && tecla<58)) {

					return true;
				}else{
					if (tecla==8 || tecla==0){
						 return true;
					}else{
						 return false;
					}
				}
			}

			function ActionEnter(e,totPag){
				var tecla = (window.event)?event.keyCode:e.which;
				var numPag = document.getElementById('pg').value;
				/*alert();*/
				if(numPag > totPag) numPag = totPag;
				if((tecla == 13)) {
					window.location.href='$busca'+numPag+'/$nomePage';
					return true;
				}else{
					return false;
				}
			}
	    </script>";

  if($totalPaginas > 1) {

	$menos = $pagina - 1;
	$mais = $pagina + 1;
	if($totalPaginas > 1 ) {
	  if($menos == 0) $menos =1;
	  if($pagina > $totalPaginas){ $pagina = $totalPaginas; $menos = $totalPaginas;}
		echo "<a class='prev block' href=\"".$busca.$menos."/".$nomePage."\" ><< Anterior</a>";
	 	echo "<div><input type='text' value='".$pagina."' id='pg' onKeyDown='ActionEnter(event,$totalPaginas)' onkeypress='return SomenteNumero(event)'/><p>de  ".$totalPaginas."</p></div>";

	  if (($pagina - 4) < 1 ) {
		$anterior = 1;
	  } else {
		$anterior = $pagina - 4;
	  }

	  if (($pagina + 4) > $totalPaginas) {
		$posterior = $totalPaginas;
	  } else {
		$posterior = $pagina + 4;
	  }

	  for($i = $anterior; $i <= $posterior; $i++) {
		if($i != $pagina) {
		 // echo "<a href=\"".$nomePage."?pag=".$i."\">".$i."</a> | ";
		} else {
		//  echo "<a href=\"".$nomePage."?pag=".$i."\"><strong>".$i."</strong></a> | ";
		}
	  }

	  if($mais <= $totalPaginas) {
		//echo "<a href=\"?pag=".$mais."&qtd=".$limite."&cmp=".$ordemCampo."&q=".$busca."\">Proxima &raquo;</a>";
		echo "<a href=\"".$busca.$mais."/".$nomePage."\" class='next'>Pr&oacute;ximo >></a>";

	  }
	}

  }
}


function formataData($datetime, $formato, $horaEx) {

		if($datetime == "") $datetime = "00/00/0000 00:00:00";
	    $separa = explode(" ",$datetime);
		$data = explode("-",$separa[0]);
		$hora = explode(":",$separa[1]);


    if($formato == "en") {
			$dataExt = $data[2]."-".Mes($data[1])."-".$data[0];
			if($horaEx == 1){
			   $dataExt .= " at ".$hora[0]."h".$hora[1];
			}elseif($horaEx == 2){
				 $dataExt .= " ".$hora[0].":".$hora[1].":".$hora[2];
			}
	} else {
	if($horaEx == 1){
		$dataExt = $data[2]."/".$data[1]."/".$data[0];
		$dataExt .= " &agrave;s ".$hora[0]."h".$hora[1];
		}
	if($horaEx == 2){
		$dataExt = $data[2]." de ".Mes($data[1])." de ".$data[0];
		}
	if($horaEx == 3){
		$dataExt = $data[2]."/".$data[1]."/".$data[0];
		}
	if($horaEx == 4){
		$dataExt = $data[2]." ".substr(Mes($data[1]),0,3)." ".$data[0];
		}
	if($horaEx == 5){
		$dataExt = "<div class='data'><p class='dia'>".$data[2]."</p><p class='mes'>".substr(Mes($data[1]),0,3)."</p></div>";
		}
	if($horaEx == 6){
		$dataExt = $data[2]."/".$data[1];
		}
	if($horaEx == 7){
		$dataExt = '<span>'.$data[2]."</span>".substr(Mes($data[1]),0,3);
		}
	if($horaEx == 8){
		$dataExt = substr(Mes($data[1]),0,3);
		}
	if($horaEx == 9){
		$dataExt = $data[2]." ".substr(Mes($data[1]),0,3);
	}
	if($horaEx == 10){
		$dataExt = $data[2];
	}
	if($horaEx == 11){
		$dataExt = Mes($data[1]).' '.$data[0];
	}
	if($horaEx == 12){
		$dataExt = $data[0];
	}

	}
    return $dataExt;
}


function Mes($mes){
              switch($mes){
                case '01': $Mes = 'Janeiro'; break;
                case '02' : $Mes ='Fevereiro'; break;
                case '03': $Mes ='Março'; break;
                case '04' : $Mes ='Abril'; break;
                case '05' : $Mes ='Maio'; break;
                case '06' : $Mes ='Junho'; break;
                case '07' : $Mes ='Julho'; break;
                case '08' : $Mes ='Agosto'; break;
                case '09' : $Mes ='Setembro'; break;
                case '10' : $Mes ='Outubro'; break;
                case '11' : $Mes ='Novembro'; break;
                case '12' : $Mes ='Dezembro'; break;
			  }

                return $Mes;
}

function siglaMes($mes){
	$sigla = array(
		'Jan' => 'Jan',
		'Feb' => 'Fev',
		'Mar' => 'Mar',
		'Apr' => 'Abr',
		'May' => 'Mai',
		'Jun' => 'Jun',
		'Jul' => 'Jul',
		'Aug' => 'Ago',
		'Sep' => 'Set',
		'Oct' => 'Out',
		'Nov' => 'Nov',
		'Dec' => 'Dez'
	);

	return $sigla[$mes];
}


function retornaDia($diaSemana){
	switch($diaSemana){
                case "7": $dia = 'DOMINGO'; break;
                case "1" : $dia = 'SEGUNDA'; break;
                case "2": $dia = 'TERÇA'; break;
                case "3" : $dia = 'QUARTA'; break;
                case "4" : $dia = 'QUINTA'; break;
                case "5" : $dia = 'SEXTA'; break;
                case "6" : $dia = 'SÁBADO'; break;
	}
	return $dia;
}

function download($arquivo){
	header("Content-type: application/save");
	header("Content-Disposition: attachment; filename=".$arquivo." ");
	header('Content-Disposition: attachment; filename="' . basename($arquivo) . '"');
	header('Expires: 0');
	header('Pragma: no-cache');
	readfile("/sgw/upload".$arquivo);
}


function download2($pasta,$idsite,$idconteudo,$nome_servidor,$nome_arquivo = NULL) {

	 /* header("Content-type: application/save");
	  header("Content-Disposition: attachment; filename=".$nome_arquivo." ");
	  header('Content-Disposition: attachment; filename="'. basename($nome_arquivo).'"');
	  header('Expires: 0');
	  header('Pragma: no-cache');*/
	 echo $_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."storage".DIRECTORY_SEPARATOR.$pasta.DIRECTORY_SEPARATOR.$idsite.DIRECTORY_SEPARATOR.$idconteudo.DIRECTORY_SEPARATOR.$nome_servidor;

}

function leitura($conteudo, $midia,$pasta,$site,$classe = "box-img"){

	$conteudo['texto'] = str_replace('</p>','<br>',$conteudo['texto']);
	$conteudo['texto'] = str_replace('<p>','',$conteudo['texto']);

	//$html = "<h2>".utf8_decode($conteudo['nome'])."<h2>";
	//if($conteudo['resumo'])$html .= "<div class='resumo'>".utf8_decode($conteudo['resumo'])."</div>";
	//if($conteudo['autor'])$html .= "<p class='autor'>".utf8_decode($conteudo['autor'])."</p>";

	//$html .= "<div class='toolbar'></div>";

	if(!empty($conteudo['video'])){
		$links = explode("v=",$conteudo['video']);
		$link = $links[1];
		if(!$link){
			$links = explode("/",$conteudo['video']);
			$link = $links[4];
		}
	}

	$html = "<div class='conteudo_html'>";
	$html .= $conteudo['texto'];
	if($conteudo['video']){
		$html .= "<div class='video ".$classe."'><iframe width='100%' height='411' src='https://www.youtube.com/embed/".$link."' frameborder='0' allowfullscreen></iframe></div>";
	}
	$html .="</div>";

	if (!empty($variavel_T)) {
		$html = str_replace($variavel_T,"<div class='foto1 ".$classe."'>2".$img.$legenda."</div>",$html);
	}

	if(is_array($midia)){
		foreach($midia as $ln){
			$pos = strpos($conteudo['texto'], substr(strtoupper($ln['variavel']),0,-2));

			if($pos){
				$variavel   =  strtoupper($ln['variavel']);
				$variavel_T =  substr(strtoupper($ln['variavel']),0,-2)."-T]]";
				$variavel_D =  substr(strtoupper($ln['variavel']),0,-2)."-D]]";
				$variavel_E =  substr(strtoupper($ln['variavel']),0,-2)."-E]]";

				if($ln['legenda']){
					$legenda = "<p style='width:95%; word-wrap:break-word;'>".$ln['legenda'] ."</p>";
				}else{
					$legenda = "";
				}

				$img = ImagemLeitura($pasta ,$site,$ln['idconteudo'] , $ln['avatar_servidor'], $ln['avatar_nome'], $ln['legenda']);

				if(arquivos($ln['avatar_servidor'])){
					$html = str_replace($variavel,$img,$html);
				}else{
					$html = str_replace($variavel,"<div class='foto1 ".$classe."'>".$img.$legenda."</div>",$html);
				}
				$html = str_replace($variavel_T,"<div style='width:100%;' class='foto1 ".$classe."'>".$img.$legenda."</div>",$html);
				$html = str_replace($variavel_E,"<div style='float:left; margin:10px; width:320px; height:auto;' class='foto2 ".$classe."'>".$img.$legenda."</div>",$html);
				$html = str_replace($variavel_D,"<div style='float:right; margin:10px; width:320px; height:auto;' class='foto3 ".$classe."'>".$img.$legenda."</div>",$html);
			}
		}
	}

	return $html;
}

function youtubeImage($url, $size = 'small'){
    $url = explode('v=',$url);
    $url = explode('&',$url[1]);
    $url = $size == 'small' ? ('http://i1.ytimg.com/vi/' . $url[0] . '/default.jpg') : ('http://img.youtube.com/vi/' . $url[0] . '/0.jpg');
	return $url;
}

/*function leitura($conteudo, $midia,$pasta,$site){


$links = explode("v=",$conteudo['link']);
$link = $links[1];
if(!$link){
	$links = explode("/",$conteudo['link']);
	$link = $links[4];
	}

if($conteudo['link'])$html .= "<div class='box-img'><iframe width='640' height='360' src='http://www.youtube.com/embed/".$link."' frameborder='0' allowfullscreen></iframe></div>";
$html .='<p>'.$conteudo['texto'].'</p>';


$html = str_replace($variavel_T,"<div class='box-img''>2".$img.$legenda."</div>",$html);


if(is_array($midia)){
	foreach($midia as $ln){

			$pos = strpos($conteudo['texto'], substr(strtoupper($ln['variavel']),0,-2));
			if($pos){
				$variavel   =  strtoupper($ln['variavel']);
				$variavel_T =  substr(strtoupper($ln['variavel']),0,-2)."-T]]";
				$variavel_D =  substr(strtoupper($ln['variavel']),0,-2)."-D]]";
				$variavel_E =  substr(strtoupper($ln['variavel']),0,-2)."-E]]";

				if($ln['legenda']){$legenda = "<p>".$ln['legenda'] ."</p>";}else{$legenda = "";}
				$img = ImagemLeitura($pasta ,$site,$ln['idconteudo'] , $ln['avatar_servidor'], $ln['avatar_nome'], $ln['legenda'],NULL,NULL);


			if(arquivos($ln['avatar_servidor'])){
				$html = str_replace($variavel,$img,$html);
			}else{
				$html = str_replace($variavel,"<div class='box-img'>".$img.$legenda."</div>",$html);
			}
				$html = str_replace($variavel_T,"<div class='box-img'>".$img.$legenda."</div>",$html);
				$html = str_replace($variavel_E,"<div class='box-img'>".$img.$legenda."</div>",$html);
				$html = str_replace($variavel_D,"<div class='box-img'>".$img.$legenda."</div>",$html);
		}
	}
}

return $html;
}*/
function arquivos($arquivo){
						$icones = array();
					  $icones['pdf']  = 'pdf.png';
					  $icones['doc']  = 'doc.png';
					  $icones['docx'] = 'docx.png';
					  $icones['xls']  = 'xls.png';
					  $icones['zip']  = 'zip.png';
					  $icones['rar']  = 'rar.png';
					  $icones['cdr']  = 'cdr.png';
					  $icones['mp3']  = 'mp3.png';
					  $icones['ac3']  = '';
					  $icones['ddt']  = '';
					  $icones['wma']  = 'wma.png';
					  $icones['wav']  = 'wav.png';
					  $icones['midi'] = 'midi.png';
					  $icones['csv']  = '';
					  $icones['xlsx'] = 'xlsx.png';
					  $icones['avi']  = '';
					  $icones['mp4']  = '';
					  $icones['3gp']  = '';
					  $icones['mov']  = 'mov.png';
					  $icones['rmvb'] = 'rmvb.png';
					  $icones['wmv']  = 'wmv.png';
					  $icones['mkv']  = '';
					  $icones['flv']  = '';
					  $icones['vob']  = '';
		  $extencao =  array_shift(array_reverse(explode(".",$arquivo)));
		  if(in_array($extencao,array_keys($icones))){
			  return $icones[$extencao];
		  }else{
		 	  return false;
		  }

}
function arquivosDownload($arquivo){
						$icones = array();
					  $icones['pdf']  = 'icone-pdf.png';
					  $icones['doc']  = 'icone-doc.png';
					  $icones['docx'] = 'icone-doc.png';
					  $icones['xls']  = 'icone-xls.png';
					  $icones['xlsx']  = 'icone-xls.png';
					  $icones['zip']  = 'icone-zip.png';
					  $icones['rar']  = 'cone-zip.png';
					  $icones['gif']  = 'icone-gif.png';
					  $icones['png']  = 'icone-png.png';
					  $icones['jpg']  = 'icone-jpg.png';
					  $icones['jpeg']  = 'icone-jpg.png';

          $arquivo = array_reverse(explode(".",$arquivo));
		  $extencao =  array_shift($arquivo);
		  if(in_array($extencao,array_keys($icones))){
			  return $icones[$extencao];
		  }else{
		 	  return false;
		  }

}

function ImagemListagem2($pasta ,$site,$id, $arquivo=false, $nome = false ,$altura = false, $largura = false,$classe=false){

			if($arquivo){
					$icone = arquivos($arquivo);
					if($icone){
						$arquivo = $icone;
						 if($altura || $largura) {
							 if($altura && !$largura) {
								echo "<img ".$classe." src='".IMAGEM_THUMB.'img'.DIRECTORY_SEPARATOR .$arquivo."&altura=".$altura."' alt='".$nome."'  >";
							 }elseif(!$altura && $largura){
								echo "<img ".$classe." src='".IMAGEM_THUMB.'img'.DIRECTORY_SEPARATOR .$arquivo."&largura=".$largura."' alt='".$nome."'>";
							 }else{
								echo "<img ".$classe." src='".IMAGEM_THUMB.'img'.DIRECTORY_SEPARATOR .$arquivo."&altura=".$altura."&largura=".$largura."' alt='".$nome."'>";
							 }
						 }else{
								echo "<img ".$classe." src='".IMAGEM_THUMB.'img'.DIRECTORY_SEPARATOR .$arquivo."' alt='".$nome."'>";
						 }
					}else{

						 if($altura || $largura) {
							 if($altura && !$largura) {
								echo "<img ".$classe." src='".IMAGEM_THUMB.$pasta.DIRECTORY_SEPARATOR.$site.DIRECTORY_SEPARATOR.$id.DIRECTORY_SEPARATOR .$arquivo."&altura=".$altura."' alt='".$nome."'>";
							 }elseif(!$altura && $largura){
								echo "<img ".$classe." src='".IMAGEM_THUMB.$pasta.DIRECTORY_SEPARATOR.$site.DIRECTORY_SEPARATOR.$id.DIRECTORY_SEPARATOR .$arquivo."&largura=".$largura."' alt='".$nome."'>";
							 }else{
								echo "<img ".$classe." src='".IMAGEM_THUMB.$pasta.DIRECTORY_SEPARATOR.$site.DIRECTORY_SEPARATOR.$id.DIRECTORY_SEPARATOR .$arquivo."&altura=".$altura."&largura=".$largura."' alt='".$nome."'>";
							 }
						 }else{
								echo "<img ".$classe." src='".IMAGEM.$pasta.DIRECTORY_SEPARATOR.$site.DIRECTORY_SEPARATOR.$id.DIRECTORY_SEPARATOR .$arquivo."' alt='".$nome."'>";
						 }
					}

					 }else{
						 $arquivo = 'sem_foto.jpg';
						 if($altura || $largura) {
							 if($altura && !$largura) {
								echo "<img ".$classe." src='".IMAGEM_THUMB.'img'.DIRECTORY_SEPARATOR .$arquivo."&altura=".$altura."' alt='".$nome."'>";
							 }elseif(!$altura && $largura){
								echo "<img ".$classe." src='".IMAGEM_THUMB.'img'.DIRECTORY_SEPARATOR .$arquivo."&largura=".$largura."' alt='".$nome."'>";
							 }else{
								echo "<img ".$classe." src='".IMAGEM_THUMB.'img'.DIRECTORY_SEPARATOR .$arquivo."&altura=".$altura."&largura=".$largura."' alt='".$nome."'>";
							 }
						 }else{
								echo "<img ".$classe." src='".IMAGEM_THUMB.'img'.DIRECTORY_SEPARATOR .$arquivo."' alt='".$nome."'>";
						 }
					}


 }




function ImagemLeitura($pasta ,$site,$id, $arquivo=false, $nome = false,$legenda = false ,$altura = false, $largura = false){

					if($arquivo){
						$icone = arquivos($arquivo);
					if($icone){
						$arquivo = $icone;

						if(!$legenda){$legenda = "Clique aqui para baixar o arquivo anexado";}

								$imagem = "<div class='download'>
												<img src='".IMAGEM."img".DIRECTORY_SEPARATOR .$arquivo."' alt='".$nome."' style='width:45;height:45;float:left;'>
												<span><a href='/download?pasta=conteudo&idsite=".$site."&idconteudo=". $id."&nome_servidor=". $arquivo."&nome_arquivo=".$nome."' target='_blank'>".$legenda.".</a></span>
											</div>";


					}else{

						 if($altura || $largura) {
							 if($altura && !$largura) {
								$imagem = "<img class='fotoNoticia margemFotoInterna' src='".IMAGEM_THUMB.$pasta.DIRECTORY_SEPARATOR.$site.DIRECTORY_SEPARATOR.$id.DIRECTORY_SEPARATOR .$arquivo."&altura=".$altura."' alt='".$nome."'>";
							 }elseif(!$altura && $largura){
								$imagem = "<img class='fotoNoticia margemFotoInterna' src='".IMAGEM_THUMB.$pasta.DIRECTORY_SEPARATOR.$site.DIRECTORY_SEPARATOR.$id.DIRECTORY_SEPARATOR .$arquivo."&largura=".$largura."' alt='".$nome."'>";
							 }else{
								$imagem = "<img class='fotoNoticia margemFotoInterna' src='".IMAGEM_THUMB.$pasta.DIRECTORY_SEPARATOR.$site.DIRECTORY_SEPARATOR.$id.DIRECTORY_SEPARATOR .$arquivo."&altura=".$altura."&largura=".$largura."' alt='".$nome."'>";
							 }
						 }else{
								$imagem = "<img class='fotoNoticia margemFotoInterna' src='".IMAGEM.$pasta.DIRECTORY_SEPARATOR.$site.DIRECTORY_SEPARATOR.$id.DIRECTORY_SEPARATOR .$arquivo."' alt='".$nome."'>";
						 }
					}

					 }else{
						 $arquivo = 'sem_foto.gif';
						 if($altura || $largura) {
							 if($altura && !$largura) {
								$imagem = "<img src='".IMAGEM_THUMB.'img'.DIRECTORY_SEPARATOR .$arquivo."&altura=".$altura."' alt='".$nome."'>";
							 }elseif(!$altura && $largura){
								$imagem = "<img src='".IMAGEM_THUMB.'img'.DIRECTORY_SEPARATOR .$arquivo."&largura=".$largura."' alt='".$nome."'>";
							 }else{
								$imagem = "<img src='".IMAGEM_THUMB.'img'.DIRECTORY_SEPARATOR .$arquivo."&altura=".$altura."&largura=".$largura."' alt='".$nome."'>";
							 }
						 }else{
								$imagem = "<img src='".IMAGEM_THUMB.'img'.DIRECTORY_SEPARATOR .$arquivo."' alt='".$nome."'>";
						 }
					}
				return $imagem;

 }


function EnviaEmail ($array){
$HTTP_POST_VARS = $array;
$recipient = $_POST["enviar_email"];

if(isset( $recipient ) && !empty( $recipient ) && is_array($HTTP_POST_VARS) ) {

	     // Cria��o do Destinat�rio
         if( !isset( $email ) || empty( $email ) ){
      	   $email = $recipient;
         }
         reset( $HTTP_POST_VARS );
         // Cria��o da Mensagem
         $mensagem = null;

         $mensagem = "<table width=\"450\"  border=\"0\" cellpadding=\"2\" cellspacing=\"1\">
					    <tr bgcolor=\"#778899\">
						  <td colspan=\"2\">
						    <font color=\"#FFFFFF\" face=\"trebuchet ms\" size=\"4\">&nbsp;&nbsp;contato</font>
					      </td>
					    </tr>
					";

         while(list($campo, $conteudo ) = each( $HTTP_POST_VARS ) )
         {
            $conteudo  = stripslashes( $conteudo );
			if(!($campo != 'recipient' xor $campo != 'redirect')){
			$mensagem .= "<tr bgcolor=\"#BEBEBE\">
				            <td width=\"131\" bgcolor=\"#E4E4E4\" valign=\"top\">
						      <div align=\"right\"><font color=\"#000000\" face=\"verdana\" size=\"2\">$campo:</font></div>
						    </td>
				            <td width=\"304\" bgcolor=\"#F4F4F4\">
							  <font color=\"#000000\" face=\"verdana\" size=\"2\">". htmlentities($conteudo) ."</font>
							</td>
			              </tr>	\n";
		      }
         }

		 $mensagem .= "<tr bgcolor=\"#708090\">
			             <td colspan=\"2\">
						   <div align=\"right\">
						     <a href=\"http://www.alfamaweb.com.br/\" target=\"_blank\">
							   <font color=\"#FFFFFF\" face=\"trebuchet ms\" size=\"1\">acesse: http://www.alfamaweb.com.br</font></a>
						   </div>
						 </td>
			           </tr>
		             </table>";

          // Cria��o do Assunto
         if( !isset( $assunto ) )
         {
            if( isset( $subject ) )
            {
               $assunto = $subject;
            }
            else
            {
               $assunto = "Contato!";
            }
         }

         // Redirecionamento
         if( isset( $redirect ) )
         {
            if( !strstr($redirect, "http://") )
            {
               $redirect = "http://".$redirect;
            }
         }
         else
         {
            $redirect = "http://mostraofertas.alfamaweb.com.br/";
         }
		 //email para o email
		 if($email == ""){
		 	$email = $renasc;
		 }else{
		 	$email = $email;
		 }
         // Enfim, envia o e-mail
         $cabecalho  = "From: <".$email.">\n";
         $cabecalho .= "AlfamaWeb: http://www.alfamaweb.com.br/\n\n";

		 $arquivo = isset($_FILES["arquivo"]) ? $_FILES["arquivo"] : FALSE;

		 if(file_exists($arquivo["tmp_name"]) and !empty($arquivo)){
			  $fp = fopen($_FILES["arquivo"]["tmp_name"],"rb");
			  $anexo = fread($fp,filesize($_FILES["arquivo"]["tmp_name"]));
			  $anexo = base64_encode($anexo);
			  fclose($fp);
			  $anexo = chunk_split($anexo);

			  //Anexo
			  $boundary = "XYZ-" . date("dmYis") . "-ZYX";

			  $mens = "--$boundary\n";
			  $mens .= "Content-Transfer-Encoding: 8bits\n";
			  $mens .= "Content-Type: text/html; charset=\"ISO-8859-1\"\n\n"; //plain
			  $mens .= "$mensagem\n";
			  $mens .= "--$boundary\n";
			  $mens .= "Content-Type: ".$arquivo["type"]."\n";
			  $mens .= "Content-Disposition: attachment; filename=\"".$arquivo["name"]."\"\n";
			  $mens .= "Content-Transfer-Encoding: base64\n\n";
			  $mens .= "$anexo\n";
			  $mens .= "--$boundary--\r\n";


			$headers  = "MIME-Version: 1.0\n";
			$headers .= "From: \"$nome\" <$email_from>\r\n";
			$headers .= "Content-type: multipart/mixed; boundary=\"$boundary\"\r\n";
			$headers .= "$boundary\n";

        	$enviou = mail ($recipient, $assunto, $mens, "From: $email\n$headers");
		}else{

			$headers = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= "From: CONTATO MOSTRA OFERTAS: <$email>\r\n";

			$enviou = mail ($recipient, $assunto, $mensagem, $headers);
		}

		// Modifiquei a classe porque a estou incluindo em outra classe q nao posso usar o header.
		// Caso queira usar normalmente habilitar o comando acima
       if($enviou){
	    	$reterono["secesso"] = true;
			$reterono["msg"] = "Mensagem enviado com sucesso.";
	   }else{
			$reterono["secesso"] = false;
			$reterono["msg"] = "Desculpe, seu e-mail nao pode ser enviado.";
	   }
        // exit;
   }else{
        $reterono["secesso"] = false;
		$reterono["msg"] = "Desculpe, seu e-mail nao pode ser enviado.";
   }
   return $reterono;

}

function protect($str){
  if(!is_array($str)){
	  $str = preg_replace('~&#x([0-9a-f]+);~ei', 'chr(hexdec("\\1"))', $str);
	  $str = preg_replace('~&#([0-9]+);~e', 'chr("\\1")', $str);
	  $str = str_replace("&lt;script","",$str);
	  $str = str_replace("script>","",$str);
	  $str = str_replace("&lt;Script","",$str);
	  $str = str_replace("Script>","",$str);
	  $str = trim($str);
	  $tbl = get_html_translation_table(HTML_ENTITIES);
	  $tbl = array_flip($tbl);
	  $str = addslashes($str);
	  $str = strip_tags($str);

  	return strtr($str, $tbl);
  }else{
  	return $str;
  }
}

// GERA URL DA IMAGEM
function urlImage($nome_imagem, $id_galeria, $tipo = "galeria/1/", $options = array( "altura" => "768", "largura" => "1366", "qualidade" => "60" )){
	return IMAGEM_THUMB.$tipo.$id_galeria.'/'.$nome_imagem.'&altura='.$options['altura'].'&largura='.$options['largura'].'&qualidade='.$options['qualidade'];
}

// RETORNA FOTOS MARCADAS COMO DESTAQUE NA GALERIA
function retornarDestaqueGaleria($galeria){
	function filtroDestaque($imagem){
		return $imagem['privada'] == 'S';
	}
	
	return array_filter($galeria['fotos'], 'filtroDestaque');
	
}

?>
