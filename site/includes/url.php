<?php
$url = strip_tags($_SERVER["REDIRECT_URL"]);
$get_array = explode("?",$url); 
$url = explode("/",$get_array[0]);
array_shift($url);
$urlreservada = array();

$urlreservada["home"]               = "index.php";
$urlreservada["contato"]            = "contato.php";
$urlreservada["dr-felipe"]          = "dr-felipe.php";
$urlreservada["equipe"]             = "equipe.php";
$urlreservada["doencas"]            = "doencas.php";
$urlreservada["posoperatorio"]      = "posoperatorio.php";
$urlreservada["videolaparoscopia"]  = "videolaparoscopia.php";
$urlreservada["convenios"]          = "convenios.php";
$urlreservada["blog"]               = "blog.php";
$urlreservada["leitura-blog"]       = "blog.php";



$urlreservada["abscesso"]           = "abscesso.php";
$urlreservada["cancer"]             = "cancer.php";
$urlreservada["diverticulos"]       = "diverticulos.php";
$urlreservada["fissura"]            = "fissura.php";
$urlreservada["hemorroidas"]        = "hemorroidas.php";
$urlreservada["inflamatorios"]      = "inflamatorios.php";
$urlreservada["endometriose"]       = "endometriose.php";



foreach($url as $ind => $valor) {
  if(!is_array($url[$ind])) {
	  $url[$ind] = SQLInjectionProtection($valor);
  } else {
	  $url[$ind] = array_map(SQLInjectionProtection, $valor);
  }
}
?>