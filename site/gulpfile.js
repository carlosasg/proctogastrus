//npm install gulp-uglify gulp-sass gulp-changed gulp-clean-css del gulp-rename gulp-concat gulp-autoprefixer gulp@3.9.1 node-sass --save-dev

var gulp = require('gulp'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	rename = require('gulp-rename'),
	sass = require('gulp-sass'),
	cleanCSS = require('gulp-clean-css'),
	autoprefixer = require('gulp-autoprefixer'),
	changed  = require('gulp-changed');

var sassOptions = {
  errLogToConsole: true,
  outputStyle: 'compressed'
}

// SCRIPTS
gulp.task('scripts', function(){
	gulp.src([
			'assets/js/jquery-3.2.1.min.js',
			'assets/js/lazyload.js',
			'assets/js/popper.js',
			'assets/js/bootstrap.min.js',
			'assets/js/main.js'])
	    .pipe(concat('all.js'))
	    .pipe(rename({suffix:'.min'}))
		.pipe(uglify())
		.on('error', swallowError)
	    .pipe(gulp.dest('assets/js/'));
});

function swallowError (error) {
  // If you want details of the error in the console
  console.log(error.toString())
  this.emit('end')
}

//SASS
gulp.task('sass', function () {
	gulp.src(['assets/css/sass/site.scss'])
	    .pipe(sass(sassOptions).on('error', sass.logError))
	    .pipe(gulp.dest('assets/css/'));
});

// CSS
gulp.task('css', function(){
	gulp.src([
			'assets/css/main.css', 
			'assets/css/site.css'])
	    .pipe(concat('all.css'))
	    .pipe(autoprefixer({
           overrideBrowserslist: ['last 2 versions'],
           cascade: false
   		}))
	    .pipe(rename({suffix:'.min'}))
		.pipe(cleanCSS({compatibility: 'ie8'}))
	    .pipe(gulp.dest('assets/css/'));
});

//Watch Task
gulp.task('watch', function(){
	gulp.watch('assets/js/main.js', ['scripts']);
	gulp.watch('assets/css/sass/**/*.scss', ['sass']);
	gulp.watch('assets/css/site.css', ['css']);
});


//Default task
gulp.task('default', ['scripts', 'sass', 'css', 'watch']);