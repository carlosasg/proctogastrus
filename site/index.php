<?php
  // error_reporting(E_ALL);
  // ini_set('display_errors', 1);

  include("includes/config.php");
  include("includes/funcoes.php");
  include("includes/url.php");

  include("model/padrao.class.php");
  
  $siteObj = new Padrao;
  $siteObj->Set("conexao_host",$config["host"]);
  $siteObj->Set("conexao_usuario",$config["usuario"] );
  $siteObj->Set("conexao_senha",$config["senha"]);
  $siteObj->Set("conexao_database",$config["database"]);
  $siteObj->Conecta();
  
  if (!empty($url[0]) && $urlreservada[$url[0]]) {
    include("controller/".$urlreservada[$url[0]]);
  } else {
    include("controller/index.php");
  }
?>
