<?php
  $padraoObj = new Padrao();
  $padraoObj->Set('idsite', 1);

  if (!empty($_POST['email'])) {
    if ($url[0] == 'newsletter') {
      $salva = $padraoObj->cadastrarUserNews($_POST['email'], $_POST['nome'], 7);
      echo '<script>
          alert("Obrigado, seus dados foram salvos com sucesso.");
          function recarregar(url){window.location = url;}
          recarregar("' . $_SERVER['HTTP_REFERER'] . '");
        </script>';
      exit;
    }

    $resultado = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=CHAVE-SECRETA&response='.$_POST['token_google']);
    $reCaptcha = json_decode($resultado);
    
    if ($reCaptcha->{'success'}) {
      switch ($url[0]) {
        case 'contato':
            $padraoObj->Set('idcategoria', 1);
            $_POST['assunto'] = "Fale Conosco";
          break;
      }

      include('includes/formmail.php');
      
      echo '<script>
          function recarregar(url){window.location = url;}
          recarregar("/obrigado");
        </script>';
      exit;
    }
  }

  switch ($url[0]) {
    case 'contato':
      include_once(VIEWS . $url[0] . '.php');
      break;
  }
?>