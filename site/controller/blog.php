<?php

if (is_numeric($url[1]) && $url[0] == "leitura-blog") {

	$listagemSingle = new Padrao();
	$listagemSingle->Set('idsite',2);
	$listagemSingle->Set('campos', 'c.nome, c.resumo,c.link ,c.data_cad,c.data_entrada ,c.texto,c.idconteudo,ct.idsite,cc.idcategoria,ct.nome as categoria');
	$listagemSingle->Set('idconteudo',$url[1]);
	$listagemSingle->Set('limite','1');
	$listagemSingle->Set('limite_midia','100');
	$listagemSingle->Set('ordem_campo','c.data_cad');
	$listagemSingle->Set('ordem','DESC');
	$first =  $listagemSingle->retornaConteudos();
	$firstItem = @array_shift($first);

	if ($_GET['debug']) {
		echo "<pre>",print_r($firstItem,1);exit;
	}
	//recomendados
	// $maisLidos = new Padrao();
	// $maisLidos->Set('idsite',2);
	// $maisLidos->Set('campos','c.nome, c.resumo, c.data_cad, c.lido, c.texto, c.idconteudo,ct.idsite,cc.idcategoria,ct.nome as categoria');
	// $maisLidos->Set('limite','3');
	// $maisLidos->Set('idcategoria',39);
	// $maisLidos->Set('limite_midia','10');
	// $maisLidos->Set('ordem_campo','c.lido');
	// $maisLidos->Set('ordem','DESC');
	// $lidos =  $maisLidos->retornaConteudos();

	include(VIEWS.'leitura-blog.php');
	exit;
}
//blog
$listagemblog = new Padrao();
$listagemblog->Set('idsite', 2);
$listagemblog->Set('campos', 'c.nome, c.resumo,c.link ,c.data_cad,c.data_entrada ,c.texto,c.idconteudo,ct.idsite,cc.idcategoria,ct.nome as categoria');
$listagemblog->Set('idcategoria', 2);
$listagemblog->Set('limite', '5');
if(is_numeric($url[1])){
	$listagemblog->Set("pagina",$url[1]);
}else{
	$listagemblog->Set("pagina",1);
}
$listagemblog->Set('limite_midia', '1');
$listagemblog->Set('ordem_campo', 'c.data_cad');
$listagemblog->Set('ordem', 'DESC');
$blog = $listagemblog->retornaConteudos();

$firstBlog = current($blog);
$chaves = 1;

if ($_GET['debug']) {
	echo "<pre>",print_r($firstBlog,1);exit;
}
include(VIEWS.'blog.php');
