<?php
    if (!empty($_POST)) {
        if (empty($_POST['nome']) || empty($_POST['email']) || empty($_POST['telefone'])) {
            echo '<script>
                    alert("Todos os campos precisam ser preenchidos.");
                    window.history.back();
                  </script>';
            exit;
        }

        include_once('model/lead.php');
        unset($_POST['url-form']);
        
        $assunto        = $_POST['assunto'];
        
        $post = array(
            'nome'      => $_POST['nome'],
            'email'     => strtolower($_POST['email']),
            'telefone'  => $_POST['telefone'],
            'conversao' => $_POST['conversao'],
            'origem'    => 'SI'
        );
        
        if (!empty($_POST['idempreendimento'])) {
            $post['idempreendimento']   = $_POST['idempreendimento'];
            $post['conversao']          = $_POST['empreendimento'];
            $assunto                    .= ' ' . $_POST['empreendimento'];
        }

        $lead = new Lead($construtor['dev']['url'], $construtor['dev']['email'], $construtor['dev']['token']);
        $interacao = $lead->enviar($post);

        if ((isset($interacao->codigo) && $interacao->codigo == 400)) {
            $erroLead = true;
            
            $_POST['assunto']           = '[INTERESSE] A INTEGRAÇÃO DE LEADS TRABALHOU DE FORMA INESPERADA ' . date('d/m/Y h:m:i');
            $_POST['post_construtor']   = json_encode($lead->getPost());
            $_POST['mensagem']          = json_encode($interacao);
            
            include('includes/formmail.php');
            unset($erroLead, $_POST['post_construtor']);
        }

        $_POST['assunto'] = $assunto;

        $padraoObj = new Padrao();
        $padraoObj->Set('idsite', 1);
        $padraoObj->Set('idcategoria', idcategoria);

        include('includes/formmail.php');

        echo '<script>
                function recarregar(url){window.location = url;}
                recarregar("/empreendimentos/' . geraUrlLimpa($_POST['empreendimento']) . '/obrigado");
            </script>';
        exit;
    } else {
        echo '<script>
                window.history.back();
            </script>';
        exit;
    }
?>