//agendar
var nome;
var telefone;
var convenio;

$('.btAgendar').click(function(event) {
  var txt = "Olá. gostaria de marcar uma consulta para: Nome: "+$('#input-nome').val()+" - Telefone: "+$('#input-telefone').val()+" - Convênio: "+$('#input-convenio').val();
  // console.log(txt);
  window.open('http://web.whatsapp.com/send?1=pt_BR&phone=5579988574444&text='+txt, '_blank');
});

//MENU XS

$('.btMenuXs').click(function(){
  $(this).toggleClass('change');

  $("#menuXs").toggleClass('up');

  $('body').toggleClass("scroll");

});

//owl
var numeroAtual = 1;

$(window).load(function() {

  $('.owl-header').owlCarousel({
      loop:true,
      margin:0,
      nav:true,
      items:1,
      dots: false,
      navText: ['<i class="fas fa-arrow-left"></i>','<i class="fas fa-arrow-right"></i>'],
      navContainer: $('.setasHeader .setas'),
      onInitialized  : counter, //When the plugin has initialized.
      onTranslated : counter
  });


});
function counter(event) {
   var element = event.target;         // DOM element, in this example .owl-carousel
    var items = event.item.count;     // Number of items
    var item = event.item.index;     // Position of the current item
  $('.setasHeader .info').html("0"+numeroAtual+" <span>0"+items+"</span>");
  if(numeroAtual < items){
    numeroAtual++;
  }else{
    numeroAtual = 1;
  }
}






$('.owl-videos').owlCarousel({
    loop:true,
    margin:30,
    nav:true,
    navText: ['<i class="fas fa-arrow-left"></i>','<i class="fas fa-arrow-right"></i>'],
    dots: false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:3
        }
    }
})

var peso = 0;
var altura = 0;
var imc = 0;
var informacao = '';

//imc
$('#botao').click(function () {
  peso = $('#peso').val();
  altura = $('#altura').val();
  imc = peso / (altura * altura);
  var hr = $('<hr>');

  imc = imc * 10000;

  console.log(imc);

  if(imc < 25){
    informacao = 'Normal';
    console.log(informacao);
  }

  if(imc >= 25 && imc < 30){
    informacao = 'Sobrepeso';
    console.log(informacao);
  }

  if(imc >= 30 && imc < 35){
    informacao = 'Obeso Grau I';
    console.log(informacao);
  }

  if(imc >= 35 && imc < 40){
    informacao = 'Obeso Grau II';
    console.log(informacao);
  }

  if(imc >= 40 && imc < 50){
    informacao = 'Obeso Grau III (Mórbida)';
    console.log(informacao);
  }

  if(imc >= 50 && imc < 60){
    informacao = 'Superobeso';
    console.log(informacao);
  }

  if(imc > 60){
    informacao = 'Supersuperobeso';
    console.log(informacao);
  }

  
  
  $('.calculo .form').css('display', 'none');
  $('.refazer').css('display', 'inline-block');
  $('.calculo .resultado').css({
    display: 'flex',
    flex: '1'
  });;

  var spanIMC = $('<span>').text('Seu IMC é: ' + imc.toFixed(2) + ' - '+ informacao);
  $('.calculo .resultado').html(spanIMC);

}); 

$('.refazer').click(function(event) {
  $(this).css('display', 'none');
  $('.calculo .form').css('display', 'flex');
  $('.calculo .resultado').css('display', 'none');
  peso = 0;
  altura = 0;
  imc = 0;
  informacao = '';
  $('#peso').val('');
  $('#altura').val('');
});



//SVG
document.querySelectorAll('img.svg').forEach(function(img){
    var imgID = img.id;
    var imgClass = img.className;
    var imgURL = img.src;

    fetch(imgURL).then(function(response) {
        return response.text();
    }).then(function(text){

        var parser = new DOMParser();
        var xmlDoc = parser.parseFromString(text, "text/xml");

        // Get the SVG tag, ignore the rest
        var svg = xmlDoc.getElementsByTagName('svg')[0];

        // Add replaced image's ID to the new SVG
        if(typeof imgID !== 'undefined') {
            svg.setAttribute('id', imgID);
        }
        // Add replaced image's classes to the new SVG
        if(typeof imgClass !== 'undefined') {
            svg.setAttribute('class', imgClass+' replaced-svg');
        }

        // Remove any invalid XML tags as per http://validator.w3.org
        svg.removeAttribute('xmlns:a');

        // Check if the viewport is set, if the viewport is not set the SVG wont't scale.
        if(!svg.getAttribute('viewBox') && svg.getAttribute('height') && svg.getAttribute('width')) {
            svg.setAttribute('viewBox', '0 0 ' + svg.getAttribute('height') + ' ' + svg.getAttribute('width'))
        }

        // Replace image with new SVG
        img.parentNode.replaceChild(svg, img);

    });

});

//Recaptcha
$(".formPadrao").submit(function(event) {

 var recaptcha = $("#g-recaptcha-response").val();
 if (recaptcha === "") {
    event.preventDefault();
    alert("Favor marcar o captcha.");
 }
});


//Marca ALfama

(function(){

  var logo = $(".logo");
  var m1 = $(".logo .m1");
  var m2 = $(".logo .m2");
  var easings = ["easeOutQuad","easeInOutQuad","easeInOutBack","easeOutElastic","easeOutBounce"];
  var values = [[20,180,0],[170,170,0],[20,360,0],[350,0,0],[0,40,360],[0,320,0],[0,180,0],[180,180,0]];
  
  m1.colh = [100,110,120];
  m2.colh = [255,192,0]

  logo.hover(function(){
      m1.logoanim(1);
      m2.logoanim(2);
  }, function(){
      m1.velocity("reverse");
      m2.velocity("reverse");
  });

  $.fn.logoanim = function(item) {

  var duration = 250;

  var a = 5;

  var e = 3;
  var easing = easings[e];
  if(e >= 2) {duration *= 2}


      if(item==1){

      $(this).velocity({
          rotateX: values[a][0] * 1,
          rotateY: values[a][1] * 1,
          rotateZ: values[a][2] * 1,
       colorRed : this.colh[0],
          colorGreen : this.colh[1],
          colorBlue : this.colh[2]
      },{
          duration: duration,
          easing: easing
          });
  }else{
      $(this).velocity({
          rotateX: values[a][0] * -1,
          rotateY: values[a][1] * -1,
          rotateZ: values[a][2] * -1,
       colorRed : this.colh[0],
          colorGreen : this.colh[1],
          colorBlue : this.colh[2]
      },{
          duration: duration,
          easing: easing
          });
  }
  }
  $(document).ready(function() {
      m1.logoanim(1);
      m1.velocity("reverse");
      m2.logoanim(2);
      m2.velocity("reverse");
  });

})();
