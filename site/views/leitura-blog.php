<!DOCTYPE html>
<html lang="en">
<head>	
	
	<?php 
		$titlePage = $firstItem[0]['nome'];
		$descriptionPage = 'Núcelo Integrado de Cirurgia Digestiva';
		$imgPage = "https://".$_SERVER["SERVER_NAME"].'/assets/img/social.jpg'; //500x250px
		$siteName = 'Gastrus';
	 ?>
	
	<?php include "_head.php"; ?>
	
	<style>	
		.icon {
			font-size:25px;
			margin-right:10px;
			background-color:##fff;
			color:#88d100;
			border-radius:3px
		}
		.conteudo h2 {
			margin-bottom: 15px;
		}
		.conteudo {
			margin-top: 25px;
		}
		
		.conteudo p{
		    color: #001e30;
		    font-size: 15px !important;
		}
	
	</style>


</head>
<body>

	<?php $menu = 'internas'; include '_menu.php'; ?>

	<?php include '_header.php'; ?>
		
	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-10 offset-md-1 conteudo">
					<div class="titulo">
						<h2><?=$firstItem[0]['nome']?></h2>
						<?php  echo leitura($firstItem[0],$firstItem[1],'conteudo',$firstItem[0]['idsite'], '');  ?>
						<div class="row">
							<p style="margin-right: 15px; font-weight: bold;">Compartilhar:</p> 
								<!-- Facebook -->
							<a class="facebook" href="javascript:window.open('https://www.facebook.com/sharer.php?u=<?php echo "https://".$_SERVER["SERVER_NAME"].strtok($_SERVER ["REQUEST_URI"],"?");?>','Share','width=500,height=500')">
								<div class="icon">
								<i class="fab fa-facebook-square" aria-hidden="true"></i>
								</div>
							</a>                                      
							<!-- Twitter -->
							<a class="twitter" href="javascript:window.open('https://twitter.com/share?url=<?php echo "https://".$_SERVER["SERVER_NAME"].strtok($_SERVER ["REQUEST_URI"],"?"); ?>' ,'Share','width=500,height=500')">
								<div class="icon">
								<i class="fab fa-twitter-square" aria-hidden="true"></i>
								</div>
							</a>
							<!-- wpp -->
							<a class="twitter" href="javascript:window.open('https://api.whatsapp.com/send?text=<?php echo "https://".$_SERVER["SERVER_NAME"].strtok($_SERVER ["REQUEST_URI"],"?"); ?>' ,'Share','width=500,height=500')">
								<div class="icon">
								<i class="fab fa-whatsapp-square" aria-hidden="true"></i>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>

	
	</section>

	<?php include "_newsletter.php"; ?>
	
	<?php include "_redes.php"; ?>
   
	<?php include "_footer.php"; ?>

   <?php include "_scripts-footer.php"; ?>
   
</body>
</html>
