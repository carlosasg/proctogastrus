
<style>
  .barra-slide {
  background: rgba(39,174,96,0.86);
  position: fixed;
  width: 270px;
  bottom: 20px;
  height: 33px;
  right: 70px;
  border-radius: 20px;
  font-family: 'Titillium Web', sans-serif;
  padding: 8px 20px 5px 20px;
	position: fixed;
	cursor: pointer;
}
a#slide-botao {
  color: #fff;
	text-decoration: none;
}
.slide-tit-topo {
  background-color: #88d000;
  padding: 10px 10px;
  color: #fff;
	line-height: 116%;
	border-top-right-radius: 5px;
	border-top-left-radius: 5px;
	margin: 2px;
  font-weight: 100;
  text-align: center;
  font-family: Montserrat-Regular;
}
#conteudo-slide{
  height: 0;
  overflow: hidden;
  transition: height 0.8s ease;
	font-family: 'Titillium Web', sans-serif;
}
div#conteudo-slide {
  width: 280px;
  background: #fff;
  border: solid 2px #88d000;
  position: fixed;
  bottom: -5px;
  right: 230px;
  border-top-right-radius: 5px;
  border-top-left-radius: 5px;
  border-bottom-left-radius: 5px;
  border-bottom-right-radius: 5px;
  z-index: 9999;
}
#conteudo-slide a {
  width: 100%;
  height: 64px;
  display: flex;
  padding: 3px;
  padding-top: 10px;
	text-decoration: none;
	border-bottom: 1px solid #66666629;
}
#conteudo-slide a:hover {
	background: #ededed;
	}
.slide-atende img {
  height: 55px;
  border-radius: 50%;
}
.slide-atende {
  float: left;
  margin-right: 12px;
}
.slide-atende-setor {
  display: flex;
	color:#3B3B3B;
	text-decoration: none;
	font-size: 12px;
}
.slide-atende-nome {
  display: flex;
	color: #000;
	text-decoration: none;
	font-weight:bold;
  font-family: Montserrat-Regular;
  font-size: 15px;
}
.i-style{
  margin-top: 3px;
  margin-right: 10px;
}

.overlay a.active {
  color:#fff;
}

</style>

<div id="menuXs" class="overlay">
  <div class="overlay-content">
    <div class="itensMenu">
      <a href="/home" title="Home" <?php if($url[0] == '' || $url[0] == 'home'){ echo 'class="active"'; } ?> >Home</a>
      <a href="/dr-felipe" title="Dr. Felipe" <?php if($url[0] == 'dr-felipe'){ echo 'class="active"'; } ?> >Dr. Felipe</a>
      <a href="/equipe" title="Equipe" <?php if($url[0] == 'equipe'){ echo 'class="active"'; } ?> >Equipe</a>
      <a href="/doencas" title="Principais Doenças" <?php if($url[0] == 'doencas'){ echo 'class="active"'; } ?> >Principais Doenças</a>
      <a href="/posoperatorio" title="Pós-operatório" <?php if($url[0] == 'posoperatorio'){ echo 'class="active"'; } ?> >Pós-operatório</a>
      <a href="/videolaparoscopia" title="Videolaparoscopia" <?php if($url[0] == 'videolaparoscopia'){ echo 'class="active"'; } ?> >Videolaparoscopia</a>
      <a href="/blog" title="Blog" <?php if($url[0] == 'blog'){ echo 'class="active"'; } ?> >Notícias</a>
      <a href="/convenios" title="Convênios" <?php if($url[0] == 'convenios'){ echo 'class="active"'; } ?> >Convênios</a>
      <a href="/contato" title="Contato" <?php if($url[0] == 'contato'){ echo 'class="active"'; } ?> >Locais de Atendimento</a>
    </div>
  </div>
</div>

<div class="btMenuXs d-lg-none d-block">
    <div class="barras">
        <div class="bar1"></div>
        <div class="bar2"></div>
        <div class="bar3"></div>
    </div>
</div>

<nav class="<?php if(isset($menu)){ echo $menu; } ?>">
  <div class="container">
    <div class="row">
      <div class="col-lg-2 col-md-6 marcaTopo">
        <a href="/home" title="Página Inicial <?= $siteName ?>">
          <img src="/assets/img/marca-topo.png" title="Marca <?= $siteName ?>" alt="marca-topo" class="img-fluid">
        </a>
      </div>
      <div class="col-lg-10 menu d-none d-lg-flex">
        <ul>
          <li>
            <a href="/home" title="Home" <?php if($url[0] == '' || $url[0] == 'home'){ echo 'class="active"'; } ?> >Home</a>
          </li>
          <li>
            <a href="/dr-felipe" title="Dr. Felipe" <?php if($url[0] == 'dr-felipe'){ echo 'class="active"'; } ?> >Dr. Felipe</a>
          </li>
          <li>
            <a href="/equipe" title="Equipe" <?php if($url[0] == 'equipe'){ echo 'class="active"'; } ?> >Equipe</a>
          </li>
          <li>
            <a href="/doencas" title="Principais Doenças" <?php if($url[0] == 'doencas'){ echo 'class="active"'; } ?> >Principais Doenças</a>
          </li>
          <li>
            <a href="/posoperatorio" title="Pós-operatório" <?php if($url[0] == 'posoperatorio'){ echo 'class="active"'; } ?> >Pós-operatório</a>
          </li>
          <li>
            <a href="/videolaparoscopia" title="Videolaparoscopia" <?php if($url[0] == 'videolaparoscopia'){ echo 'class="active"'; } ?> >Videolaparoscopia</a>
          </li>
          <li>
            <a href="/blog" title="Blog" <?php if($url[0] == 'blog'){ echo 'class="active"'; } ?> >Notícias</a>
          </li>
          <li>
            <a href="/convenios" title="Convênios" <?php if($url[0] == 'convenios'){ echo 'class="active"'; } ?> >Convênios</a>
          </li>
          <li>
            <a href="/contato" title="Contato" <?php if($url[0] == 'contato'){ echo 'class="active"'; } ?> >Atendimento</a>
          </li>
        </ul>
      </div>
    </div>
    <div class="social d-none d-lg-flex">
      <a href="/https://www.facebook.com/clinicagastrusaracaju/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a>
      <a href="/https://www.instagram.com/clinicagastrus/" target="_blank" title="Facebook"><i class="fab fa-instagram"></i></a>
      <a id="slide-botao"class="whatsapp" style="cursor:pointer;">
        <i class="fab fa-whatsapp"></i>
        <span>FALE VIA<br><b>WHATSAPP</b></span>
      </a>
    </div>
  </div>
</nav>



<!-- Slide whatsapp -->
<div id="conteudo-slide">
		<div class="slide-tit-topo" style="cursor:pointer;">
      Entre em contato direto por Whatsapp.
		</div>
		
		<a href="https://api.whatsapp.com/send?phone=5579988574444" target="_blank" class="">
			<div class="slide-atende"></div>
			<div class="slide-atende-info">						
				<span class="slide-atende-nome"> <i class="fab fa-whatsapp i-style"></i> Atendimento Clínica Gastrus</span>
			</div>
		</a>
		
		<a href="https://api.whatsapp.com/send?phone=5579991546009" target="_blank" class="">
			<div class="slide-atende"></div>
			<div class="slide-atende-info">					
				<span class="slide-atende-nome"> <i class="fab fa-whatsapp i-style"></i> Atendimento Torres Centro Médico</span>
			</div>
		</a>
	</div>