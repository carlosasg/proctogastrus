<!DOCTYPE html>
<html lang="en">
<head>

	<?php
		$titlePage = 'Blog';
		$descriptionPage = 'Núcelo Integrado de Cirurgia Digestiva';
		$imgPage = "https://".$_SERVER["SERVER_NAME"].'/assets/img/social.jpg'; //500x250px
		$siteName = 'Gastrus';
	 ?>

	<?php include "_head.php"; ?>

	<style>
	.titulo-blog{
		font-weight: bold;
		font-size: 20px;
		font-family: Montserrat-Bold;
		margin-bottom: 0px;
		min-height: 90px;
	}
	.resumo-blog{
		font-size: 16px;
		margin-bottom: 20px !important;
		margin-top: 10px;
	}
	.btn-blog{
		width: 90%;
		text-align: center;
		position: relative;
		left: 5%;
	}
	.card {
		margin-bottom: 20px;
	}
	</style>

</head>
<body>

	<?php $menu = 'internas'; include '_menu.php'; ?>

	<?php include '_header.php'; ?>

	<section>
		<div class="container">

		<div class="row">
        <div class="col-md-12 conteudo">
          <div class="titulo">
            <h2>Nosso Blog</h2>
          </div>
        </div>
      </div>

		<?if($blog){?>
			<div class="row">
			<?

			foreach ($blog as $key => $value) {
				for ($i = 0; $i <= count($value[1]); $i++ ) {
					if ($value[1][$i]['capa'] == 'S') {
						$imgBgTop = IMAGEM_THUMB.'conteudo/'.$value[0]["idsite"].'/'.$value[0]['idconteudo'].'/'.$value[1][$i]['avatar_servidor'].'&qualidade=70';
					} else {
						$imgBgTopMobile = IMAGEM_THUMB.'conteudo/'.$value[0]["idsite"].'/'.$value[0]['idconteudo'].'/'.$value[1][$i]['avatar_servidor'].'&qualidade=50';
					}
					if(!$imgBgTopMobile && !$imgBgTop){
						$imgBgTop = "/assets/img/dev/marca-topo.png";
					}
				}
			?>
				<div class="col-sm-4">
					<div class="card">
					<div style="background: url(<?=$imgBgTop?>) no-repeat center center;background-size: cover;height: 200px;">
					</div>
						<div class="card-body">
							<p class="text-dark titulo-blog" href="/leitura-blog/<?=$value[0]['idconteudo']?>/<?=geraUrlLimpa($value[0]['nome'])?>"><?=$value[0]['nome']?></p>
							<div class="mb-1 text-muted" id="dateh1"><?=formataData($value[0]['data_cad'],"",2)?></div>

							<p class="card-text mb-auto resumo-blog"><?=cortar($value[0]['resumo'],80)?></p>
							<a href="/leitura-blog/<?=$value[0]['idconteudo']?>/<?=geraUrlLimpa($value[0]['nome'])?>" class="btBox btn-blog">Continuar lendo</a>
						</div>
					</div>
				</div>
				<? } ?>
			</div>
			<!-- <div class -->
			<div id="paginacao">
				<!-- PAGINAÇÃO -->
				<div aria-label="...">
					<?php paginacaoGeralBlog($listagemblog->Get("paginas"), $listagemblog->Get("pagina"), $listagemblog->Get("limite"), $listagemblog->Get("ordenarPor"), '/'.$url[0].'/',geraUrlLimpa('pages').$complemento); ?>
				</div>
				<!-- FIM PAGINAÇÃO -->
			</div>
		<?
		}else{?>
			<div class="row">
				<div class="card flex-md-row mb-4 shadow-sm h-md-250" id="destaque-blog">
					<div class="card-body d-flex flex-column align-items-start">
						<h3 class="mb-0">Nenhuma notícia cadastrada...</h3>
					</div>
				</div>
			</div>
		<?}?>
		</div>

	</section>


	<style>
			.img-destaque {
				width: 30%;
			}

			.img-card {
				width: 40%;

			}

			h3 {
				color: #001e30;
				font-size: 20px;
				font-family: Montserrat-Bold;
				text-transform: uppercase;
			}

			p {
				font-size: 11px;
				font-family: Montserrat-Light;
				line-height: 30px;
				margin-bottom: 20px;
				color: #959595;
			}

			#date {
				font-size:10px;
			}

			#dateh1 {
				font-size:12px;
			}




	</style>










	<?php include "_newsletter.php"; ?>

 	<?php include "_redes.php"; ?>

 	<?php include "_footer.php"; ?>

	<?php include "_scripts-footer.php"; ?>

</body>
</html>
