<!DOCTYPE html>
<html lang="en">

<head>

  <?php 
  
  $titlePage = 'Convênios';
  $descriptionPage = 'Núcelo Integrado de Cirurgia Digestiva';
  $imgPage = "https://".$_SERVER["SERVER_NAME"].'/assets/img/social.jpg'; //500x250px
  $siteName = 'Gastrus';

  include '_head.php';
      
  ?>

</head>

<body>

  <?php $menu = 'internas'; include '_menu.php'; ?>

  <?php include '_header.php'; ?>

  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-12 conteudo">
          <div class="titulo">
            <h2>Convênios</h2>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
        <ul class="listaConvenios">
          <!-- <?
            for ($i=1; $i < 25; $i++) { 
          ?>
          <li style="background: url(assets/img/c<?= $i ?>.jpg) no-repeat center center;">
          </li>
          <?
            }
          ?>
          ?> -->
          <li style="background: url(assets/img/c3.jpg) no-repeat center center;"></li>
          <li style="background: url(assets/img/c4.jpg) no-repeat center center;"></li>
          <li style="background: url(assets/img/c5.jpg) no-repeat center center;"></li>
          <li style="background: url(assets/img/c6.jpg) no-repeat center center;"></li>
          <li style="background: url(assets/img/c7.jpg) no-repeat center center;"></li>
          <li style="background: url(assets/img/c8.jpg) no-repeat center center;"></li>
          <li style="background: url(assets/img/c10.jpg) no-repeat center center;"></li>
          <li style="background: url(assets/img/c11.jpg) no-repeat center center;"></li>
          <li style="background: url(assets/img/c12.jpg) no-repeat center center;"></li>
          <li style="background: url(assets/img/c13.jpg) no-repeat center center;"></li>
          <li style="background: url(assets/img/c14.jpg) no-repeat center center;"></li>
          <li style="background: url(assets/img/c15.jpg) no-repeat center center;"></li>
          <li style="background: url(assets/img/c16.jpg) no-repeat center center;"></li>
          <li style="background: url(assets/img/c20.jpg) no-repeat center center;"></li>
          <li style="background: url(assets/img/c21.jpg) no-repeat center center;"></li>
          <li style="background: url(assets/img/c22.jpg) no-repeat center center;"></li>
          <li style="background: url(assets/img/c23.jpg) no-repeat center center;"></li>
          <li style="background: url(assets/img/c24.jpg) no-repeat center center;"></li>
        </ul>  
        
        </div>
      </div>
    </div>
  </section>

  <?php include '_footer.php'; ?>

  <?php include '_scripts-footer.php'; ?>
     
</body>

</html>
