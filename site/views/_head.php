<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="description" content="<?php echo $descriptionPage; ?>'/>">
<meta name="author" content="Alfama Web">

<meta name="theme-color" content="#88D100">

<!-- Social: Twitter -->
<meta name='twitter:card' content='summary_large_image'>
<meta name='twitter:title' content='<?php echo $siteName; ?> - <?php echo $titlePage; ?>'>
<meta name='twitter:description' content='<?php echo $descriptionPage; ?>'>
<meta name='twitter:image:src' content='<?php echo $imgPage; ?>'>

<!-- Social: Facebook / Open Graph -->
<meta property='og:url' content='<?php echo "https://".$_SERVER["SERVER_NAME"].strtok($_SERVER ["REQUEST_URI"],"?"); ?>'>
<meta property='og:type' content='website'>
<meta property='og:title' content='<?php echo $siteName; ?> - <?php echo $titlePage; ?>'>
<meta property='og:image' content='<?php echo $imgPage; ?>'/>
<meta property='og:description' content='<?php echo $descriptionPage; ?>'>
<meta property='og:site_name' content='<?php echo $siteName; ?>'>

<!-- ICONS --> 
<link rel="apple-touch-icon" sizes="57x57" href="/assets/img/metas/apple-icon-57x57.png"> 
<link rel="apple-touch-icon" sizes="60x60" href="/assets/img/metas/apple-icon-60x60.png"> 
<link rel="apple-touch-icon" sizes="72x72" href="/assets/img/metas/apple-icon-72x72.png"> 
<link rel="apple-touch-icon" sizes="76x76" href="/assets/img/metas/apple-icon-76x76.png"> 
<link rel="apple-touch-icon" sizes="114x114" href="/assets/img/metas/apple-icon-114x114.png"> 
<link rel="apple-touch-icon" sizes="120x120" href="/assets/img/metas/apple-icon-120x120.png"> 
<link rel="apple-touch-icon" sizes="144x144" href="/assets/img/metas/apple-icon-144x144.png"> 
<link rel="apple-touch-icon" sizes="152x152" href="/assets/img/metas/apple-icon-152x152.png"> 
<link rel="apple-touch-icon" sizes="180x180" href="/assets/img/metas/apple-icon-180x180.png"> 
<link rel="icon" type="image/png" sizes="192x192" href="/assets/img/metas/android-icon-192x192.png"> 
<link rel="icon" type="image/png" sizes="32x32" href="/assets/img/metas/favicon-32x32.png"> 
<link rel="icon" type="image/png" sizes="96x96" href="/assets/img/metas/favicon-96x96.png"> 
<link rel="shortcut icon" type="image/png" sizes="16x16" href="/assets/img/metas/favicon-16x16.png"> 
<link rel="manifest" href="/assets/img/metas/manifest.json"> 
<meta name="msapplication-TileColor" content="#88D100"> 
<meta name="msapplication-TileImage" content="/assets/img/metas/ms-icon-144x144.png"> 

<link rel="stylesheet" type="text/css" href="/assets/css/all.min.css">
<!-- 
<noscript id="deferred-styles">
	<link rel="stylesheet" type="text/css" href="/assets/css/all.min.css?v=1">
</noscript>
<script>
  var loadDeferredStyles = function() {
    var addStylesNode = document.getElementById("deferred-styles");
    var replacement = document.createElement("div");
    replacement.innerHTML = addStylesNode.textContent;
    document.body.appendChild(replacement)
    addStylesNode.parentElement.removeChild(addStylesNode);
  };
  var raf = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
      window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
  if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
  else window.addEventListener('load', loadDeferredStyles);
</script> -->

<title><?php echo $siteName; ?> - <?php echo $titlePage; ?></title>

<?php if (!isset($_SERVER['HTTP_USER_AGENT']) || stripos($_SERVER['HTTP_USER_AGENT'], 'Speed Insights') === false): ?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-125058528-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-125058528-1');
</script>

<?php endif; ?>

<style>
		.icon {
			font-size:25px;
			margin-right:10px;
			background-color:##fff;
			color:#88d100;
			border-radius:3px
		}
		.conteudo h2 {
			margin-bottom: 15px;
		}
		.conteudo {
			margin-top: 25px;
		}
		
		.conteudo p{
		    color: #001e30;
		    font-size: 15px !important;
		}

</style>