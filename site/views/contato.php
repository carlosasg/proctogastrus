<!DOCTYPE html>
<html lang="en">

<head>

  <?php 
  
  $titlePage = 'Locais de Atendimento';
  $descriptionPage = 'Núcelo Integrado de Cirurgia Digestiva';
  $imgPage = "https://".$_SERVER["SERVER_NAME"].'/assets/img/social.jpg'; //500x250px
  $siteName = 'Gastrus';

  include '_head.php';
      
  ?>

<style>
.box-whats{
    height: 90px;
    display: flex;
    align-items: center;
    font-size: 20px;
    width: 424px;
    max-width:100%;
    padding: 9px 20px;
}
.box-whats i {
  margin-right: 10px;
}
@media screen and (max-width: 640px) {
  .box-whats{
    height: 120px;
  } 
}
</style>
</head>

<body>

  <?php $menu = 'internas'; include '_menu.php'; ?>

  <?php include '_header.php'; ?>

  <section>
    <div class="container">
      <div class="row">
        <div class="col-lg-6 col-md-12 conteudo">
          <div class="titulo">
            <h2>Locais de Atendimento</h2>
            <p>Entre em contato direto por Whatsapp</p>
            <div class="row">
              <a href="https://api.whatsapp.com/send?phone=5579988574444" target="_blank" title="Contato Whats" class="btBox box-whats"> <i class="fab fa-whatsapp"></i> Atendimento Clínica Gastrus</a>
            </div>
            <div class="row" style="margin-top:15px; padding-bottom:20px;">
              <a href="https://api.whatsapp.com/send?phone=5579991546009" target="_blank" title="Contato Whats" class="btBox box-whats"> <i class="fab fa-whatsapp"></i> Atendimento Torres Centro Médico</a>
            </div>
          </div>

          <!-- <form id="form_contato" class="formPadrao" name="form_contato" method="post" action="/enviar" enctype='multipart/form-data' data-sucess='Mensagem enviada com sucesso' data-error='Problemas ao enviar a mensagem'>
             <input type="hidden" name="assunto" value="Clínica Gastrus">
              <input type='hidden' name='subject' value='Clínica Gastrus'>
              <input type='hidden' name='site' value='<?php echo "https://".$_SERVER["SERVER_NAME"].strtok($_SERVER ["REQUEST_URI"],"?"); ?>'>
              <div class="row">
                <div class="col-sm-12">
                  <div class='lince-input'>
                      <label for='input-nome'></label>
                      <input type='text' name='nome' placeholder='Nome' id='input-nome' maxlength='100' class='input-alpha' required>
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class='lince-input'>
                      <label for='input-email'></label>
                      <input type='text' name='email' placeholder='E-mail' id='input-email' maxlength='50' class='input-email' required>
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class='lince-input'>
                      <label for='input-telefone'></label>
                      <input type='text' name='telefone' placeholder='Telefone' id='input-telefone' maxlength='50' class='input-tel' required>
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class='lince-input'>
                      <label for='input-assunto'></label>
                      <input type='text' name='assunto' placeholder='Assunto' id='input-assunto' maxlength='100' class='input-alpha' required>
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class='lince-input'>
                      <label for='input-mensagem'></label>
                      <textarea name='mensagem' id='input-mensagem' placeholder='Mensagem' class='textarea' maxlength='5000' cols='30' rows='6' required></textarea>
                  </div>
                </div>
                <div class="col-sm-12 captcha">
                  <div class="g-recaptcha" data-sitekey="6LeiHG4UAAAAAKHbQDv0NUqe6yFBLrz-GG20WSl1"></div>
              </div>
              <div class="col-sm-12 text-center">
                <input type='submit' value='Enviar Mensagem' class="btBox">
              </div>

          </div> -->
            
            <input type='text' name='url-form' class='url-form' value=' ' />
        </form>
        </div>
       
        <div class="col-lg-6 col-md-12 conteudo">
        <h3>Torres Centro Médico</h3>
          <p>Centro Médico José Augusto Barreto, salas 508/09</p>
          <p>Avenida Gonçalo Prado Rolemberg, 211<br>Aracaju - SE, 49010-410<br><br></p>
          <h3>Clínica Gastrus</h3>
          <p>Av. Ministro Geraldo Barreto Sobral, 2131, Sala 207 - Bairro Jardins<br>Aracaju - SE, 49026-010<br>(79) 3024-5505<br>clinicagastrus.se@gmail.com</p>
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3917.1832681885808!2d-37.066999985197626!3d-10.949524792204201!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x71ab3dc45fe05c1%3A0xb3e0e6ada612cb3e!2sAv.+Ministro+Geraldo+Barreto+Sobral%2C+2131+-+Jardins%2C+Aracaju+-+SE%2C+49026-240!5e0!3m2!1sen!2sbr!4v1535742286154" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
        
      </div>
    </div>
  </section>

  <?php include '_footer.php'; ?>

  <?php include '_scripts-footer.php'; ?>

  <script src='https://www.google.com/recaptcha/api.js'></script>
     
</body>

</html>
