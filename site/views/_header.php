<header class="internas">
  <div class="container">
    <div class="row">
      <div class="col-md-10">
        <ul class="breadcrumb">
          <li class="breadcrumb-item"><a href="/home">Home</a></li>
          <li class="breadcrumb-item active"><?= $titlePage ?></li>
        </ul>
      </div>
    </div>
  </div>
</header>