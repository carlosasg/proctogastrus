<!DOCTYPE html>
<html lang="en">

<head>

  <?php 
  
  $titlePage = 'PRINCIPAIS DOENÇAS - Doenças Inflamatórias Intestinais';
  $descriptionPage = 'O termo Doenças Inflamatórias Intestinais se refere basicamene as duas patologias. ';
  $imgPage = "https://".$_SERVER["SERVER_NAME"].'/assets/img/social.jpg'; //500x250px
  $siteName = 'Principais Doenças';

  include '_head.php';
      
  ?>

<style>
  .row-abscesso{
    margin-right: 15px;
    margin-left: 15px;
  }
  </style>

</head>

<body>

  <?php $menu = 'internas'; include '_menu.php'; ?>

  <?php include '_header.php'; ?>

  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-12 conteudo">
          <div class="titulo">
            <h2>Doenças Inflamatórias Intestinais</h2>
          </div>
        </div>
      </div>
		<div class="row conteudo row-abscesso">


		  <h3>Quais são as principais doenças inflamatórias intestinais?</h3>
      <p>O termo doenças inflamatórias intestinais se refere basicamente a duas patologias: à doença de crohn e à retocolite ulcerativa. Ambas caracterizadas por inflamações na forma de úlceras, erosões, descamação da mucosa, provocando diarreia, dor abdominal, muco, pus ou sangue nas fezes.</p>

      <h3>De que maneira essas doenças limitam a vida social de um indivíduo?</h3>
      <p>Por acometerem cronicamente o indivíduo e por apresentarem períodos de exacerbação dos sintomas,  são patologias que, se não acompanhadas de perto por especialista e equipe multidisciplinar treinada, acarretam importante limitação da atividade social, absenteísmo no trabalho, limitação para viagens e eventos de lazer. Mas com novas terapias que vem surgindo no mundo e com a co-participação do paciente no cuidado, esse prejuízo é minimizado e até mesmo eliminado, em alguns casos.</p>
		  
      <h3>Quais são as causas? Quais hábitos ajudam a piorar o quadro?</h3>
      <p>São patologias sem uma causa única ou específica, de origem multifatorial. A predisposição genética, fatores higieno-dietéticos, microorganismos da flora (ou microbiota) intestinal e até fatores emocionais estão ligados à etiologia das doenças inflamatórias intestinais. O que se sabe é que elas tem um fundo imunológico, ou seja, proteínas e células inflamatórias do próprio organismo causam danos às paredes do tubo digestivo do indivíduo. Alimentos enlatados, embutidos, processados e com muitos condimentos podem agravar os sintomas. Orienta-se um estilo de vida saudável, abundante em frutas, fibras e vegetais, menor quantidade de carne vermelha, atividade física regular e ambiente psicossocial saudável.</p>

      <h3>Há casos em que a doença ultrapassa os problemas intestinais, repercutindo também em outros órgãos do corpo? Por que isso acontece?</h3>
      <p>Sim, são as chamadas manifestações extra-intestinais da doença, que podem acometer ossos e articulações, rins, fígado e vias biliares (canais da bile), pele, olho entre outras. Elas acontecem porque o tubo digestivo, de certa forma, sinaliza para todo o sistema imunológico que "algo errado" está acontecendo e essas células inflamtórias podem se ativar em outros órgãos e causar danos a eles. Outras manifestações ocorrem por efeito secundário à disabsorção de nutrientes e sais no sangue, como é o caso de cálculos na vesícula e de cálculos renais. </p>

      <h3>Como acontece o diagnóstico dessas doenças inflamatórias?</h3>
      <p>Os principais exames para diagnóstico são a colonoscopia e a endoscopia, por verem diretamente o aspecto inflamado da mucosa. Além disso, eles permitem a biópsia que, por sua vez  pode encontrar alguns padrões microscópicos de inflamação. Exames de sangue e de fezes ajudam a excluir outros diagnósticos comuns como parasitose intestinal (verminoses), doença celíaca e intolerância a lactose. Já os exames de imagem, como êntero-tomografia e êntero-ressonância, ajudam a estadiar a doença e verificar complicações.</p>

      <h3>Como se dá o tratamento para essas doenças? </h3>
      <p>Hoje vivemos uma epoca favorável ao tratamento. Novas drogas estão surgindo todos os anos, tendo como alvo sítios cada vez mais específicos. Mas de forma geral o objetivo do tratamento é combater a inflamação, seja através de anti-inflamatórios que agem no intestino e corticóides, seja através de medicamentos que reduzem a imunidade do paciente (imunossupressores). Destes medicamentos, sem dúvida os chamados biológicos, que são anticorpos contra receptores e moléculas inflamatórias, vieram para revolucionar e mudar a história natural da doença.</p>

      <h3>As doenças no intestino podem ter ligação com o lado psicológico do indivíduo? Como identificar quadros de diarreia ou prisão de ventre constantes em pessoas com problemas de ansiedade?</h3>
      <p>A resposta é sim. As diversas moléculas liberadas pelo cérebro durante estresse, tristeza ou ansiedade interferem diretamente no funcionamento digestivo. Inclusive, em períodos de estresse, as doenças inflamatórias podem exacerbar. Mas o fato de pessoas terem intestino preso ou diarréia durante período de estresse não significa necessariamente que ele tem alguma doenca intestinal. O especialista é quem irá desvendar essa dúvida.</p>

      <h3>O que é a Campanha Maio Roxo?</h3>
      <p>A campanha maio roxo surgiu inspirada por outras campanhas semelhantes ao redor do mundo, com o intuito de conscientizar a população e as autoridades sobre a importância dessas doenças. O objetivo é promover melhorias na assistência aos pacientes, aproximar pacientes  e equipe profissional para desmistificar assuntos do tema, e aproximar os próprios paciente para dividir experiências com o tratamento e com  o manejo da doença. Embora não tenham cura, essas doenças podem ser bem controladas. Dar a oportunidade ao paciente de voltar ao convívio social é, sem dúvida, a maior meta. </p>


      <!-- <div class="row">
        <div class="col-md-12">
        <iframe width="100%" height="600" src="https://www.youtube.com/embed/slzja37r5vk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div> -->
    </div>
  </section>

  <?php include '_footer.php'; ?>

  <?php include '_scripts-footer.php'; ?>
     
</body>

</html>
