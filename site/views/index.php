<!DOCTYPE html>
<html lang="en">

<head>

  <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-159375968-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-159375968-1');
    </script>






  <?php 
  
  $titlePage = 'Dr. Felipe Torres';
  $descriptionPage = 'Núcelo Integrado de Cirurgia Digestiva';
  $imgPage = "https://".$_SERVER["SERVER_NAME"].'/assets/img/social.jpg'; //500x250px
  $siteName = 'Home';

  include '_head.php';
      
  ?>

</head>

<body>

  <?php include '_menu.php'; ?>

  <header>
    <div class="owl-carousel owl-theme owl-header" style="margin-top: 60px;">
     
		<?php //-- Very simple way
			$useragent = $_SERVER['HTTP_USER_AGENT']; 
			$iPod = stripos($useragent, "iPod"); 
			$iPad = stripos($useragent, "iPad"); 
			$iPhone = stripos($useragent, "iPhone");
			$Android = stripos($useragent, "Android"); 
			$iOS = stripos($useragent, "iOS");
			//-- You can add billion devices 

			$DEVICE = ($iPod||$iPad||$iPhone||$Android||$iOS||$webOS||$Blackberry||$IEMobile||$OperaMini);

			if ($DEVICE !=true) {?>

			<!--DESKTOP-->
			  <div class="item" style="background: url(assets/img/slide-01.jpg) no-repeat center center;"></div>
			  <div class="item" style="background: url(assets/img/slide-02.jpg) no-repeat center center;"></div>

			<?php }else{ ?> 

			<!--MOBILE-->
			  <div class="item" style="background: url(assets/img/slide-01-mob.jpg) no-repeat center center;"></div>
			  <div class="item" style="background: url(assets/img/slide-02-mob.jpg) no-repeat center center;"></div>

			<?php } ?>
		
    </div>
    <div class="setasHeader">
      <div class="container">
        <div class="row">
          <div class="col-md-12 conteudo">
            <div class="info"></div>
            <div class="setas"></div>
          </div>
        </div>
      </div>
    </div>
  </header>

  <section class="calculo">
    <div class="container">
      <div class="row justify-content-md-center">
        <div class="col-md-7  justify-content-md-center">
          <div class="bg">
            <div class="tituloDestaque">
              <h2 class="branco">Agende sua consulta</h2>
            </div>
            <div class="form">
              <button class="btBox azul">
				 <a href="http://www.clinicagastrus.com.br/equipe.php?id=37">Agendar</a>
				</button>
            </div>
            <div class="resultado"></div>
          </div>  
        </div>
      </div>
    </div>
  </section>

  <section class="gastrus">
    <div class="container">
      <div class="row" style="justify-content: center;">
        <div class="col-lg-6 col-md-6">
          <div class="titulo">
            <h1>SOBRE A COLOPROCTOLOGIA</h1>
          </div>
          <div>
            <p>A Coloproctologia (do grego Kólon- intestino grosso, e proktós, significando ânus e, por extensão, reto;) é a especialidade que cuida das patologias do intestino grosso, reto e ânus. </p>

			<p>É uma especialidade tanto clínica quanto cirúrgica, e também envolve a realização de exames como a Colonoscopia, manometria anorretal, ultrassom endoanal, entre outras. </p>
			  
            <a href="coloproctologia" title="Saiba Mais" class="btBox">Saiba Mais</a>
          </div>
        </div>
        <div class="col-lg-5 d-none d-lg-flex imgGastru">
          <img src="assets/img/colopro.jpg" title="COLOPROCTOLOGIA" alt="COLOPROCTOLOGIA">
        </div>
        <div class="col-lg-4 col-md-6" style="display: none;">
          <div class="formAgendamento">
            <div class="tituloDestaque text-center">
              <h2 class="branco">PRÉ AGENDAMENTO<br><span>DE CONSULTA</span></h2>
            </div>
            <form id="form_contato" name="form_contato" method="post" action="/contato" enctype='multipart/form-data' data-sucess='Mensagem enviada com sucesso' data-error='Problemas ao enviar a mensagem'>
                 <input type="hidden" name="assunto" value="Gastrus">
                  <input type='hidden' name='subject' value='Gastrus'>
                  <div class="row">
                   
                    <div class="col-sm-12">
                      <div class='lince-input'>
                          <label for='input-nome'></label>
                          <input type='text' name='nome' placeholder='Nome' id='input-nome' maxlength='100' class='input-alpha' required>
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class='lince-input'>
                          <label for='input-telefone'></label>
                          <input type='text' name='telefone' placeholder='Telefone' id='input-telefone' maxlength='50' class='input-tel' required>
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class='lince-input'>
                          <label for='input-convenio'></label>
                          <input type='text' name='convenio' placeholder='Convênio' id='input-convenio' maxlength='100' class='input-alpha' required>
                      </div>
                    </div>
                  <div class="col-sm-12">
                    <!-- <button class="btBox">Marcar Consulta</button> -->
                    <a href="javascript:void(0)" title="Marcar Consulta" class="btAgendar btBox">Marcar Consulta</a>
                  </div>

              </div>
                
                <input type='text' name='url-form' class='url-form' value=' ' />
            </form>
            <p>*Após realizar o pré agendamento aguarde a confirmação por um atendente</p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <div class="titulo">
            <h2>NOSSOS VÍDEOS</h2>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
            <div class="owl-carousel owl-theme owl-videos">
              
              <a href="https://www.youtube.com/watch?v=7qHNvVhNxFs" target="_blank" class="item" style="background: url(https://img.youtube.com/vi/7qHNvVhNxFs/maxresdefault.jpg) no-repeat center center;">
                <span class="hover"><i class="fab fa-youtube"></i></span>
              </a>

              <a href="https://www.youtube.com/watch?v=d_Ej_AaID9A" target="_blank" class="item" style="background: url(https://img.youtube.com/vi/d_Ej_AaID9A/maxresdefault.jpg) no-repeat center center;">
                <span class="hover"><i class="fab fa-youtube"></i></span>
              </a>

              <a href="https://www.youtube.com/watch?v=_CsYzzHtmvE" target="_blank" class="item" style="background: url(https://img.youtube.com/vi/_CsYzzHtmvE/maxresdefault.jpg) no-repeat center center;">
                <span class="hover"><i class="fab fa-youtube"></i></span>
              </a>

              <a href="https://www.youtube.com/watch?v=WfoWHf-xds0" target="_blank" class="item" style="background: url(https://img.youtube.com/vi/WfoWHf-xds0/maxresdefault.jpg) no-repeat center center;">
                <span class="hover"><i class="fab fa-youtube"></i></span>
              </a>

              <a href="https://www.youtube.com/watch?v=IdXDrXLUFio" target="_blank" class="item" style="background: url(https://img.youtube.com/vi/d_Ej_AaID9A/maxresdefault.jpg) no-repeat center center;">
                <span class="hover"><i class="fab fa-youtube"></i></span>
              </a>

              <a href="https://www.youtube.com/watch?v=caDtRKHwEuw" target="_blank" class="item" style="background: url(https://img.youtube.com/vi/caDtRKHwEuw/maxresdefault.jpg) no-repeat center center;">
                <span class="hover"><i class="fab fa-youtube"></i></span>
              </a>

              <a href="https://www.youtube.com/watch?v=TfRN-S5czGI" target="_blank" class="item" style="background: url(https://img.youtube.com/vi/TfRN-S5czGI/maxresdefault.jpg) no-repeat center center;">
                <span class="hover"><i class="fab fa-youtube"></i></span>
              </a>
              
              <a href="https://www.youtube.com/watch?v=dnx40OLJbx8" target="_blank" class="item" style="background: url(https://img.youtube.com/vi/dnx40OLJbx8/maxresdefault.jpg) no-repeat center center;">
                <span class="hover"><i class="fab fa-youtube"></i></span>
              </a>
              
              <a href="https://www.youtube.com/watch?v=xm9206Prbf8" target="_blank" class="item" style="background: url(https://img.youtube.com/vi/xm9206Prbf8/maxresdefault.jpg) no-repeat center center;">
                <span class="hover"><i class="fab fa-youtube"></i></span>
              </a>
              
              
            </div>
        </div>
      </div>
    </div>
  </section>

  <section class="calculo footer">
    <div class="container">
      <div class="row justify-content-md-center">
        <div class="col-md-7  justify-content-md-center">
          <div class="bg">
            <div class="tituloDestaque">
              <h2 class="branco">Agende sua consulta</h2>
            </div>
            <div class="form">
              <button class="btBox azul">
				 <a href="http://www.clinicagastrus.com.br/equipe.php?id=37">Agendar</a>
				</button>
            </div>
            <div class="resultado"></div>
          </div>  
        </div>
      </div>
    </div>
  </section>

  <div class="modal fade" id="cesar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <div class="info">
            <h3>César</h3>
          </div>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body conteudo">
          <p>Meu nome é César tenho 48 anos.</p>
          <p>Desde meus 20 anos luto contra a balança, mesmo sempre gostando de fazer esporte, mas com o passar do tempo foi ficando mais difícil pois o peso é limitador para a realização de diversas atividades. Sempre fui preocupado com minha saúde, realizava exames periódicos, e nunca tive nenhum problema, apesar de usar medicamentos para o controle da pressão arterial. Há um tempo vinha amadurecendo a ideia de fazer a cirurgia bariátrica, tendo tomado a decisão final quando no começo do ano, em um exame de rotina, o nível da glicose chegou a um patamar muito alto. Logo marquei a consulta, sendo operado em março deste ano com o Dr. MARCELO PROTASIO, um médico muito capacitado e excepcional. Não posso deixar de falar também da equipe, que com muita competência, auxiliou-me nessa difícil jornada. Sou bastante grato a todos. Hoje com 8 meses de cirurgia e 50 kg a menos estou muito bem, sem tomar nenhum remédio e em um aprendizado continuo. Meu muito obrigado.</p>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="kisia" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <div class="info">
            <h3>Kisia</h3>
          </div>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body conteudo">
          <p>Olá meu nome é Kisia e eu vim aqui contar um pouquinho da minha cirurgia para vocês!</p>
          <p>Fiz cirurgia bariátrica em outubro de 2017, técnica Sleeve! Conheci o Dr. Marcelo Protásio através do meu convênio fui a primeira consulta e logo senti confiança, cheguei com a ideia de fazer a técnica baypass porque achava que emagrecia mais rápido e a perda de peso seria maior e menor tempo, logo Dr. Marcelo me explicou que as vantagens da sleeve e eu achei muito interessante, fiz todos os exames passei com a psicóloga da equipe a Dra Thais que é uma pessoa maravilhosa e super alto astral, com a nutricionista Dra Andrea outra maravilhosa que me ajudou bastante também e me preparei para cirurgia. Chegado o dia eu estava super nervosa e logo encontrei com Dr. Oderlan anestesista da equipe que me acalmou e partimos para o centro cirúrgico! A equipe estava completa e foram bastante atenciosos comigo, tive uma cirurgia tranquila, um pós operatório excelente, com toda atenção e cuidado do Dr. Marcelo que sempre esteve ali para tirar todas as minhas dúvidas que não são poucas até hoje rsrsrs! Enfim em 8 meses atingi o meu peso ideal e venho mantendo, estou muito satisfeita com a técnica que fiz, com a equipe que escolhi  hoje minha vida melhorou 100% e principalmente com meu cirurgião que é um ser humano único sempre atencioso e cuidadoso com todos os pacientes! Obrigada a equipe Gastrus por todo cuidado comigo!</p>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="saulo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <div class="info">
            <h3>Saulo Meneses</h3>
          </div>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body conteudo">
          <p>Sou o Saulo Meneses, tenho 35 anos, altura 1,78 cm, no período de preparação par a cirurgia eu pesava 127 kg, além da obesidade, apresentava diversas comorbidades associadas, tais como: pressão alta, pré-diabético, gordura no fígado, risco de infarto do miocárdio (inclusive dormindo), níveis de triglicerídeos, colesterol e outros, fora das taxas consideradas normais.</p>
          <p>A técnica acordada em conjunto com meu cirurgião foi a Sleeve, realizada em 15/06/2018, após 04 meses já foram eliminados 31 kg. Fiquei internado por apenas dois dias no hospital, e após recebi alta. Não tive nenhuma complicação no pós cirúrgico pois segui todas as orientações passadas pela equipe da Clinica Gastrus. </p>
          <p>Minhas recuperação foi excelente sem dores, sem gases, inchaço, prisão de ventre, vômitos ou qualquer outro tipo de complicações. Nos primeiros 15 dias me alimentava apenas de líquidos, após esse período já comecei a ingerir alimentos pastosos e em seguida sólidos, e meu organismos aceitou sem nenhum problemas.</p>
          <p>Quanto a equipe médica, posso dizer que fui iluminado por Deus no momento da escolha de meu cirurgião Dr. Marcelo Protásio, onde me faltam adjetivos para cortejar e agradecer por toda atenção, compreensão da minha necessidade, confesso que nunca vi em toda minha vida u profissional com tanta atenção, dedicação prestada no pré e pós-cirúrgico. #superrecomendo. Agradeço por toda equipe a Dr. Oderlan Carvalho (Anestesista), Dra. Thais Lopes (Psicóloga), Dra. Andrea Fraga (Nutricionista) ambos fazem parte dessa trajetória de sucesso, confie na equipe que você escolheu, são profissionais capacitados e preparados. Sugiro que não busque informações em grupos de redes sociais, pois nem tudo é verdade, busque fontes seguras. O procedimento foi extraordinariamente bem sucedido! Os profissionais que me acolheram foram de uma responsabilidade imensa.</p>
          <p>Ainda sobre a técnica, gostei muito! Foi sugerida pelo profissional em que no meu caso seria a mais indicada, nessa questão posso aconselhar que o profissional que você escolher com certeza ele terá a melhor opção. Confie!</p>
          <p>Atualmente apenas faço uso de vitaminas as quais são essenciais para qualquer individuo seja ele bariátricado ou não. Estou mais cheio de autoestima, de mais entusiasmo, de mais felicidade! Com mais SAÚDE e disposição, pois os meus índices/níveis em exames demonstraram que em quase 3 meses já foram regularizados.</p>
          <p>Algumas pessoas perguntam se tive medo em realizar a cirurgia, confesso que devido ao fato de nunca ter passado por nenhum procedimento cirúrgico, fiquei um pouco apreensivo, porém acredito que devemos ter mais medo da obesidade, pois esta vai nos matando aos poucos. Minha expectativa para os próximos meses é chegar ao peso definido pela equipe médica, usufruir da liberdade tão sonhada, que já se faz presente. Continuar com a consciência de hoje em me permitir escolhas conscientes que me permitam a tão sonhada longevidade! </p>
          <p>Ressignificando os prazeres, me permitindo um estilo de vida saudável, pra que essa viagem chamada vida, seja ainda mais saborosa, afinal a sensação de você poder ir a uma loja e comprar uma roupa que você tenha prazer em escolher e vestir, e não comprar apenas o que “dar em seu manequim” isso é incontestável.</p>
          <p>Neste espaço, registro minha minha gratidão a Deus, a equipe que acreditou que era possível, a todos que apoiaram, aos que desestimularam (me impulsionou a provar que seria capaz), aos amigos, e especialmente a mim mesma, que não desisti dos meus sonhos. Chega uma hora que ninguém além de você mesmo, pode torná-los reais.</p>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="janeide" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <div class="info">
            <h3>Janeide Marques</h3>
          </div>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body conteudo">
          <p>Meu nome é Janeide Marques, 31 anos.</p>
          <p>A idéia de fazer a cirurgia surgiu da minha endocrinologista com quem já tinha tentado alguns tratamentos ao longo do tempo sem sucesso. A princípio comecei a estudar como seria feita quais tipos de técnicas eram usadas, passei por vários profissionais com especialidades na área até encontrar a equipe do Dr. Marcelo e com essa equipe eu me identifiquei bastante. Entrei no consultório do Dr. Marcelo e ele com sua paciência e humanidade esclareceu todas as dúvidas e já encaminhou ao resto da sua equipe que sem dúvidas é a melhor. A técnica escolhida entre mim e o gastro foi a sleeve, a cirurgia em si superou minhas expectativas  estava muito nervosa no pré operatório e mais uma vez a equipe fez a diferença o Dr.Oderlan Carvalho foi lá e me acalmou com sua paciência e solidariedade. A cirurgia é super tranquila no dia seguinte a cirurgia já queria ir para casa pois me sentia super bem. Meu maior medo era passar pelas restrições alimentares  e principalmente  da dieta líquida e novamente a equipe fez toda a diferença fui muito bem preparada psicologicamente pela Thais Lopes a  melhor psicóloga que tiver o prazer de conhecer. A nutricionista é maravilhosa e sempre esteve presente para qualquer dúvida e sempre me apoiando nessa fase. </p>
          <p>Enfim hoje tenho exatos 8 meses de pós cirúrgico   já atingi minha meta pessoal na última vez que  me pesei já tinha eliminado 37 quilos dos 40 que precisava perder e com isso ganhei uma qualidade de vida que não tem preço que pague.</p>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="tarcio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <div class="info">
            <h3>Tárcio Willy C. Oliveira</h3>
          </div>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body conteudo">
          <p>Tárcio Willy C. Oliveira, 23 anos.</p>
          <p>Faz 7 meses eu fiz a minha cirurgia bariátrica, fiz não por estética, fiz por me ver aos 23 anos, tendo uma saúde ruim, sedentário e me vendo perder o bom da vida, perdi ao total 34kg, cheguei na minha meta e percebo claramente que minha saúde está melhor, minha alto estima, e minha disposição no dia a dia! Eu sempre entendi que a cirurgia não é o milagre da obesidade, até porque, tem que mudar os hábitos, controlar alimentação, fazer exercícios, se alimentar melhor, e o acompanhamento com a nutricionista e a psicóloga foram essenciais para mim, eu precisava entender que obesidade é uma doença, e eu precisava tratar, porque muitas pessoas chegavam para criticar, dizendo que uma dieta resolvia, mas se no meu caso resolvesse eu não precisaria fazer a cirurgia, é importante não ligar para as opniões pois somos nós que sentimos os efeitos da obesidade, e a psicóloga me ajudou nisso e também foi preciso eu compreender os passos do pós operatório, que necessito tomar minhas vitaminas (1 comprimido) diariamente, e seguir todos os passos que o médico mandar. Sou muito grato ao Dr. Marcelo Protasio e toda sua equipe, super prestativos no pré e no pós operatório! Hoje me sinto saudável e feliz como não me via há tempos! </p>
        </div>
      </div>
    </div>
  </div>

  <?php include '_footer.php'; ?>

  <?php include '_scripts-footer.php'; ?>

  <?php
    // Fix Api Whatsapp on Desktops
    // Dev: Jean Livino
    $iphone = strpos($_SERVER['HTTP_USER_AGENT'],"iPhone");
    $android = strpos($_SERVER['HTTP_USER_AGENT'],"Android");
    $palmpre = strpos($_SERVER['HTTP_USER_AGENT'],"webOS");
    $berry = strpos($_SERVER['HTTP_USER_AGENT'],"BlackBerry");
    $ipod = strpos($_SERVER['HTTP_USER_AGENT'],"iPod");

    $caminho;

    // check if is a mobile
    if ($iphone || $android || $palmpre || $ipod || $berry == true)
    {
      //header('Location: https://api.whatsapp.com/send?phone=YOURNUMBER&text=YOURTEXT');
      //OR
      //echo "<script>window.location='https://api.whatsapp.com/send?phone=YOURNUMBER&text=YOURTEXT'</script>";
      $caminho = 'api';
    }

    // all others
    else {
      $caminho = 'web';
      // header('Location: https://web.whatsapp.com/send?phone=YOURNUMBER&text=YOURTEXT');
      //OR
      //echo "<script>window.location='https://web.whatsapp.com/send?phone=YOURNUMBER&text=YOURTEXT'</script>";
    }
  ?>

  <script>
    //agendar
    var nome;
    var telefone;
    var convenio;

    $('.btAgendar').click(function(event) {
      var txt = "Olá. gostaria de marcar uma consulta para: Nome: "+$('#input-nome').val()+" - Telefone: "+$('#input-telefone').val()+" - Convênio: "+$('#input-convenio').val();
      // console.log(txt);
      window.open('https://<?= $caminho ?>.whatsapp.com/send?1=pt_BR&phone=5579988574444&text='+txt, '_blank');
    });
  </script>
     
</body>

</html>
