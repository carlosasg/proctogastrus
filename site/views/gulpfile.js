//npm install gulp-uglify gulp-sass gulp-changed gulp-imagemin gulp-clean-css del gulp-rename gulp-concat gulp-autoprefixer gulp --save-dev

var gulp = require('gulp'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	rename = require('gulp-rename'),
	sass = require('gulp-sass'),
	cleanCSS = require('gulp-clean-css'),
	imagemin = require('gulp-imagemin'),
	autoprefixer = require('gulp-autoprefixer'),
	changed  = require('gulp-changed');
	gzip = require('gulp-gzip');

var sassOptions = {
  errLogToConsole: true,
  outputStyle: 'compressed'
}

// SCRIPTS
gulp.task('scripts', function(){
	gulp.src([
			'assets/js/jquery.js', 
			'assets/js/bootstrap.js', 
			'assets/js/prefixfree.min.js', 
			'assets/js/velocity.min.js', 
			'assets/js/owl.carousel.js', 
			'assets/js/linceform/linceform.js', 
			'assets/js/main.js'])
	    .pipe(concat('all.js'))
	    .pipe(rename({suffix:'.min'}))
		.pipe(uglify())
		.on('error', swallowError)
		.pipe(gzip())
	    .pipe(gulp.dest('assets/js/'));
});

function swallowError (error) {
  // If you want details of the error in the console
  console.log(error.toString())
  this.emit('end')
}

//SASS
gulp.task('sass', function () {
	gulp.src('assets/css/sass/site.scss')
	    .pipe(sass(sassOptions).on('error', sass.logError))
	    .pipe(gulp.dest('assets/css/'));
});

// CSS
gulp.task('css', function(){
	gulp.src([
			'assets/css/bootstrap.css', 
			'assets/css/fontawesome-all.css', 
			'assets/css/owl.carousel.css', 
			'assets/css/owl.theme.default.min.css', 
			'assets/js/linceform/linceform.css',
			'assets/css/site.css'])
	    .pipe(concat('all.css'))
	    .pipe(autoprefixer({
           browsers: ['last 2 versions'],
           cascade: false
   		}))
	    .pipe(rename({suffix:'.min'}))
		.pipe(cleanCSS({compatibility: 'ie8'}))
		.pipe(gzip())
	    .pipe(gulp.dest('assets/css/'));
});

//IMAGES
gulp.task('images', function(tmp) {
    gulp.src(['assets/img/dev/**/*.jpg', 'assets/img/dev/**/*.png'])
        .pipe(changed('assets/img/'))
        .pipe(imagemin({ optimizationLevel: 5, progressive: true, interlaced: true }))
        .pipe(gulp.dest('assets/img/'));
});


//Watch Task
gulp.task('watch', function(){
	gulp.watch('assets/js/main.js', ['scripts']);
	gulp.watch('assets/css/sass/site.scss', ['sass']);
	gulp.watch('assets/css/site.css', ['css']);
});


//Default task
gulp.task('default', ['scripts', 'sass', 'css', 'watch']);