<!DOCTYPE html>
<html lang="en">

<head>

  <?php 
  
  $titlePage = 'PRINCIPAIS DOENÇAS';
  $descriptionPage = 'Núcelo Integrado de Cirurgia Digestiva';
  $imgPage = "https://".$_SERVER["SERVER_NAME"].'/assets/img/social.jpg'; //500x250px
  $siteName = 'Principais Doenças';

  include '_head.php';
      
  ?>

</head>

<body>

  <?php $menu = 'internas'; include '_menu.php'; ?>

  <?php include '_header.php'; ?>

  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-12 conteudo">
          <div class="titulo">
            <h2>Principais Doenças</h2>
          </div>
        </div>
		  <div class="col-md-4 conteudo">
          		<a href="abscesso" class="col-md-3 itemDepoimento doencas" title="Doenças">
				  <div class="borda">
						<h3>Abscesso e fístula anais</h3>
					  <p> Abcessos e fístulas perianais fazem parte da mesma doença, só que em estágios diferentes. O abscesso perianal é uma cavidade cheia de pus próxima da região anal... </p>
					<span>Continuar lendo</span>
				  </div>
				</a>
        	</div>
			<div class="col-md-4 conteudo">
          		<a href="cancer" class="col-md-3 itemDepoimento doencas" title="Doenças">
				  <div class="borda">
						<h3>Câncer Colorretal</h3>
					  <p> É um tumor maligno do intestino grosso e reto e o terceiro câncer mais comum dentre todos os tipos de câncer no Brasil, segundo dados do INCA... </p>
					<span>Continuar lendo</span>
				  </div>
				</a>
        	</div>
			<div class="col-md-4 conteudo">
          		<a href="diverticulos" class="col-md-3 itemDepoimento doencas" title="Doenças">
				  <div class="borda">
						<h3>Divertículos intestinais</h3>
					  <p> Divertículos do intestino são saculações (“saquinhos”) que ocorrem na parede do intestino grosso em decorrência do enfraquecimento da parede intestinal... </p>
					<span>Continuar lendo</span>
				  </div>
				</a>
        	</div>
			<div class="col-md-4 conteudo">
          		<a href="fissura" class="col-md-3 itemDepoimento doencas" title="Doenças">
				  <div class="borda">
						<h3>Fissura Anal</h3>
					  <p> A fissura anal é uma “rachadura”, um “corte” linear no tecido anal, semelhante uma rachadura no canto da boca. Devido à rica inervação da região, é bastante dolorosa... </p>
					<span>Continuar lendo</span>
				  </div>
				</a>
        	</div>
			<div class="col-md-4 conteudo">
          		<a href="hemorroidas" class="col-md-3 itemDepoimento doencas" title="Doenças">
				  <div class="borda">
						<h3>Hemorróidas</h3>
					  <p> As hemorroidas são vasos sanguíneos normais, presentes na região anal. Contudo quando elas se dilatam e começam a prolapsar na região anal, causando sintomas, dizemos então que...</p>
					<span>Continuar lendo</span>
				  </div>
				</a>
        	</div>
			<div class="col-md-4 conteudo">
          		<a href="inflamatorios" class="col-md-3 itemDepoimento doencas" title="Doenças">
				  <div class="borda">
					<h3>Doenças Inflamatórias Intestinais</h3>
					  <p> O termo doenças inflamatórias intestinais se refere basicamente a duas patologias: à doença de crohn e à retocolite ulcerativa...</p>
					<span>Continuar lendo</span>
				  </div>
				</a>
        	</div>
			<div class="col-md-4 conteudo">
          		<a href="endometriose" class="col-md-3 itemDepoimento doencas" title="Doenças">
				  <div class="borda">
					<h3>Endometriose Profunda</h3>
					  <p> A endometriose ocorre quando o tecido que recobre o útero internamente (endométrio) acaba se implantando, de maneira anômala em outros órgãos, provocando aderências...</p>
					<span>Continuar lendo</span>
				  </div>
				</a>
        	</div>
      </div>
    </div>
  </section>

  <?php include '_footer.php'; ?>

  <?php include '_scripts-footer.php'; ?>
     
</body>

</html>
