<!DOCTYPE html>
<html lang="en">

<head>

  <?php 
  
  $titlePage = 'Pós-operatório';
  $descriptionPage = 'Núcelo Integrado de Cirurgia Digestiva';
  $imgPage = "https://".$_SERVER["SERVER_NAME"].'/assets/img/social.jpg'; //500x250px
  $siteName = 'Gastrus';

  include '_head.php';
      
  ?>

</head>

<body>

  <?php $menu = 'internas'; include '_menu.php'; ?>

  <?php include '_header.php'; ?>

  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-12 conteudo">
          <div class="titulo">
            <h2>Pós-operatório</h2>
          </div>
          <h3>Manual do paciente Cirúrgico</h3>
          <p>Clique <a href="assets/arquivos/manual_do_paciente.pdf" target="_blank"> AQUI</a> para baixar</p>
        </div>
      </div>
    </div>
  </section>

  <?php include '_footer.php'; ?>

  <?php include '_scripts-footer.php'; ?>
     
</body>

</html>
