<!DOCTYPE html>
<html lang="en">

<head>

  <?php 
  
  $titlePage = 'Coloproctologia';
  $descriptionPage = 'Núcelo Integrado de Cirurgia Digestiva';
  $imgPage = "https://".$_SERVER["SERVER_NAME"].'/assets/img/social.jpg'; //500x250px
  $siteName = 'Gastrus';

  include '_head.php';
      
  ?>

</head>

<body>

  <?php $menu = 'internas'; include '_menu.php'; ?>

  <?php include '_header.php'; ?>

  <div class="wrap">
    <section>
      <div class="container">
        <div class="row">
          <div class="col-md-12 conteudo">
            <div class="titulo">
              <h2>Coloproctologia</h2>
            </div>
          </div>
        </div>
        <div class="row">
		<div class="col-md-12">
          <p>A Coloproctologia (do grego Kólon- intestino grosso, e proktós, significando ânus e, por extensão, reto;) é a especialidade que cuida das patologias do intestino grosso, reto e ânus.<br>
          </p>
          <p>É uma especialidade tanto clínica quanto cirúrgica, e também envolve a realização de exames como a Colonoscopia, manometria anorretal, ultrassom endoanal, entre outras.<br>
          </p>
          <p>Doenças como Câncer de intestino e de reto, pólipos intestinais, diverticulite, megacólon, doenças inflamatórias intestinais (retocolite e Crohn), constipação/diarréia crônica,distúrbios da evacuação, síndrome do intestino irritável, hemorróidas, fistulas e fissuras anais, cisto pilonidal, incontinência anal entre outras, fazem parte do dia-a-dia dessa especialidade.</p>
          <p>Um fator de confusão muito grande dos pacientes é se essa especialidade cuida da Próstata. Apesar do nome parecido, a especialidade que cuida dessa região é a UROLOGIA, e as duas especialidades fazem o toque retal, só que com objetivos diferentes.<br>
          </p>
          <p>O urologista para avaliar a próstata especificamente; o coloproctologista para avaliar doenças do ânus e reto.</p>
		  </div>
			
		<div class="col-md-12">
			<div class="imgs-video titulo" style="margin-bottom: 3px;">
			  <h2>Quando devo procurar um Coloproctogista?</h2>
			</div>
		</div>
		
		<div class="col-md-12">	
          <ul>
            <li>- Sangramento anal;</li>
            <li>- Dificuldade para evacuar;</li>
            <li>- Dor e cólica abdominal frequentes;</li>
            <li>- Diarreia crônica (por mais de 3 semanas);</li>
            <li>- Alteração do ritmo intestinal;</li>
            <li>- Prisão de ventre (constipação intestinal);</li>
            <li>- Dor ao evacuar ou secreções perianais;</li>
            <li>- Histórico familiar com câncer intestinal ou pólipos;</li>
            <li>- Perda de fezes ou gases em roupa íntima.</li>
          </ul>
        </div>
        </div>
        </div>
    </section>
    
  </div>


  <?php include '_footer.php'; ?>

  <?php include '_scripts-footer.php'; ?>
     
</body>

</html>
