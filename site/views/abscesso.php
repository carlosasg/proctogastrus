<!DOCTYPE html>
<html lang="en">

<head>

  <?php 
  
  $titlePage = 'PRINCIPAIS DOENÇAS - Abscesso e fístula anais';
  $descriptionPage = 'As hemorroidas são vasos sanguíneos normais, presentes na região anal. ';
  $imgPage = "https://".$_SERVER["SERVER_NAME"].'/assets/img/social.jpg'; //500x250px
  $siteName = 'Principais Doenças';

  include '_head.php';
      
  ?>

  <style>
  .row-abscesso{
    margin-right: 15px;
    margin-left: 15px;
  }
  </style>

</head>

<body>

  <?php $menu = 'internas'; include '_menu.php'; ?>

  <?php include '_header.php'; ?>

  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-12 conteudo">
          <div class="titulo">
            <h2>Abscesso e fístula anais</h2>
          </div>
        </div>
      </div>
		<div class="row conteudo row-abscesso">
		  <h3>O que são abscessos e fístulas?</h3>
		  <p> Abcessos e fístulas perianais fazem parte da mesma doença, só que em estágios diferentes. O abscesso perianal é uma cavidade cheia de pus próxima da região anal. Geralmente é a fase aguda da infecção de glândulas localizadas dentro do ânus. Quando essa infeção da glândula anal procura um trajeto para exteriorizar o pus e o faz através de um orifício externo na pele perianal, formou-se uma fístula (um trajeto comunicando o epitélio do canal anal à pele). A fístula é uma fase mais crônica dessa infecção.</p>
		  <h3>Qual a causa?</h3>
		  <p><img src="assets/img/doencas/abscesso-01.jpg"  class="img-fluid float-right" alt=""/>O que se sabe é que pequenas glândulas anorretais infectadas dão origem a abscessos e fístulas, mas a causa exata dessa infecção ainda permanece obscura. Acredita-se que pequenos pedaços de fezes possam obstruir essas glândulas e contaminá-las. Pacientes que possuem Doença de Crohn e apresentam inflamação crônica do reto, são mais predispostos a fístulas. Além disso, existem doenças fistulizantes como actinomicose e tuberculose que podem abrir o quadro com fístulas perianais e abscessos.</p>
		  <h3>Quais os sintomas?</h3>
		  <p><img src="assets/img/doencas/abscesso-02.jpg"  class="img-fluid float-right" alt=""/>Os sintomas de abscesso geralmente são dor e tumefação na região próxima à nádega ou ao ânus. A tumefação é dolorosa, quente e avermelhada, muitas vezes impede o paciente de sentar adequadamente; pode haver ainda febre e calafrios. Os sintomas da fístula são descarga purulenta-sanguinolenta de odor fétido pelo orifício externo, desconforto na região da nádega afetada, geralmente aliviado por descarga purulenta. O abscesso perianal é uma URGÊNCIA e o médico especialista deve ser consultado o quanto antes. Caso não seja possível agendamento de consulta, o paciente deve procurar um setor de urgência do hospital, principalmente se o paciente for diabético ou tiver usando medicações que reduzem a imunidade. A fístula não tem caráter de urgência e pode esperar por uma consulta na maioria das vezes</p>
		  <h3>Qual é o Tratamento?</h3>
		  <p>O tratamento do abscesso é a drenagem cirúrgica do pus, geralmente sob anestesia local ou bloqueio raquimedular, através de uma incisão pequena na pele. Às vezes é deixado um dreno na região para facilitar a cura. Em pacientes diabéticos e que usam medicações que interferem na imunidade pode ser necessário uso de antibióticos. </p>
		  <p>O tratamento da fístula baseia-se em abrir o trajeto e deixa-lo exposto para que cicatrize de dentro para fora. Porém, esse tratamento pode ser bem mais complexo e delicado por envolver o esfíncter anal nos trajetos e pelo risco de incontinência se forem abertos, então muitas vezes é necessário utilizar outras técnicas. Os benefícios e riscos devem ser discutidos com o seu especialista. Em geral, ambas as cirurgias podem sem realizadas em regime de hospital-dia.</p>
		  <h3>&nbsp;</h3>
			<!-- <p style="font-style: italic; font-size: 12px;">DR. FELIPE AUGUSTO DO PRADO TORRES e DR. JUVENAL DA ROCHA TORRES NETO</p> -->
			
      </div>
    </div>
  </section>

  <?php include '_footer.php'; ?>

  <?php include '_scripts-footer.php'; ?>
     
</body>

</html>
