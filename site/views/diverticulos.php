<!DOCTYPE html>
<html lang="en">

<head>

  <?php 
  
  $titlePage = 'PRINCIPAIS DOENÇAS - Divertículos intestinais';
  $descriptionPage = 'As hemorroidas são vasos sanguíneos normais, presentes na região anal. ';
  $imgPage = "https://".$_SERVER["SERVER_NAME"].'/assets/img/social.jpg'; //500x250px
  $siteName = 'Principais Doenças';

  include '_head.php';
      
  ?>

<style>
  .row-abscesso{
    margin-right: 15px;
    margin-left: 15px;
  }
  </style>
  
</head>

<body>

  <?php $menu = 'internas'; include '_menu.php'; ?>

  <?php include '_header.php'; ?>

  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-12 conteudo">
          <div class="titulo">
            <h2>Divertículos intestinais</h2>
          </div>
        </div>
      </div>
		<div class="row conteudo row-abscesso">
		  <h3>O que são divertículos?</h3>
		  <p>Divertículos do intestino são saculações (“saquinhos”) que ocorrem na parede do intestino grosso em decorrência do enfraquecimento da parede intestinal. Eles são muito comuns na população e podem ser assintomáticos, só provocando sintomas quando inflamam (diverticulite) ou quando sangram (hemorragia digestiva).</p>
		  <h3>Qual a causa?</h3>
		  <p>Os dois fatores relacionados ao aparecimento dessa patologia são a idade avançada (&gt;60 anos), por envelhecimento e enfraquecimento da musculatura intestinal e a dieta pobre em fibras e rica em massas, pães, farinha, bolachas. A ausência de fibras dificulta o intestino grosso a empurrar o bolo fecal e devido ao esforço excessivo da musculatura intestinal, ela acaba por ter zonas de enfraquecimento que empurram a parede do intestino para fora formando os divertículos.</p>
		  <h3>Quais os sintomas?</h3>
		  <p><img src="assets/img/doencas/diverticulos-01.jpg"  class="img-fluid float-right" alt=""/>A presença dos divertículos pode ser completamente assintomática, mas alguns pacientes podem apresentar cólicas intestinais leves ou prisão de ventre (constipação). Os sintomas mais exuberantes só surgem quando tem alguma complicação. Na diverticulite, os sintomas típicos são: dor abdominal moderada a intensa no lado esquerdo e inferior do abdome, febre, náuseas e calafrios. Na hemorragia digestiva, o paciente apresenta sangramento retal de média a grande quantidade, geralmente vermelho vinhoso ou vermelho vivo, indolor. IMPORTANTE: nem todos que possuem divertículos apresentarão essas complicações; apenas têm que estar cientes do risco de elas acontecerem. Caso exista suspeita dessas complicações o paciente deve procurar uma URGÊNCIA ou contatar o seu<br>
	      coloproctologista para atendimento o quanto antes. Muitas vezes é necessária a INTERNAÇÃO HOSPITALAR.</p>
		  <h3>Qual o tratamento?</h3>
		  <p><img src="assets/img/doencas/diverticulos-02.jpg"  class="img-fluid float-right" alt=""/>Uma vez que já se formaram os divertículos não há como fazê-los desaparecer. O melhor tratamento é a prevenção. Apenas orientamos dieta rica em fibras para facilitar o bom funcionamento intestinal e evitar aparecimento de novos divertículos. Quando há diverticulite, por se tratar de uma infecção, é necessário uso de antibióticos, por via oral ou venosa a depender da gravidade da inflamação, e dieta líquida. Quando é necessária a cirurgia? Em alguns casos mais graves, que necessitam da retirada do órgão acometido (geralmente cólon sigmoide) com ou sem a confecção de colostomia temporária; ou em casos de diverticulites recorrentes. </p>
		  <p>Quando há hemorragia digestiva, após estabilização inicial, o paciente deve ser submetido a colonoscopia para encontrar o local do sangramento e muitas vezes já tratá-lo na própria colonoscopia. Felizmente, na maioria das vezes o sangramento é auto-limitado (pára sem tratamento). Em raros casos, quando não se encontra o foco específico e o paciente continua sangrando, o paciente pode necessitar de cirurgia para remover todo o intestino grosso. As cirurgias podem ser realizadas por laparoscopia em casos selecionados.<br>
	      </p>
		  <h3>&nbsp;</h3>
			<p style="font-style: italic; font-size: 12px;">DR. FELIPE AUGUSTO DO PRADO TORRES e DR. JUVENAL DA ROCHA TORRES NETO</p>

      </div>
    </div>
  </section>

  <?php include '_footer.php'; ?>

  <?php include '_scripts-footer.php'; ?>
     
</body>

</html>
