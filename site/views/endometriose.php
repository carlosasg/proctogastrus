<!DOCTYPE html>
<html lang="en">

<head>

  <?php 
  
  $titlePage = 'PRINCIPAIS DOENÇAS - Endometriose Profunda';
  $descriptionPage = 'A endometriose ocorre quando o tecido que recobre o útero internamente. ';
  $imgPage = "https://".$_SERVER["SERVER_NAME"].'/assets/img/social.jpg'; //500x250px
  $siteName = 'Principais Doenças';

  include '_head.php';
      
  ?>

<style>
  .row-abscesso{
    margin-right: 15px;
    margin-left: 15px;
  }
  </style>

</head>

<body>

  <?php $menu = 'internas'; include '_menu.php'; ?>

  <?php include '_header.php'; ?>

  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-12 conteudo">
          <div class="titulo">
            <h2>Endometriose Profunda</h2>
          </div>
        </div>
      </div>
		<div class="row conteudo row-abscesso">


		  <h3>Por que ocorre a endometriose?</h3>
		  <p>A endometriose ocorre quando o tecido que recobre o útero internamente (endométrio) acaba se implantando, de maneira anômala em outros órgãos, provocando aderências, distorção da anatomia e levando a alteração da função dos sistemas digestório, reprodutor e urinário femininos.  Os locais mais comuns de implantes de endometriose profunda são o reto ou sigmoide (intestino grosso acima do reto), os ovários e trompas, os nervos pélvicos, os ureteres (estruturas que levam a urina do rim ate a bexiga) , a bexiga. Outros órgãos também podem ser afetados, como o apêndice e o intestino delgado.</p>
      <p>A consequência disso são sintomas que envolvem principalmente esses três sistemas, por isso esta patologia necessita de  uma equipe multidisciplinar (ginecologista, radiologista, coloproctologista e as vezes urologista) para adequado tratamento. Na nossa clinica temos equipe composta por todos esses profissionais e ocorre uma discussão das estratégias de tratamento antes de cada procedimento.</p>

      <h3>Quais os sintomas?</h3>
		  <p>Os sintomas mais comuns que levam a paciente ao consultório medico são as dores e a infertilidade. As dores geralmente são pélvicas crônicas, associadas ou não a menstruação, mas as pacientes também podem se queixar de dores nas relações sexuais, dores ao evacuar e ao urinar. Há também outros sintomas que podem levar ao diagnostico, como  o aumento de fluxo menstrual, a constipação, distúrbios urinários.</p>
      <p>Comumente, as pacientes referem um período médio de 2-3 anos de sintomas ate o diagnostico correto. Por isso a procura por um grupo especializado em endometriose é fundamental.</p>


      <h3>Como é feito o diagnostico?</h3>
      <p>Os principais exames indicados são a Ultrassonografia Transvaginal com Preparo Intestinal (exame especifico para pesquisa de endometriose, realizado por profissional treinado) , a Ressonância Magnética de Pelve e dosagem de alguns marcadores sanguíneos. No caso de suspeita de endometriose intestinal, uma colonoscopia geralmente é indicada.</p>
      <p>Contudo, sempre informamos aos doentes, que o padrão-ouro para o acurado diagnostico é a Laparoscopia (cirurgia com passagem de uma câmera através do umbigo para visualização direta da cavidade abdominal), pois não raras vezes, ocorre uma dissociação  entre os achados nos exames complementares e os achados na hora de cirurgia.</p>

      <h3>Qual é o Tratamento?</h3>
      <p>O tratamento da endometriose profunda normalmente é cirúrgico. Embora alguns casos específicos se beneficiem de terapia hormonal, normalmente quando falamos em endometriose profunda, já ocorreu distorção da anatomia e múltiplas aderências estão presentes, necessitando de correção cirúrgica. A cirurgia consiste em soltar as aderências, refazendo a anatomia, e remover os focos por completo com margem. No caso da endometriose intestinal, há 3 tipos de procedimento possíveis. O mais simples deles é o “shaving”, do inglês raspar, no qual o cirurgião superficialmente “raspa” e retira o tecido doente do órgão onde havia o implante. O segundo, é a ressecção discoide, indicado para implantes de 1 -3 cm, no qual é necessário retirar um pedaço do intestino ou reto em forma de moeda que precisa depois ser costurado (suturado). E por fim, o terceiro tipo envolve a retirada de um cilindro de reto ou intestino (ressecção segmentar), e está reservado para os casos com nódulos maiores que 3cm.</p>

      <!-- <div class="row">
        <div class="col-md-12">
        <iframe width="100%" height="600" src="https://www.youtube.com/embed/slzja37r5vk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div> -->
    </div>
  </section>

  <?php include '_footer.php'; ?>

  <?php include '_scripts-footer.php'; ?>
     
</body>

</html>
