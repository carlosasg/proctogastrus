<!DOCTYPE html>
<html lang="en">

<head>

  <?php include "_head.php"; ?>

  <link href="<?php echo ASSETS; ?>css/print.css" rel="stylesheet">

  <title>Propagtur</title>

</head>

<body>
    
    <?php include "_social.php"; ?>
    
    <?php include "_menu.php"; ?>

    <header class="internas" style="background: url(<?php echo ASSETS; ?>img/header.jpg) no-repeat center center">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2>PACOTES</h2>
                </div>
            </div>
        </div>
    </header>
<?php 
if($pac[0]["intercambio"] == 'S'){
  ?>
    <section class="internas">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                <?php 
                for ($i=0; $i < count($pac[1]); $i++) { 
                  if($pac[1][$i]["capa"] == 'N'){
                    echo "<img style='width:100%;' src='".IMAGEM_THUMB; ?>pacotes/<?php echo $pac[0]["idsite"]; ?>/<?php echo $pac[0]["idpacote"]; ?>/<?php echo $pac[1][$i]["avatar_servidor"]."' />"; 
                  }

                ?>
                <?php 
                }
                ?>
                </div>
            </div>
        </div>
    </section>
  <?php 
}
else{

?>    
    <section class="internas">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 titulo">
                    <h3>EU NÃO CRIO RAIZ.</h3>
                    <h2>O MEU CHÃO É O MUNDO</h2>
                </div>
            </div>
            <div class="row">
              <div class="col-sm-12 text-center btsApoio">
                <a href="#" title="Enviar para um amigo" class="btAro" data-toggle="modal" data-target="#enviarAmigo">ENVIAR PARA UM AMIGO</a>
                <a href="#" title="Imprimir" class="btAro" onclick="window.print();">IMPRIMIR</a>
                <a href="#" title="Faça sua Reserva" class="btAro" data-toggle="modal" data-target="#reserva">FAÇA SUA RESERVA</a>
              </div>
                <div class="col-md-8 col-sm-6">
                    <div class="galeria">
                        <div id="slideshow-1">
                          <p>
                              <a href="#" class="cycle-prev"><i class="fa fa-angle-left"></i></a>
                              <a href="#" class="cycle-next"><i class="fa fa-angle-right"></i></a>
                          </p>
                          <div id="cycle-1" class="cycle-slideshow"
                               data-cycle-slides= "> .display-element"
                               data-cycle-prev=".cycle-prev"
                               data-cycle-next=".cycle-next"
                               data-cycle-caption="#slideshow-1 .custom-caption"
                               data-cycle-pause-on-hover="true"
                               data-cycle-fx="fade" >
                              <?php 
                              if(is_array($pac[1]))
                              {
                               foreach($pac[1] as $ind => $pacotes)
                               {    
                                  if($pacotes['avatar_tipo']!="application/pdf")
                                  {
                              ?>
                              <div class="display-element" style="background:url(<?php echo IMAGEM_THUMB; ?>pacotes/<?php echo $idsite; ?>/<?php echo $idpacote; ?>/<?php echo $pacotes["avatar_servidor"]; ?>&altura=485&largura=750);"></div>
                              <?php 
                                   }
                                }
                              }
                              ?>
                          </div>
                        </div>

                        <!-- <div id="slideshow-2">
                          <div id="cycle-2" class="cycle-slideshow"
                               data-cycle-slides= "> .display-element"
                               data-cycle-caption="#slideshow-2 .custom-caption"
                               data-cycle-fx="carousel"
                               data-cycle-carousel-visible="6"
                               data-cycle-carousel-fluid=true
                               data-allow-wrap="false" >
                              <?php 
                              if(is_array($pac[1]))
                              {
                               foreach($pac[1] as $ind => $pacotes)
                               {    
                                  if($pacotes['avatar_tipo']!="application/pdf")
                                  {
                              ?>
                              <div class="display-element" >
                                  <img src="<?php echo IMAGEM_THUMB; ?>pacotes/<?php echo $idsite; ?>/<?php echo $idpacote; ?>/<?php echo $pacotes["avatar_servidor"]; ?>&altura=50&largura=125" alt="">
                              </div>
                              <?php 
                                   }
                                }
                              }
                              ?>
                          </div>
                        </div> -->
                      </div>
                </div>
                <div class="col-md-4 col-sm-6 mt30-xs infoPacote leituraConteudo">
                    <h3><?php echo $pac[0]["nome"];?></h3>
                    <h4><?php echo $categoria["nome"]; ?></h4>
                    <p><?php echo $pac[0]["resumo"];?></p>
                    <p><?php echo $pac[0]["preco_pessoa"];?></p>
                    <a href="#" title="QUERO RESERVAR" class="btBox laranja" data-toggle="modal" data-target="#reserva">QUERO RESERVAR</a>
                </div>
            </div>
        </div>
    </section>
    <section class="internas">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel-group" id="accordion">

                        <?php
                          if($pac[0]["apartamento"]){
                        ?>

                        <div class="panel panel-default">
                          <div class="panel-heading">
                              <a data-toggle="collapse" data-parent="#accordion" href="#periodo-acomodacao">ACOMODAÇÃO
                              <span class="bgIconAccordion pull-right"><i class="fa fa-angle-down rotate down"></i></span>
                              </a>
                          </div>
                          <div id="periodo-acomodacao" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <p><?php echo $pac[0]["apartamento"];?></p>
                            </div>
                          </div>
                        </div>

                        <?php
                          }
                        ?>                        

                        <?php
                          if($pac[0]["introducao"]){
                        ?>
                        <div class="panel panel-default">
                          <div class="panel-heading">
                              <a data-toggle="collapse" data-parent="#accordion" href="#pacote">PACOTE
                              <span class="bgIconAccordion pull-right"><i class="fa fa-angle-down rotate"></i></span>
                              </a>
                          </div>
                          <div id="pacote" class="panel-collapse collapse">
                            <div class="panel-body">
                              <p><?php echo $pac[0]["introducao"];?></p>
                            </div>
                          </div>
                        </div>

                        <?php
                          }
                        ?>

                        <?php
                          if($pac[0]["pacote_inclui"]){
                        ?>

                        <div class="panel panel-default">
                          <div class="panel-heading">
                              <a data-toggle="collapse" data-parent="#accordion" href="#pacote_inclui">PACOTE INCLUI
                              <span class="bgIconAccordion pull-right"><i class="fa fa-angle-down rotate"></i></span>
                              </a>
                          </div>
                          <div id="pacote_inclui" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p><?php echo $pac[0]["pacote_inclui"];?></p>
                            </div>
                          </div>
                        </div>

                        <?php
                          }
                        ?>                        

                        <?php
                          if($pac[0]["pacote_nao_inclui"]){
                        ?>

                        <div class="panel panel-default">
                          <div class="panel-heading">
                              <a data-toggle="collapse" data-parent="#accordion" href="#pacote_nao_inclui">PACOTE NÃO INCLUI
                              <span class="bgIconAccordion pull-right"><i class="fa fa-angle-down rotate"></i></span>
                              </a>
                          </div>
                          <div id="pacote_nao_inclui" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p><?php echo $pac[0]["pacote_nao_inclui"];?></p>
                            </div>
                          </div>
                        </div>

                        <?php
                          }
                        ?>                        

                        <?php
                          if($pac[0]["periodo"]){
                        ?>

                        <div class="panel panel-default">
                          <div class="panel-heading">
                              <a data-toggle="collapse" data-parent="#accordion" href="#periodo">PERÍODO
                              <span class="bgIconAccordion pull-right"><i class="fa fa-angle-down rotate down"></i></span>
                              </a>
                          </div>
                          <div id="periodo" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p><?php echo $pac[0]["periodo"];?></p>
                            </div>
                          </div>
                        </div>

                        <?php
                          }
                        ?>

                        <?php
                          if($pac[0]["roteiro"]){
                        ?>

                        <div class="panel panel-default">
                          <div class="panel-heading">
                              <a data-toggle="collapse" data-parent="#accordion" href="#roteiro">ROTEIRO
                              <span class="bgIconAccordion pull-right"><i class="fa fa-angle-down rotate"></i></span>
                              </a>
                          </div>
                          <div id="roteiro" class="panel-collapse collapse">
                            <div class="panel-body">
                              <?php echo $pac[0]["roteiro"];?>
                            </div>
                          </div>
                        </div>

                        <?php
                          }
                        ?>

                        <?php
                          if($pac[0]["observacao"]){
                        ?>

                        <div class="panel panel-default">
                          <div class="panel-heading">
                              <a data-toggle="collapse" data-parent="#accordion" href="#observacao">OBSERVAÇÃO
                              <span class="bgIconAccordion pull-right"><i class="fa fa-angle-down rotate"></i></span>
                              </a>
                          </div>
                          <div id="observacao" class="panel-collapse collapse">
                            <div class="panel-body">
                              <?php echo $pac[0]["observacao"];?>
                            </div>
                          </div>
                        </div>

                        <?php
                          }
                        ?>

                        <?php
                          if($pac[0]["observacao2"]){
                        ?>

                        <div class="panel panel-default">
                          <div class="panel-heading">
                              <a data-toggle="collapse" data-parent="#accordion" href="#observacao2">OBSERVAÇÃO ADICIONAL
                              <span class="bgIconAccordion pull-right"><i class="fa fa-angle-down rotate"></i></span>
                              </a>
                          </div>
                          <div id="observacao2" class="panel-collapse collapse">
                            <div class="panel-body">
                              <?php echo $pac[0]["observacao2"];?>
                            </div>
                          </div>
                        </div>

                        <?php
                          }
                        ?>

                        <?php
                          if($pac[0]["forma_pagamento"]){
                        ?>

                        <div class="panel panel-default">
                          <div class="panel-heading">
                              <a data-toggle="collapse" data-parent="#accordion" href="#forma-pagamento">FORMAS DE PAGAMENTO
                              <span class="bgIconAccordion pull-right"><i class="fa fa-angle-down rotate"></i></span>
                              </a>
                          </div>
                          <div id="forma-pagamento" class="panel-collapse collapse">
                            <div class="panel-body">
                            <?php
                             if(is_array($forma))
                             { 
                              foreach($forma as $ind2 => $formas)
                              {  
                            ?>
                                  <img alt="<?php echo $formas["idforma_pagamento"]; ?>.jpg" src="http://sgw.propagtur.com.br/storage/inc_thumb3.php?&arquivo=formas/<?php echo $formas["idforma_pagamento"]; ?>.jpg&largura=45" style="margin-right:10px;" >
                               <? 
                                  }     
                               } 
                               ?>
                               <?php echo utf8_decode($pac[0]["forma_pagamento"]);?>
                              <p><span>*OBS:</span><?php echo utf8_decode($pac[0]["observacao"]);?></p>
                            </div>
                          </div>
                        </div>

                        <?php
                          }
                        ?>

                        <?php
                          if($pac[0]["informacoes_reservas"]){
                        ?>

                        <div class="panel panel-default">
                          <div class="panel-heading">
                              <a data-toggle="collapse" data-parent="#accordion" href="#informacoes-reservas">INFORMAÇÕES E RESERVAS
                              <span class="bgIconAccordion pull-right"><i class="fa fa-angle-down rotate"></i></span>
                              </a>
                          </div>
                          <div id="informacoes-reservas" class="panel-collapse collapse">
                            <div class="panel-body">
                              <?php echo $pac[0]["informacoes_reservas"];?>
                            </div>
                          </div>
                        </div>

                        <?php
                          }
                        ?>


                    </div>
                </div>
            </div>
        </div>
    </section>
<?php 
}
?>
    <section class="internas">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 subTitulo">
                    <h3>PACOTES RELACIONADOS</h3>
                </div>
                <div class="col-sm-6 btSubtitulo">
                    <a href="/pacote-listagem/<?php echo $pac[0]["idcategoria"];?>/<?php echo $url[0];?>" title="VER TODOS" class="btAro">VER TODOS</a>
                </div>
            </div>
            <div class="row">
                <?php
                  if(is_array($pacoteOutros))
                  {
                     foreach($pacoteOutros as $ind1 => $pacotes)
                     { 
                ?>
                
                <div class="col-md-3 col-sm-6 mt20">
                    <div class="item itemBox verde">
                        <a href="/<?=$paccat[$pacotes[0]['idcategoria']];?>/<?php echo $pacotes[0]["idpacote"]; ?>/<? echo geraUrlLimpa($pacotes[0]["nome"]); ?>" title="Quero Conhecer">
                            <div class="img" style="background:url(<?php echo IMAGEM_THUMB; ?>pacotes/<?php echo $pacotes[0]["idsite"]; ?>/<?php echo $pacotes[0]["idpacote"]; ?>/<?php echo $pacotes[1][0]["avatar_servidor"]; ?>&altura=180&largura=275) no-repeat center center;">
                                <div class="hover">
                                    <span>Quero conhecer</span>
                                </div>
                            </div>
                            <div class="info">
                              <h4><?php echo $pacotes[0]["nome"]; ?></h4>
                              <p><?php echo $pacotes[0]["entrada"]; ?></p>
                              <p> <?php if($pacotes[0]["entrada"]){ echo '+'; } if($pacotes[0]["parcelas"]){ echo $pacotes[0]["parcelas"].'x no cartão'; }?><span><?php echo $pacotes[0]["valorparcela"]; ?></span></p>
                            </div>
                        </a>
                    </div>
                </div>

                <?php
                    }
                }
                ?>
            </div>
        </div>
    </section>

    <section class="print">
      <div class="container">
        <div class="row">
          <div class="col-sm-6 marcaTopoPrint">
            <img src="<?php echo ASSETS; ?>img/marca-topo-cor.png">
          </div>
          <div class="col-sm-6 telPrint">
              <p>COMPRE PELO TELEFONE</p>
              <h2><span>79</span> 2107-4444</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <div class="bgPrint">
              <img src="<?php echo IMAGEM_THUMB; ?>pacotes/<?php echo $pac[0]["idsite"]; ?>/<?php echo $pac[0]["idpacote"]; ?>/<?php echo $pac[1][0]["avatar_servidor"]; ?>">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12 infoPacote">
            <h3><?php echo $pac[0]["nome"];?></h3>
            <h4><?php echo $categoria["nome"]; ?></h4>
            <p><?php echo $pac[0]["resumo"];?></p>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-12 infoPacote">
            <h4>PERÍODO</h4>
            <p><?php echo $pac[0]["periodo"];?></p>
            <h4>PACOTE INCLUI</h4>
            <p><?php echo $pac[0]["pacote_inclui"];?></p>            
            <h4>PACOTE NÃO INCLUI</h4>
            <p><?php echo $pac[0]["pacote_nao_inclui"];?></p>
            <h4>ROTEIRO</h4>
            <p><?php echo $pac[0]["roteiro"];?></p>
            <h4>FORMAS DE PAGAMENTO</h4>
            <?php
             if(is_array($forma))
             { 
              foreach($forma as $ind2 => $formas)
              {  
            ?>
                  <img alt="<?php echo $formas["idforma_pagamento"]; ?>.jpg" src="http://sgw.propagtur.alfamaweb.com.br/storage/inc_thumb3.php?&arquivo=formas/<?php echo $formas["idforma_pagamento"]; ?>.jpg&largura=45" style="margin-right:10px;" >
               <? 
                  }     
               } 
               ?>
               <?php echo utf8_decode($pac[0]["forma_pagamento"]);?>
              <p><span>*OBS:</span><?php echo utf8_decode($pac[0]["observacao"]);?></p>
            <h4>INFORMAÇÕES E RESERVAS</h4>
            <p><?php echo $pac[0]["informacoes_reservas"];?></p>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-12 infoPacote">
            <h4>NOSSOS ENDEREÇOS</h4>

            <h5 class="negativo">MATRIZ</h5>
            <p class="negativo">TEL: (79) 2107-4444 FAX: (79) 2107-4440 comercial@propagtur.com.br ARACAJU - SE</p>
            <h5 class="negativo">FILIAL 01 – AEROPORTO</h5>
            <p class="negativo">TEL: (79) 3179-4664 FAX: (79) 3179-4665 aeroporto@propagtur.com.br ARACAJU - SE</p>
            <h5 class="negativo">FILIAL 02 – SALVADOR</h5>
            <p class="negativo">TEL: (71) 3341-1633 FAX: (71) 3016-1679 salvador@propagtur.com.br SALVADOR - BA</p>
            <h5 class="negativo">FILIAL 03 – MANAUS</h5>
            <p class="negativo">TEL: (92) 3321-2795 FAX: (92) 3307-7268 manaus@propagtur.com.br MANAUS - AM</p>
            <h5 class="negativo">FILIAL 04 - MACEIÓ</h5>
            <p class="negativo">TEL: (82) 3316-5457 FAX: (82) 3316-5458 maceio@propagtur.com.br MACEIÓ - AL</p>
          </div>
        </div>
      </div>
    </section>

    <?php include "_footer.php"; ?>



    <!-- Modal -->
    <div id="enviarAmigo" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close fecharModal" data-dismiss="moadl">&times;</button>
            <h4 class="modal-title">ENVIE PARA UM AMIGO</h4>
          </div>
          <div class="modal-body">
            <p>Compartilhe esse pacote com um amigo.</p>

            <form id="contato"
                action='/<?php echo $url[0]; ?>/<?php echo $url[1] ?>/<?php echo $url[2] ?>'
                method='post'
                class="form-guia formModal" 
                enctype='multipart/form-data'
                data-sucess='Mensagem enviada com sucesso'
                data-error='Problemas ao enviar a mensagem'
                 >
                  <input type="hidden" name="assunto" value="<?php echo $pac[0]["nome"];?> - Indique para um amigo Propagtur">
                  <input type='hidden' name='subject' value='Indique para um amigo Propagtur'>
                  <input type='hidden' name='site' value='<?php echo "http://".$_SERVER["SERVER_NAME"].strtok($_SERVER ["REQUEST_URI"],"?"); ?>'>

                  <!-- Input Text -->
                  <div class="row">
                      <div class="col-sm-12">
                        <div class='lince-input'>
                            <label for='input-nome'></label>
                            <input type='text' name='nome' placeholder='Nome' id='input-nome' maxlength='100' class='input-alpha' required>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class='lince-input'>
                            <label for='input-email'></label>
                            <input type='text' name='email' placeholder='E-mail' id='input-email' maxlength='50' class='input-email' required>
                        </div>
                      </div>
                      <div class="col-sm-12 text-right">
                      <div class="g-recaptcha" data-sitekey="6LdmXBAUAAAAADjtncVuIotbzYQ_Dw68d8QB2NNt"></div>
                    </div>
                    <div class="col-sm-12 text-right">
                      <input type='submit' value='Compartilhar' class="btBox verde">
                    </div>

                </div>
                  
                  <!-- Anti-Spam -->
                  <input type='text' name='url-form' class='url-form' value=' ' />
              </form>
          </div>
        </div>

      </div>
    </div>


    <!-- Modal -->
    <div id="reserva" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close fecharModal" data-dismiss="moadl">&times;</button>
            <h4 class="modal-title">Reserva</h4>
          </div>
          <div class="modal-body">
            <p>Para reservar um pacote, preencha o formulário abaixo.</p>

            <form id="reserva"
                action='/<?php echo $url[0]; ?>/<?php echo $url[1] ?>/<?php echo $url[2] ?>'
                method='post'
                class="form-guia formModal" 
                enctype='multipart/form-data'
                data-sucess='Mensagem enviada com sucesso'
                data-error='Problemas ao enviar a mensagem'
                 >
                 <input type="hidden" name="assunto" value="<?php echo $pac[0]["nome"];?> - Reserva Propagtur">
                  <input type='hidden' name='subject' value='<?php echo $pac[0]["nome"];?> - Reserva Propagtur'>
                  <input type='hidden' name='site' value='<?php echo "http://".$_SERVER["SERVER_NAME"].strtok($_SERVER ["REQUEST_URI"],"?"); ?>'>

                  <!-- Input Text -->
                  <div class="row">
                      <div class="col-sm-12">
                        <div class='lince-input'>
                            <label for='input-nome'></label>
                            <input type='text' name='nome' placeholder='Nome' id='input-nome' maxlength='100' class='input-alpha' required>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class='lince-input'>
                            <label for='input-email'></label>
                            <input type='text' name='email' placeholder='E-mail' id='input-email' maxlength='50' class='input-email' required>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class='lince-input'>
                            <label for='input-tel'></label>
                            <input type='text' name='telefone' placeholder='Telefone' id='input-tel' maxlength='14' class='input-tel' required>
                        </div>
                      </div>
                      <div class="col-sm-12 text-left">
                        <div class="g-recaptcha" data-sitekey="6LdmXBAUAAAAADjtncVuIotbzYQ_Dw68d8QB2NNt"></div>
                    </div>
                    <div class="col-sm-12 text-left">
                      <input type='submit' value='Reservar' class="btBox verde">
                    </div>

                </div>
                  
                  <!-- Anti-Spam -->
                  <input type='text' name='url-form' class='url-form' value=' ' />
              </form>
          </div>
        </div>

      </div>
    </div>



    <?php include "_scripts-footer.php"; ?>

    <script>
        //ICONES
        function toggleChevron(e) {
          $(e.target)
              .prev('.panel-heading')
              .find("i.rotate")
              .toggleClass('down');
        }
        $('#accordion').on('hidden.bs.collapse', toggleChevron);
        $('#accordion').on('shown.bs.collapse', toggleChevron);

        //bts mapa
        $(".btsMapa li").click(function() {
            $(".btsMapa li").find('a').removeClass('active')
            $(this).find('a').addClass('active')
        });

        //GALERIA
        jQuery(document).ready(function($){

        var slideshows = $('.cycle-slideshow').on('cycle-next cycle-prev', function(e, opts) {
            // advance the other slideshow
            slideshows.not(this).cycle('goto', opts.currSlide);
        });

        $('#cycle-2 .cycle-slide').click(function(){
            var index = $('#cycle-2').data('cycle.API').getSlideIndex(this);
            slideshows.cycle('goto', index);
        });

        });
    </script>

</body>

</html>
