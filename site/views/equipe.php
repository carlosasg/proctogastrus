<!DOCTYPE html>
<html lang="en">

<head>

  <?php 
  
  $titlePage = 'EQUIPE Cirúrgica';
  $descriptionPage = 'Núcelo Integrado de Cirurgia Digestiva';
  $imgPage = "https://".$_SERVER["SERVER_NAME"].'/assets/img/social.jpg'; //500x250px
  $siteName = 'Equipe';

  include '_head.php';
      
  ?>

</head>

<body>

  <?php $menu = 'internas'; include '_menu.php'; ?>

  <?php include '_header.php'; ?>

  
  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-12 conteudo">
          <div class="titulo">
            <h2>EQUIPE Cirúrgica</h2>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <img src="assets/img/equipe-cirurgica2.jpg" class="img-fluid" style="width: 100%;" title="Equipe Cirúrgica" alt="equipe-cirurgica">
        </div>
      </div>
    </div>
  </section>
	
	<section>
    <div class="container">
      <div class="row">
        <div class="col-md-12 conteudo">
          <div class="titulo">
            <h2>EQUIPE MULTIDISCIPLINAR</h2>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-3 col-md-6 itemEquipe">
          <div class="bg" style="background: url(assets/img/beatriz-leite.jpg) no-repeat center center;"></div>
          <div class="info">
            <h3>Beatriz Leite</h3>
            <!-- <span>CRM 000</span> -->
            <p>Nutricionista</p>
            <a href="javascript:void(0)" data-toggle="modal" data-target="#beatriz-leite" title="Ver Detalhes" class="btAro">Ver Detalhes</a>
          </div>
        </div>

        <!-- <div class="col-lg-3 col-md-6 itemEquipe">
          <div class="bg" style="background: url(assets/img/andrea-fraga.jpg) no-repeat center center;"></div>
          <div class="info">
            <h3>Andrea Fraga</h3>
            <p>Nutricionista</p>
            <a href="javascript:void(0)" data-toggle="modal" data-target="#andrea-fraga" title="Ver Detalhes" class="btAro">Ver Detalhes</a>
          </div>
        </div> -->

        <!-- <div class="col-lg-3 col-md-6 itemEquipe">
          <div class="bg" style="background: url(assets/img/thais-lopes.jpg) no-repeat center center;"></div>
          <div class="info">
            <h3>Thais Lopes</h3>
            <p>Psicóloga</p>
            <a href="javascript:void(0)" data-toggle="modal" data-target="#thais-lopes" title="Ver Detalhes" class="btAro">Ver Detalhes</a>
          </div>
        </div> -->

        <!-- <div class="col-lg-3 col-md-6 itemEquipe">
          <div class="bg" style="background: url(assets/img/tiago-nery.jpg) no-repeat center center;"></div>
          <div class="info">
            <h3>Tiago Nery</h3>
            <p>Educador Físico</p>
            <a href="javascript:void(0)" data-toggle="modal" data-target="#tiago-nery" title="Ver Detalhes" class="btAro">Ver Detalhes</a>
          </div>
        </div> -->

        <div class="col-lg-3 col-md-6 itemEquipe">
          <div class="bg" style="background: url(assets/img/Amaryllis_Maria.jpg) no-repeat center center;"></div>
          <div class="info">
            <h3>Amaryllis Maria</h3>
            <!-- <span>&nbsp;</span> -->
            <p>Enfermagem</p>
            <a href="javascript:void(0)" data-toggle="modal" data-target="#amaryllis" title="Ver Detalhes" class="btAro">Ver Detalhes</a>
          </div>
        </div>

        <div class="col-lg-3 col-md-6 itemEquipe">
          <div class="bg" style="background: url(assets/img/Manuela_Brito.jpg) no-repeat center center;"></div>
          <div class="info">
            <h3>Manuela Brito</h3>
            <!-- <span>&nbsp;</span> -->
            <p>Fisioterapeuta</p>
            <a href="javascript:void(0)" data-toggle="modal" data-target="#britto" title="Ver Detalhes" class="btAro">Ver Detalhes</a>
          </div>
        </div>

      </div>
    </div>
  </section>

  <div class="modal fade" id="dr-marcelo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <div class="info">
            <h3>Marcelo Protásio</h3>
            <span>CRM 20147</span>
            <p>Cirurgia Digestiva e Bariátrica</p>
          </div>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body conteudo">
          <h3 class="mt0">Áreas de Atuação:</h3>
          <ul>
            <li>Cirurgia Digestiva e Bariátrica;</li>
            <li>Cirurgia Geral e Videolaparoscopia;</li>
            <li>Tumores do Trato Digestivo</li>
          </ul>
          <h3>Formação Acadêmica</h3>
          <ul>
            <li>Graduação em Medicina (Universidade Federal de Sergipe)</li>
            <li>Residência Médica em Cirurgia Geral (Instituto de Assistência Médica ao Servidor Público do Estado - IAMSPE SP)</li>
            <li>Residência Médica em Cirurgia do Aparelho Digestivo e Coloproctologia (Instituto de Assistência Médica ao Servidor Público do Estado - IAMSPE SP)</li>
            <li>Mestrando em Cirurgia Bariátrica (Instituto de Assistência Médica ao Servidor Público do Estado - IAMSPE SP)</li>
          </ul>
          <h3>Formação Complementar:</h3>
          <ul>
            <li>Observership em Cirurgia Digestiva e Bariátrica (Cleveland Clinic Florida - EUA)</li>
            <li>Preceptor de Cirurgia Geral (Hospital Santa Isabel - SE)</li>
            <li>Professor de Clínica Cirúrgica do Curso de Medicina da Universidade Tiradentes (UNIT)</li>
            <li>Membro Efetivo do Colégio Brasileiro de Cirurgia Digestiva (CBCD)</li>
            <li>Membro Associado da Sociedade Brasileira de Cirurgia Bariátrica e Metabólica (SBCBM)</li>
          </ul>
          <p>Currículo Lattes: <a href="http://www.lattes.cnpq.br/4738009647745491" target="_blank" title="Clique Aqui">Clique Aqui</a></p>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="beatriz-leite" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <div class="info">
            <h3>Beatriz Leite</h3>
            <!-- <span>CRM 000</span> -->
            <p>Nutricionista</p>
          </div>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body conteudo">
          <ul>
            <li>Doutora em Ciências da Saúde - Universidade Federal de São Paulo;</li>
            <li>Mestre em Ciências da Saúde - Universidade Federal de São Paulo;</li>
            <li>Pós graduação em Nutrição Clínica - GANEP/ Beneficência Portuguesa - SP;</li>
            <li>Pós graduação em Nutrição Clínica Funcional - VP Consultoria Nutricional /SP;</li>
            <li>Pós graduação em Fitoterapia aplicada a nutricao - VP Consultoria Nutricional/ SP;</li>
            <li>Pós graduação em Nutrição Esportiva- VP Consultoria Nutricional /SP;</li>
            <li>Especialista em Obesidade, Emagrecimento e Saúde - Universidade Federal de São Paulo;</li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="andrea-fraga" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <div class="info">
            <h3>Andrea Fraga</h3>
            <!-- <span>CRM 000</span> -->
            <p>Nutricionista</p>
          </div>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body conteudo">
          <ul>
            <li>Graduação em Nutrição Bacharelado – Universidade Federal de Sergipe</li>
            <li>Pós-graduação em Nutrição Esportiva - Bases Nutricionais da Atividade Física - Gama Filho </li>
            <li>Especialista em Suplementação no Exercício - HI NUTRITION </li>
            <li>Especialista em Emagrecimento - Instituto Ana Paula Pujol</li>
          </ul>          
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="thais-lopes" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <div class="info">
            <h3>Thais Lopes</h3>
            <!-- <span>CRM 000</span> -->
            <p>Psicóloga</p>
          </div>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body conteudo">
          <ul>
            <li>Graduada em Psicologia pela Universidade Tiradentes;</li>
            <li>Pós graduada em Terapia cognitivo comportamental pelo IMEA/SE;</li>
            <li>Especialização em transtornos alimentares e obesidade pelo Hospital das Clínicas da FMUSP/CEPSIC;</li>
            <li>Membro associada à Soc.Bras.de Cirurgia Bariátrica e Metabólica;</li>
            <li>Título de Especialista em Psicologia Clínica, expedido pelo CEP 19a Região;</li>
            <li>Avaliação psicológica para cirurgia bariátrica e metabólica. Certificada pelo INBIO/SP.</li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="tiago-nery" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <div class="info">
            <h3>Tiago Nery</h3>
            <!-- <span>&nbsp;</span> -->
            <p>Educador Físico</p>
          </div>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body conteudo">
          <ul>
            <li>Graduado em Educação Física - UNIT, especialista em lesões e doenças musculoesqueléticas. </li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="amaryllis" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <div class="info">
            <h3>Amaryllis Maria de Andrade Tavares</h3>
            <!-- <span>&nbsp;</span> -->
            <p>Enfermagem</p>
          </div>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body conteudo">
          <ul>
            <li>Graduada em Enfermagem pela Universidade Tiradentes, com especializações em: Estomaterapia, Enfermagem do Trabalho, Cuidados Paliativos e Terapia da Dor e Enfermagem em Auditoria Hospitalar. </li>
            <li>Atualmente, realizo atendimento a pacientes com feridas e estomias, Auditora de enfermagem no Hospital São Lucas e Enfermeira do Trabalho pela Sansim.</li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="britto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <div class="info">
            <h3>Manuela Brito</h3>
            <!-- <span>&nbsp;</span> -->
            <p>Fisioterapeuta</p>
          </div>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body conteudo">
          <ul>
            <li>Graduada em Fisioterapia pela Faculdade Estácio-Fase</li>
            <li>Pós-Graduanda em Fisioterapia Pélvica com Ênfase em Obstetrícia, na Instituição IES.</li>
            <li>Curso de Fisioterapia Aplicada a Proctologia</li>
            <li>Curso avançado de Fisioterapia Aplicada a Proctologia</li>
            <li>Curso De Disfunções Coloproctológicas No Adulto e Biofeedback</li>
            <li>Fisioterapia proctologia desde de 2015.</li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <?php include '_footer.php'; ?>

  <?php include '_scripts-footer.php'; ?>
     
</body>

</html>
