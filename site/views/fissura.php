<!DOCTYPE html>
<html lang="en">

<head>

  <?php 
  
  $titlePage = 'PRINCIPAIS DOENÇAS - Fissura Anal';
  $descriptionPage = 'As hemorroidas são vasos sanguíneos normais, presentes na região anal. ';
  $imgPage = "https://".$_SERVER["SERVER_NAME"].'/assets/img/social.jpg'; //500x250px
  $siteName = 'Principais Doenças';

  include '_head.php';
      
  ?>

<style>
  .row-abscesso{
    margin-right: 15px;
    margin-left: 15px;
  }
  </style>

</head>

<body>

  <?php $menu = 'internas'; include '_menu.php'; ?>

  <?php include '_header.php'; ?>

  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-12 conteudo">
          <div class="titulo">
            <h2>Fissura Anal</h2>
          </div>
        </div>
      </div>
		<div class="row conteudo row-abscesso">
		  <h3>O que é a fissura anal?</h3>
		  <p>A fissura anal é uma “rachadura”, um “corte” linear no tecido anal, semelhante uma rachadura no canto da boca. Devido à rica inervação da região, é bastante dolorosa. Frequente causa de consulta ao Coloproctologista, esta patologia é confundida pelo leigo como hemorroida.</p>
		  <h3>O que causa?</h3>
		  <p>A fissura anal está relacionada geralmente a um hábito intestinal preso (constipado), com fezes duras e ressecadas, embora diarreia também possa causar fissura. Além disso, a hipertonia do esfíncter anal (aumento do tônus do esfíncter) também pode ser fator causal. O trauma da passagem das fezes ressecadas provoca uma “ferida” no tecido anal provocando dor. Há ainda outras causas menos frequentes como doenças sexualmente transmissíveis, doença de Crohn, tuberculose anorretal e câncer anal. Para o correto diagnóstico diferencial, é necessária avaliação do especialista.</p>
		  <h3>Quais são os sintomas?</h3>
		  <p>O principal sintoma é a DOR, principalmente durante e logo após as evacuações. Pode ocorrer sangramento de pequena monta e excesso de pele notado ao se higienizar (resultante de inflamação crônica da região). Muitos pacientes com fissura evitam ou têm medo de evacuar devido a dor.</p>
		  <h3>Qual é o Tratamento?</h3>
		  <p><img src="assets/img/doencas/fissura.jpg" class="img-fluid float-right" alt=""/>O tratamento inicialmente envolve a prescrição de fibras suplementares à dieta e aumento da ingesta de água para amolecer as fezes e evitar novos traumas na região. Banhos de assento com agua morna e pomadas analgésicas podem ajudar no alivio da dor e na cicatrização. Alguns pacientes podem necessitar de pomadas que relaxam a musculatura esfincteriana. Caso nenhuma dessas medidas seja capaz de cicatrizar totalmente a fissura, o paciente pode necessitar de algum procedimento cirúrgico. Os procedimentos visam relaxar a musculatura para adequada cicatrização dos tecidos; as opções são a secção de parte do esfíncter interno (esfincterotomia lateral) ou a aplicação de toxina botulínica (Botox ™), entre outras. Os riscos e benefícios de cada procedimento deve ser discutido com seu coloproctologista.</p>
		  <h3>A fissura pode voltar?</h3>
		  <p>Mesmo após cicatrizada a fissura pode voltar a ocorrer, principalmente se o fator causal não foi solucionado (constipação na maioria das vezes). Em caso de múltiplas recorrências, outras causas menos comuns devem ser pesquisadas, dentre elas câncer de canal anal, principalmente na população feminina sexualmente ativa.		  </p>
		  <h3>&nbsp;</h3>
			<p style="font-style: italic; font-size: 12px;">DR. FELIPE AUGUSTO DO PRADO TORRES e DR. JUVENAL DA ROCHA TORRES NETO</p>
		  </div>

      <div class="row">
        <div class="col-md-12">
        <iframe width="100%" height="600" src="https://www.youtube.com/embed/slzja37r5vk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
    </div>
  </section>

  <?php include '_footer.php'; ?>

  <?php include '_scripts-footer.php'; ?>
     
</body>

</html>
