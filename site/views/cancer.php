<!DOCTYPE html>
<html lang="en">

<head>

  <?php 
  
  $titlePage = 'PRINCIPAIS DOENÇAS - Câncer Colorretal';
  $descriptionPage = 'As hemorroidas são vasos sanguíneos normais, presentes na região anal. ';
  $imgPage = "https://".$_SERVER["SERVER_NAME"].'/assets/img/social.jpg'; //500x250px
  $siteName = 'Principais Doenças';

  include '_head.php';
      
  ?>

  <style>
  .row-abscesso{
    margin-right: 15px;
    margin-left: 15px;
  }
  </style>

</head>

<body>

  <?php $menu = 'internas'; include '_menu.php'; ?>

  <?php include '_header.php'; ?>

  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-12 conteudo">
          <div class="titulo">
            <h2>Câncer Colorretal</h2>
          </div>
        </div>
      </div>
		<div class="row conteudo row-abscesso">
		  <h3>O que é o Câncer Colorretal?</h3>
		  <p>É um tumor maligno do intestino grosso e reto e o terceiro câncer mais comum dentre todos os tipos de câncer no Brasil, segundo dados do INCA. A maioria dos casos surge em pacientes acima de 50 anos. Apesar da alta incidência, é prevenível se forem detectadas precocemente as lesões pré-malignas- os pólipos de intestino-  através da colonoscopia.</p>
		  <h3>Quais os fatores de risco?</h3>
		  <p>Idade, história familiar e alterações genéticas (síndromes genéticas como HNPCC e PAF) são os fatores de risco mais importantes. Contudo, fatores relacionados a hábitos de vida como dieta rica em carne vermelha (churrasco), pobre em fibras, sedentarismo, etilismo, tabagismo e obesidade estão implicados no aumento da incidência deste tipo de câncer. Além disso, algumas doenças intestinais como a Retocolite Ulcerativa e a Doença de Crohn estão também relacionadas a um maior risco. </p>
		  <h3>Quais os sintomas?</h3>
		  <p>Os sintomas são variados, mas os mais frequentes são ALTERÇÃO DO HÁBITO INESTINAL (diarreia ou prisão de ventre, ou alternância entre os dois), SANGRAMENTO RETAL (vermelho vinhoso ou escurecido, com ou sem coágulos) e PERDA DE PESO. Pode ocorrer ainda, dor abdominal, fraqueza, anemia, saída de muco nas fezes, afilamento das fezes.</p>
		  <p>Entretanto, quando há sintomas é porque o tumor já apresentou algum crescimento. NAS FASES INICIAIS ELE É COMPLETAMENTE ASSINTOMÁTICO!</p>
		  <h3>Como prevenir?</h3>
		  <p><img src="assets/img/doencas/cancer.jpg"  class="img-fluid float-right" alt=""/>A melhor prevenção começa com hábitos de vida saudáveis, com atividade física regular, dieta rica em fibras e alimentos contendo antioxidantes, como frutas e verduras cruas, controle da obesidade, deixar o fumo e o uso abusivo de bebida alcóolica. Contudo, mesmo que não tenha conseguido controlar os fatores de risco, o câncer colorretal é prevenível se for rastreado corretamente em busca de lesões pré-malignas (pólipos adenomatosos) através de colonoscopia. A idade de início do rastreamento é 50 anos em uma população sem história familiar ou 10 anos antes da idade do aparecimento do câncer colorretal em familiar (ex.: se familiar teve câncer aos 45 anos, o rastreio em familiares começa aos 35).</p>
		  <h3>Tratamento. Tem cura?</h3>
		  <p>Este câncer tem cura! O tratamento curativo se baseia em cirurgia para retirada do órgão acometido com retirada dos gânglios linfáticos que drenam a região. Mesmo se houver alguma metástase para órgãos vizinhos,  fígado e pulmão é possível ainda a cura, embora com índices menores do que tumores em estágios mais iniciais. A quimioterapia pós-operatória é indicada para alguns pacientes, não para todos. Em tumores de reto, especificamente, pode ser necessária a realização de quimio e radioterapia pré-operatórias. A cirurgia pode ser realizada por laparoscopia (pequenas incisões) ou por via aberta. Em alguns casos é necessário deixar uma colostomia ou ileostomia (bolsinha para evacuar) temporária ou definitiva. </p>
		  <h3>&nbsp;</h3>
			<p style="font-style: italic; font-size: 12px;">DR. FELIPE AUGUSTO DO PRADO TORRES e DR. JUVENAL DA ROCHA TORRES NETO</p>
      </div>
      <div class="row">
        <div class="col-md-12">
        <iframe width="100%" height="600" src="https://www.youtube.com/embed/GFI5Y6FPJ2Y" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
    </div>
  </section>

  <?php include '_footer.php'; ?>

  <?php include '_scripts-footer.php'; ?>
     
</body>

</html>
