<!DOCTYPE html>
<html lang="en">

<head>

  <?php 
  
  $titlePage = 'PRINCIPAIS DOENÇAS - Hemorróidas';
  $descriptionPage = 'As hemorroidas são vasos sanguíneos normais, presentes na região anal. ';
  $imgPage = "https://".$_SERVER["SERVER_NAME"].'/assets/img/social.jpg'; //500x250px
  $siteName = 'Principais Doenças';

  include '_head.php';
      
  ?>

  <style>
  .row-abscesso{
    margin-right: 15px;
    margin-left: 15px;
  }
  </style>

</head>

<body>

  <?php $menu = 'internas'; include '_menu.php'; ?>

  <?php include '_header.php'; ?>

  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-12 conteudo">
          <div class="titulo">
            <h2>Hemorróidas</h2>
          </div>
        </div>
      </div>
		<div class="row conteudo row-abscesso">
		 <h3>O que s&atilde;o hemorroidas?</h3>
		 <p> As hemorroidas s&atilde;o vasos sangu&iacute;neos normais, presentes na regi&atilde;o anal. Contudo quando elas se dilatam e come&ccedil;am a prolapsar na regi&atilde;o anal, causando sintomas, dizemos ent&atilde;o que h&aacute; uma Doen&ccedil;a hemorroid&aacute;ria, essa sim necessita de cuidados m&eacute;dicos.</p>
		 <h3> Quais os sintomas?</h3>
		 <p> <img src="assets/img/doencas/hemo-01.jpg"  class="img-fluid float-right" alt=""/>O sintoma mais frequente &eacute; o sangramento, geralmente vermelho vivo, notado no papel higi&ecirc;nico ou mesmo no vaso sanit&aacute;rio. O sangramento em geral &eacute; de pequena monta e para espontaneamente nos casos iniciais. O paciente pode notar ainda abaulamento (sensa&ccedil;&atilde;o de &ldquo;caro&ccedil;o&rdquo; arredondado no &acirc;nus) que pode reduzir espontaneamente ou necessitar ser empurrado para dentro. Ao contr&aacute;rio do que se imagina, raramente pode haver dor na doen&ccedil;a hemorroid&aacute;ria, e, quando isso ocorre, geralmente existe uma complica&ccedil;&atilde;o chamada trombose hemorroid&aacute;ria, que &eacute; bastante desconfort&aacute;vel para o paciente.</p>
		 <h3><br>
		   Como se faz o diagn&oacute;stico?</h3>
		 <p> O diagn&oacute;stico &eacute; feito atrav&eacute;s do exame f&iacute;sico realizado por especialista. Inicialmente, a simples inspe&ccedil;&atilde;o pode dar importantes informa&ccedil;&otilde;es para o diagn&oacute;stico, mas a anuscopia &eacute; o exame mais apropriado para o diagn&oacute;stico, atrav&eacute;s da visualiza&ccedil;&atilde;o da mucosa anorretal. O exame &eacute; realizado sem necessidade de anestesia, no pr&oacute;prio consult&oacute;rio m&eacute;dico.</p>
	     <h3>Quais os tratamentos?</h3>
	<p> <img src="assets/img/doencas/hemo-02.jpg"  class="img-fluid float-right" alt=""/>Nos graus iniciais, o tratamento n&atilde;o &eacute; cir&uacute;rgico e orienta-se alguns cuidados como dieta rica em fibras (presente em vegetais folhosos, frutas com baga&ccedil;o, linha&ccedil;a, farelo de trigo, cereais integrais etc.), aumento da ingest&atilde;o de &aacute;gua, higieniza&ccedil;&atilde;o sem papel higi&ecirc;nico. Caso persistam os sintomas, ou nos casos de graus mais avan&ccedil;ados, &eacute; necess&aacute;ria interven&ccedil;&atilde;o atrav&eacute;s de procedimentos. O mais simples deles &eacute; a ligadura el&aacute;stica (coloca&ccedil;&atilde;o de an&eacute;is el&aacute;sticos na base dos mamilos hemorroid&aacute;rios para promover a necrose dos mesmos) e pode ser realizado em consult&oacute;rio sem anestesia, mas em alguns casos &eacute; necess&aacute;rio procedimento cir&uacute;rgico propriamente dito atrav&eacute;s da excis&atilde;o dos mamilos hemorroid&aacute;rios doentes. Existem v&aacute;rias t&eacute;cnicas cir&uacute;rgicas poss&iacute;veis, que n&atilde;o ser&atilde;o abordadas neste encarte. Procure um especialista na &aacute;rea e tire suas d&uacute;vidas</p>
			<p style="font-style: italic; font-size: 12px;">DR. FELIPE AUGUSTO DO PRADO TORRES e DR. JUVENAL DA ROCHA TORRES NETO</p>
			
      </div>
      
      <div class="row">
        <div class="col-md-12">
        <iframe width="100%" height="600" src="https://www.youtube.com/embed/DQeDyleWzas" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
    </div>
  </section>

  <?php include '_footer.php'; ?>

  <?php include '_scripts-footer.php'; ?>
     
</body>

</html>
