<!DOCTYPE html>
<html lang="en">

<head>

  <?php 
  
  $titlePage = 'Dr. Felipe Torres';
  $descriptionPage = 'Núcelo Integrado de Cirurgia Digestiva';
  $imgPage = "https://".$_SERVER["SERVER_NAME"].'/assets/img/social.jpg'; //500x250px
  $siteName = 'Sobre';

  include '_head.php';
      
  ?>

<style>
* {
  box-sizing: border-box;
}

.row > .column {
  padding: 0 8px;
}

.row:after {
  content: "";
  display: table;
  clear: both;
}

.column {
  float: left;
  width: 25%;
  margin-top: 15px;
}

/* The Modal (background) */
.modal {
  display: none;
  position: fixed;
  z-index: 1;
  padding-top: 100px;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  overflow: auto;
  background-color: black;
  z-index:9999;
}

/* Modal Content */
.modal-content {
  position: relative;
  background-color: #fefefe;
  margin: auto;
  padding: 0;
  width: 90%;
  max-width: 1200px;
}

/* The Close Button */
.close {
  color: white;
  position: absolute;
  top: 10px;
  right: 25px;
  font-size: 35px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #999;
  text-decoration: none;
  cursor: pointer;
}

.mySlides {
  display: none;
}

.cursor {
  cursor: pointer;
}

/* Next & previous buttons */
.prev,
.next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -50px;
  color: white;
  font-weight: bold;
  font-size: 20px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
  -webkit-user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover,
.next:hover {
  background-color: rgba(0, 0, 0, 0.8);
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

img {
  margin-bottom: -4px;
}

.caption-container {
  text-align: center;
  background-color: black;
  padding: 2px 16px;
  color: white;
}

.demo {
  opacity: 0.6;
}

.active,
.demo:hover {
  opacity: 1;
}

img.hover-shadow {
  transition: 0.3s;
}

.hover-shadow:hover {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}
@media screen and (max-width: 640px) {
  .marg-galeria{
    margin-right: 0;
    margin-left: 0;
  }
}
</style>

</head>

<body>

  <?php $menu = 'internas'; include '_menu.php'; ?>

  <?php include '_header.php'; ?>

  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-12 conteudo">
          <div class="titulo">
            <h2>Dr. Felipe Torres</h2>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-7 conteudo">
          <!-- <h3 class="mt0">Áreas de Atuação:</h3>
          <ul>
			  <li>Coloproctologia</li>
			  <li>Cirurgia do Aparelho Digestivo</li>
			  <li>Cirurgia geral</li>
			  <li>Colonoscopia</li>
			  
          </ul>
          <h3>Formação Acadêmica:</h3>
          <ul>
            <li>Formação em medicina pela Universidade Federal de Sergipe</li>
			<li>Residência médica em Cirurgia Geral (IAMSPE- SP)</li>
          </ul>
          
		<h3>Formação Complementar:</h3>
          <ul>
            <li>Membro da Sociedade Brasileira de Coloproctologia</li>
			<li>Fellow em Coloproctologia pela Cleveland Clinic- Florida</li>
			<li>Professor e preceptor voluntário de Coloproctologia da Universidade Federal de Sergipe</li>
          </ul>
          <p>Currículo Lattes: <a href="#" target="_blank" title="Clique Aqui">Clique Aqui</a></p> -->
          <p>Dr. Felipe Torres é Coloproctologista, com residência médica em São Paulo pelo Instituto de Assistência Médica ao Servidor Público Estadual de São Paulo- IAMSPE e observership pela Cleveland Clinic Flórida- EUA. </p>

          <p>Durante sua formação, subespecializou-se em Videolaparoscopia (cirurgia com pequenas incisões onde se trabalha com uma câmeras e pinças especiais, com menor trauma cirúrgico), onde participou como aluno e depois monitor de cursos na área em São Paulo e EUA durante 5 anos. Hoje é coordenador de um curso de Laparoscopia para cirurgiões e residentes em Aracaju - o Lapcoach, criado em 2017. </p>

          <p>Dentro da Coloproctologia, dá ênfase às áreas de Câncer Colorretal, Endometriose, Diverticulite, Colonoscopia, Doença hemorroidária, Cisto pilonidal, Fissuras anais, Fistulas anais e Doenças inflamatórias (Retocolite e Doença de Crohn).</p>

          <p>Atualmente trabalha no mercado particular e de convênios em Aracaju, É membro Titular da Sociedade Brasileira de Coloproctologia, possui diversos artigos de pesquisa publicadas em revistas nacionais e coordena pesquisas e ensino na Liga Acadêmica de Coloproctologia e Cirurgia Digestiva da Unit.</p>

          <p>Hospitais que atua: Hospital Primavera, Hospital São Lucas, Hospital da Unimed.</p>
        </div>
        <div class="col-md-5 d-none d-md-block">
          <img src="assets/img/dr-marcelo.jpg" title="Dr. Felipe" alt="dr-felipe" class="img-fluid">
        </div>
      </div>
      <div class="row mt50">
        <!-- <div class="col-md-4 itemImagem">
          <div class="bg" style="background: url(assets/img/foto1.jpg) no-repeat center center;">
          </div>
          <div class="legenda d-none">Legenda da foto</div>
        </div>
        <div class="col-md-4 itemImagem">
          <div class="bg" style="background: url(assets/img/foto2.jpg) no-repeat center center;">
          </div>
          <div class="legenda d-none">Legenda da foto</div>
        </div>
        <div class="col-md-4 itemImagem">
          <div class="bg" style="background: url(assets/img/foto3.jpg) no-repeat center center;">
          </div>
          <div class="legenda d-none">Legenda da foto</div>
        </div> -->
        
        <div class="row marg-galeria">

        <div class="col-md-12 conteudo">
          <div class="titulo">
            <h2>Fotos de Dr. Felipe Torres</h2>
          </div>
        </div>

          <div class="column">
            <img src="assets/img/foto_04.jpg" style="width:100%" onclick="openModal();currentSlide(1)" class="hover-shadow cursor">
          </div>
          <div class="column">
            <img src="assets/img/foto_05.jpg" style="width:100%" onclick="openModal();currentSlide(2)" class="hover-shadow cursor">
          </div>
          <div class="column">
            <img src="assets/img/foto_06.jpg" style="width:100%" onclick="openModal();currentSlide(3)" class="hover-shadow cursor">
          </div>
          <div class="column">
            <img src="assets/img/foto_07.jpg" style="width:100%" onclick="openModal();currentSlide(4)" class="hover-shadow cursor">
          </div>
          <div class="column">
            <img src="assets/img/foto_08.jpg" style="width:100%" onclick="openModal();currentSlide(5)" class="hover-shadow cursor">
          </div>
          <div class="column">
            <img src="assets/img/foto_09.jpg" style="width:100%" onclick="openModal();currentSlide(6)" class="hover-shadow cursor">
          </div>
          <div class="column">
            <img src="assets/img/foto_10.jpg" style="width:100%" onclick="openModal();currentSlide(7)" class="hover-shadow cursor">
          </div>
          <div class="column">
            <img src="assets/img/foto_11.jpg" style="width:100%" onclick="openModal();currentSlide(8)" class="hover-shadow cursor">
          </div>
          <div class="column">
            <img src="assets/img/foto_12.jpg" style="width:100%" onclick="openModal();currentSlide(9)" class="hover-shadow cursor">
          </div>
          <div class="column">
            <img src="assets/img/foto_13.jpg" style="width:100%" onclick="openModal();currentSlide(10)" class="hover-shadow cursor">
          </div>
          <div class="column">
            <img src="assets/img/foto_14.jpg" style="width:100%" onclick="openModal();currentSlide(11)" class="hover-shadow cursor">
          </div>
          <div class="column">
            <img src="assets/img/foto_15.jpg" style="width:100%" onclick="openModal();currentSlide(12)" class="hover-shadow cursor">
          </div>
          <div class="column">
            <img src="assets/img/foto_16.jpg" style="width:100%" onclick="openModal();currentSlide(13)" class="hover-shadow cursor">
          </div>
          <div class="column">
            <img src="assets/img/foto_17.jpg" style="width:100%" onclick="openModal();currentSlide(14)" class="hover-shadow cursor">
          </div>
        </div>

        <div id="myModal" class="modal">
          <span class="close cursor" onclick="closeModal()">&times;</span>
          <div class="modal-content">

            <div class="mySlides">
              <div class="numbertext">1 / 14</div>
              <img src="assets/img/foto_04.jpg" style="width:100%">
            </div>

            <div class="mySlides">
              <div class="numbertext">2 / 14</div>
              <img src="assets/img/foto_05.jpg" style="width:100%">
            </div>

            <div class="mySlides">
              <div class="numbertext">3 / 14</div>
              <img src="assets/img/foto_06.jpg" style="width:100%">
            </div>
            
            <div class="mySlides">
              <div class="numbertext">4 / 14</div>
              <img src="assets/img/foto_07.jpg" style="width:100%">
            </div>
            
            <div class="mySlides">
              <div class="numbertext">5 / 14</div>
              <img src="assets/img/foto_08.jpg" style="width:100%">
            </div>

            <div class="mySlides">
              <div class="numbertext">6 / 14</div>
              <img src="assets/img/foto_09.jpg" style="width:100%">
            </div>

            <div class="mySlides">
              <div class="numbertext">7 / 14</div>
              <img src="assets/img/foto_10.jpg" style="width:100%">
            </div>
            
            <div class="mySlides">
              <div class="numbertext">8 / 14</div>
              <img src="assets/img/foto_11.jpg" style="width:100%">
            </div>

            <div class="mySlides">
              <div class="numbertext">9 / 14</div>
              <img src="assets/img/foto_12.jpg" style="width:100%">
            </div>

            <div class="mySlides">
              <div class="numbertext">10 / 14</div>
              <img src="assets/img/foto_13.jpg" style="width:100%">
            </div>

            <div class="mySlides">
              <div class="numbertext">11 / 14</div>
              <img src="assets/img/foto_14.jpg" style="width:100%">
            </div>
            
            <div class="mySlides">
              <div class="numbertext">12 / 14</div>
              <img src="assets/img/foto_15.jpg" style="width:100%">
            </div>

            <div class="mySlides">
              <div class="numbertext">13 / 14</div>
              <img src="assets/img/foto_16.jpg" style="width:100%">
            </div>

            <div class="mySlides">
              <div class="numbertext">14 / 14</div>
              <img src="assets/img/foto_17.jpg" style="width:100%">
            </div>
                     
            <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
            <a class="next" onclick="plusSlides(1)">&#10095;</a>

            <div class="caption-container">
              <p id="caption"></p>
            </div>

          </div>
        </div>

      </div>
    </div>
  </section>

  <?php include '_footer.php'; ?>

  <?php include '_scripts-footer.php'; ?>

  <script>
function openModal() {
  document.getElementById("myModal").style.display = "block";
}

function closeModal() {
  document.getElementById("myModal").style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>
     
</body>

</html>
