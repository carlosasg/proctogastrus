<footer>
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-12">
        <p>Av. Ministro Geraldo Barreto Sobral, 2131, Sala 207 - Bairro Jardins</p>
        <p>Aracaju - SE, 49026-010</p>
        <p>(79) 3024-5505</p>
        <p>clinicagastrus.se@gmail.com</p>
      </div>
      <div class="col-lg-4 col-md-12 social">
        <a href="https://www.facebook.com/clinicagastrusaracaju/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a>
        <a href="https://www.instagram.com/clinicagastrus/" target="_blank" title="Facebook"><i class="fab fa-instagram"></i></a>
      </div>
    </div>
    <div class="row apoio">
      <div class="col-lg-8 col-md-12 copyright">
        @Copyrigth <?= date("Y") ?> - Todos os direitos reservados a <?= $siteName ?>
      </div>
      <div class="col-lg-4 col-md-12 marcaAlfama">
        <a href="https://alfamaweb.com.br" class="logo" target="_blank">
          <span class="m1 big"><img src="assets/img/m1.png"></span><span class="m2 big"><img src="/assets/img/m2.png"></span>
        </a>
      </div>
    </div>
  </div>
</footer>