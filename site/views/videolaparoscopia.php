<!DOCTYPE html>
<html lang="en">

<head>

  <?php 
  
  $titlePage = 'VIDEOLAPAROSCOPIA';
  $descriptionPage = 'Núcelo Integrado de Cirurgia Digestiva';
  $imgPage = "https://".$_SERVER["SERVER_NAME"].'/assets/img/social.jpg'; //500x250px
  $siteName = 'Gastrus';

  include '_head.php';
      
  ?>

<style>
 
</style>

</head>

<body>

  <?php $menu = 'internas'; include '_menu.php'; ?>
  <?php include 'galeriaCSS.php'; ?>
  <?php include '_header.php'; ?>

  <div class="wrap">
    <section>
      <div class="container">
        <div class="row">
          <div class="col-md-12 conteudo">
            <div class="titulo">
              <h2>VIDEOLAPAROSCOPIA</h2>
            </div>
          </div>
        </div>
        <div class="row row-abscesso">
		  <p>Videolaparoscopia é uma técnica cirúrgica minimamente invasiva realizada por auxílio de uma endocâmera no abdômen (laparo). Para criar o espaço necessário as manobras cirúrgicas e adequada visualização das vísceras abdominais a cavidade peritonial é insuflada com gás carbônico.</p>
			<p>O instrumental cirúrgico e a endocâmera entram na cavidade através de trocaters, que são como tubos com válvulas para permitir a entrada de CO2 e dos instrumentos sem a saída de gás, que são introduzidos através de pequenas incisões na pele (i.e. 5 a 14 mm).</p>
			<p>O pneumoperitônio é realizado de forma aberta ou fechada. Na forma aberta é realizada uma minilaparotomia e o trocar é inserido diretamente, sob visão direta na cavidade. Na forma fechada é realizada uma punção com uma agulha especial (agulha de Veress) e após ser atingido o nível pressórico desejado é inserido um trocar com um mandril (tipo de punção afiado que preenche o trocar) às cegas na cavidade. Após a introdução do primeiro portal é inserida a endocâmera, a cavidade é inspecionada e são inseridos demais portais de acordo com a necessidade, de acordo com o procedimento a ser realizado. Ao final da operação são retirados os trocartes e as incisões são fechadas.</p>
			<p>A técnica de videoendoscopia também pode ser realizada em outros compartimentos como no tórax (videotoracoscopia), no pescoço, na face (em procedimentos de cirurgia plástica), vias urinárias e articulações. Nas artroscopias (videoendoscopia de articulações) e endoscopia urinárias não é utilizado o gás carbônico para se criar espaço de trabalho e sim água destilada.</p>
		  <p style="font-style: italic; font-size: 12px;"><strong>Fonte</strong>: wikipedia.org</p>
			
			<div class="row imgs-video">
				
			</div>
      <div class="row "></div>
			<div class="row mt-5 ">
				<div class="col-md-12">
				
				<div>
		    	<div class="col-md-12">
          <div class="titulo">
					</div>
					  <h5>Veja mais em: <a href="https://lapcoach.com.br" target="_blank"><img src="assets/img/lapcoach.png" width="98" height="42" alt=""/>lapcoach.com.br</a></h5>
          </div>
			 </div>
        </div>
      </div>
    </section>




    <section>
    <div class="container">
      <div class="row">
        <div class="col-md-12 conteudo">
          <!-- <div class="titulo">
            <h2>Dr. Felipe Torres</h2>
          </div> -->
        </div>
      </div>
      
      <div class="row mt50">

        <div class="row marg-galeria">

        <div class="col-md-12 conteudo">
          <div class="titulo">
            <h2>Imagens da nossa equipe profissional.</h2>
          </div>
        </div>

          <div class="col-md-3 col-sm-4">
			    	<img src="assets/img/foto_04.jpg" width="716" height="716" class="img-fluid" alt=""/>
				</div>
				<div class="col-md-3 col-sm-4">
			    	<img src="assets/img/foto_05.jpg" width="716" height="716" class="img-fluid" alt=""/>
				</div>
				<div class="col-md-3 col-sm-4">
			    	<img src="assets/img/foto_10.jpg" width="716" height="716" class="img-fluid" alt=""/>
				</div>
				<div class="col-md-3 col-sm-4">
			    	<img src="assets/img/dr7.jpg" width="716" height="716" class="img-fluid" alt=""/>
				</div>

        <!-- <div id="myModal" class="modal">
          <span class="close cursor" onclick="closeModal()">&times;</span>
          <div class="modal-content">

            <div class="mySlides">
              <div class="numbertext">1 /20</div>
              <img src="assets/img/foto_04.jpg" style="width:100%">
            </div>

            <div class="mySlides">
              <div class="numbertext">2 /20</div>
              <img src="assets/img/foto_05.jpg" style="width:100%">
            </div>

            <div class="mySlides">
              <div class="numbertext">3 /20</div>
              <img src="assets/img/foto_06.jpg" style="width:100%">
            </div>
            
                     
            <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
            <a class="next" onclick="plusSlides(1)">&#10095;</a>

            <div class="caption-container">
              <p id="caption"></p>
            </div>

          </div>
        </div> -->

      </div>
    </div>
  </section>

    
  </div>


  <?php include '_footer.php'; ?>

  <?php include '_scripts-footer.php'; ?>
     
</body>

</html>
