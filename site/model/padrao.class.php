<?php

class Padrao{

	var $slug = null;
	var $limite_midia = false;

	//VARIAVEIS DE CONEXAO
	var $conexao_host = null;
	var $conexao_usuario = null;
	var $conexao_senha = null;
	var $conexao_database = null;
	var $conexao = null;

	//VARIAVEIS DE CONSULTA
	var $campos = "*";
	var $idcategoria = null;
	var $idsite = null;
	var $idconteudo = null;
	var $capa = false;
	var $retorno = null;
	var $busca = false;
	var $id = false;
	var $idcidade = false;
	var $idestado = false;
	var $idtipo = false;
	var $idconteudo_dif = false;
	var $idestagio = false;
	var $preview = false;
	var $cotar = false;
	var $situacao = false;
	var $pensionista = false;

	//VARIAVEIS DE ORDENAÇÃO
	var $ordem = "DESC";
	var $ordem_campo = false;

	//VARIAVEIS DE PAGINAÇÃO
	var $limite = 3;
	var $limite_galeria = NULL;

	var $sql = array();
	var $imagens = array();
	var $pagina = 1;
	var $paginas = 1;
	var $total = 1;
	var $inicio = NULL;
	var $post = false;
	var $maislido = false;

	var $id_conteudos = array();

	var $contador = 0;




	function __construct() {

	}

	function __destruct() {
		//mysql_close();

	}

	function Set($variavel, $valor){
		if($valor) $this->$variavel = $valor;
	}

	function Get($variavel){
		$retorno = (isset($this->$variavel)) ? $this->$variavel : null;
		return $retorno;
	}

	function Conecta(){
		$this->conexao = mysql_connect($this->conexao_host,$this->conexao_usuario,$this->conexao_senha);
		mysql_select_db($this->conexao_database);
	}

	function RetornarConvenios() {

		$sql = "SELECT e.*, et.nome as estado, ct.nome as cidade
				FROM empresas e
				INNER JOIN empresas_categorias ec ON (e.idempresa_categoria = ec.idempresa_categoria)
				INNER JOIN estados et ON (e.idestado = et.idestado)
				INNER JOIN cidades ct ON (e.idcidade = ct.idcidade)
				WHERE e.ativo = 'S' ";

			$query = mysql_query($sql);
			$num = mysql_num_rows($query);
			$this->paginas = ceil($num/$this->limite);
			if($this->ordem_campo){ $sql .= " ORDER BY ".$this->ordem_campo." ".$this->ordem;}
			if ($this->inicio == -1)
			   $this->inicio = 0;
			else
			   $this->inicio = ($this->pagina - 1) * $this->limite;

			if($this->limite) $sql .= " LIMIT ".$this->inicio.", ".$this->limite . "";
		$query = mysql_query($sql);

		while($linha = mysql_fetch_assoc($query)){
			$retorno[] = $linha;
		}

		return $retorno;

    }
    function RetornarDocumentos() {

		$sql = "SELECT d.*
				FROM documentos d
				LEFT JOIN documento_usuario du ON (d.idocumento = du.iddocumento)
				WHERE d.ativo = 'S' ";
		if($this->categoria){
			$sql .= " AND d.categoriadocumento = '".$this->categoria."' ";
		}
		if($this->individual){
			$sql .= " AND du.idusuario = '".$this->individual."' ";
		}
			$query = mysql_query($sql);
			$num = mysql_num_rows($query);
			$this->paginas = ceil($num/$this->limite);
			$sql .= ' GROUP BY d.idocumento ';
			if($this->ordem_campo){ $sql .= " ORDER BY ".$this->ordem_campo." ".$this->ordem;}
			if ($this->inicio == -1)
			   $this->inicio = 0;
			else
			   $this->inicio = ($this->pagina - 1) * $this->limite;

			if($this->limite) $sql .= " LIMIT ".$this->inicio.", ".$this->limite . "";
		// echo $sql; exit;
		$query = mysql_query($sql);

		while($linha = mysql_fetch_assoc($query)){
			$retorno[] = $linha;
		}

		return $retorno;

    }


	function votarEnquete(){
		$sql = "UPDATE  enquete_perguntas SET  votos = votos+1 WHERE  idenquete = '".$this->idenquete."' AND idpergunta = '".$this->idpergunta."' LIMIT 1" ;
		if(mysql_query($sql)){
			return true;
		}
		else{
			return false;
		}
	}
	function retornaPerguntasEnquetes($idenquete){

		$sql = "select * from enquete_perguntas where ativo = 'S' AND idenquete = '".mysql_real_escape_string($idenquete)."'";

		$verifica = mysql_query($sql) or die('');

		if(mysql_num_rows($verifica) == 0){
			return false;
		} else {

			while($linha = mysql_fetch_assoc($verifica)){
				$retorno[] = $linha;
			}
			return $retorno;
		}

	}
	function retornaEnquetes(){

	  $sql = "SELECT ".$this->campos."
			  FROM enquetes e
				LEFT JOIN enquetes_categorias ec ON (e.idenquete = ec.idenquete)
			  WHERE  e.ativo = 'S' AND e.situacao = 'S' ";

		if($this->idcategoria){
			$sql .= " AND ec.idcategoria = '".$this->idcategoria."' ";
		}

	  if($this->ordem_campo){ $sql .= " ORDER BY ".$this->ordem_campo." ".$this->ordem;}

		if($this->limite) $sql .= " LIMIT 0, ".$this->limite . "";
	  $query = mysql_query($sql);


		if(mysql_num_rows($query) == 0){
			return false;
		} else {
			$cont = 0;
			while ($linha = mysql_fetch_assoc($query)) {
				$this->retorno[$cont] = $linha;
				$this->retorno[$cont]['perguntas'] = $this->retornaPerguntasEnquetes($linha['idenquete']);
				$cont++;
			}

			return $this->retorno;
		}



	}

	function retornaConteudos($contabilizaAcesso = true){

		$this->retorno = NULL;
		$this->id_conteudos = array();//salva todos os id conteudo retornado
	
	
		$sql = "SELECT {$this->campos}
				FROM conteudos c
				INNER JOIN conteudos_categorias cc ON(c.idconteudo = cc.idconteudo)
				INNER JOIN categorias ct ON(cc.idcategoria = ct.idcategoria)
				INNER JOIN categorias_modulos cm ON (cm.idcategoria  = ct.idcategoria)
				WHERE  c.ativo = 'S'  AND ";

		if ($this->idconteudo) {
			$sql .= " c.idconteudo   = '".$this->idconteudo."'  AND";
				if($contabilizaAcesso) $this->contarAcesso();
		}

		if ($this->idconteudo_dif){
			//REMOVE O INDICE DO ARRAY QUE VENHA VAZIO, PARA NÃO DAR ERRO NA CONSULTA DOS OUTROS SQLS
			$nao_exibir = explode(",",$this->idconteudo_dif);
			$nao_exibir = array_unique($nao_exibir);//remove valores repetidos do array, deixando apenas o primeiro que se repete
			$key = array_search('', $nao_exibir);//busca se tem valor vazio
			if(!$nao_exibir[$key]){
				unset($nao_exibir[$key]);//limpa o indíce que tenha valor vazio
			}
			$this->idconteudo_dif = implode(',',$nao_exibir);
			//FIM REMOVE O INDICE DO ARRAY QUE VENHA VAZIO, PARA NÃO DAR ERRO NA CONSULTA DOS OUTROS SQLS
			if($this->idconteudo_dif){ $sql .= " c.idconteudo not in(".$this->idconteudo_dif.") AND "; }
		}

		if(!$this->preview){ $sql .= "  (c.status = 'A' OR c.status = 'AD') AND ";}
		if ($this->idcategoria){	$sql .= " cc.idcategoria = '".$this->idcategoria."' AND ";}
		if (!empty($this->idcategoria_dif)){	$sql .= " cc.idcategoria NOT IN (".$this->idcategoria_dif.") AND ";}

		if (!empty($this->dt_evento)){ $sql .= "  date_format(c.data_evento,'%Y-%m-%d') = '".$this->dt_evento."' AND ";}

		if (!empty($this->ano)){ $sql .= "  date_format(c.data_evento,'%Y') = '".$this->ano."' AND ";}

		if ($this->busca){
		$busca = $this->busca;
		//$busca = preg_replace("/&([a-z])[a-z]+;/i", "$1", htmlentities($this->busca));///$this->acentos_str();
		$sql .= " ((c.nome LIKE '%".mysql_real_escape_string(utf8_decode(htmlentities($busca)))."%') OR (c.resumo LIKE '%".mysql_real_escape_string(utf8_decode(htmlentities($busca)))."%')  OR (c.texto LIKE '%".mysql_real_escape_string(utf8_decode(htmlentities($busca)))."%') OR (c.subtitulo LIKE '%".mysql_real_escape_string(utf8_decode(htmlentities($busca)))."%') OR (ct.nome LIKE '%".mysql_real_escape_string(utf8_decode(htmlentities($busca)))."%')) AND  ";
		}

		if (is_array($this->idsite)){
			$sql .= "  ct.idsite      in( ".join(",",$this->idsite).") AND";
		}elseif ($this->idsite){
			$sql .= "  ct.idsite      = '".$this->idsite."' AND";
		}
		if($this->maislido){
			$sql .= " date_format(c.data_cad,'%Y-%m-%d') >=  DATE_SUB(CURRENT_DATE( ), INTERVAL 7 DAY) AND";
		}
		$sql .= "  (c.data_entrada <=  NOW()  AND (c.data_saida >=  NOW() or c.data_saida is null))
							AND cm.idmodulo = 1 ";


		if (!empty($this->in)){
			$sql .= " AND " . $this->in . " ";
		}

		/*  $sql .= " DATE_FORMAT( c.data_entrada,  '%Y-%m-%d' ) <=  NOW()  AND (DATE_FORMAT( c.data_saida,  '%Y-%m-%d' ) >=  NOW() or c.data_saida is null)
							AND cm.idmodulo = 1 ";*/
		if (!empty($this->data_evento)) $sql .= " AND (DATE_FORMAT( c.data_evento,  '%Y-%m-%d' ) >= DATE_FORMAT(NOW(),  '%Y-%m-%d' ))";
		if (!empty($this->data_evento_posterior)) $sql .= " AND (DATE_FORMAT( c.data_evento,  '%Y' ) >= '" . $this->data_evento_posterior . "') ";
		if (!empty($this->data_evento_mes)) $sql .= " AND (DATE_FORMAT( c.data_evento,  '%Y-%m' ) = DATE_FORMAT(NOW(),  '%Y-%m' ))";
		if (!empty($this->data_evento_atual)) $sql .= " AND (DATE_FORMAT( c.data_evento,  '%Y-%m' ) = '".$this->data_evento_atual."')";
		if(is_null($this->idconteudo)) {
			$sqlCount = str_replace($this->campos, "COUNT(*) AS total", $sql);
			$query2 = mysql_query($sqlCount) or die(header('location: /'));
			$total = mysql_fetch_assoc($query2);
			$this->total = $total["total"];

		} else {
			$this->total = 1;
		}
		
		if (!empty($this->groupby)){
			$sql .= " GROUP BY ".$this->groupby." ";
		}
		else{
			$sql .= " GROUP BY c.idconteudo ";
		}

		if($this->ordem_campo){ $sql .= " ORDER BY ".$this->ordem_campo." ".$this->ordem;}


		$this->paginas = ceil($this->total/$this->limite);

		if ($this->inicio == -1)
			$this->inicio = 0;
		else
			$this->inicio = ($this->pagina - 1) * $this->limite;

		if($this->limite) $sql .= " LIMIT ".$this->inicio.", ".$this->limite . "";


		$query = mysql_query($sql) or die(header('location: /'));


		if(mysql_num_rows($query) == 0){
			return false;
		} else {
			while ($linha = mysql_fetch_assoc($query)) {
				$linha['nome'] = utf8_decode(htmlentities($linha['nome'], ENT_QUOTES, 'UTF-8'));
				$linha['resumo'] = (!empty($linha['resumo'])) ? utf8_decode(htmlentities($linha['resumo'], ENT_QUOTES, 'UTF-8')) : '';
				$linha['texto'] = (!empty($linha['texto'])) ? $linha['texto'] : '';
				$linha['categoria']	= (!empty($linha['categoria'])) ? $linha['categoria'] : '';
				$linha['chapeu']= (!empty($linha['chapeu'])) ? utf8_decode(htmlentities($linha['chapeu'], ENT_QUOTES, 'UTF-8')) : '';
				$linha['fonte']	= (!empty($linha['fonte'])) ? $linha['fonte'] : '';
				$linha['autor']	= (!empty($linha['autor'])) ? $linha['autor'] : '';

				$linha["url"] = $this->retornaSlugURl($linha["idcategoria"]);

				$this->id_conteudos[] = $linha["idconteudo"];
				$this->retorno[$linha["idconteudo"]][] = $linha;
				if($this->limite_midia) {
					$this->retorno[$linha["idconteudo"]][] = $this->RetornarMidias($linha["idconteudo"]);
				}

			}

			return $this->retorno;
		}

	}

	function RetornaAnivesariente($mes){//DATE_FORMAT(data_nasc,'%Y-%m-%d')
		$sql =  "SELECT DATE_FORMAT(data_nasc,'%d/%m') as dia_mes,DATE_FORMAT(data_nasc,'%m') as mes, DATE_FORMAT(data_nasc,'%d') as dia, nome,observacoes
					FROM usuarios_intranet
				 WHERE ativo = 'S' AND DATE_FORMAT(data_nasc,'%m') = '".mysql_real_escape_string($mes)."' ORDER BY dia ASC ";

		$query = mysql_query($sql);

		$retorno = array();
		while ($linha = mysql_fetch_assoc($query)) {
			$retorno[] = $linha;
		}
		return $retorno;
	}

	function retornaSlug($slug){

		$sql = "select idcategoria from slug_categoria where  url like '".mysql_real_escape_string($slug)."'";
		//echo $sql;
		$verifica = mysql_query($sql) or die('');

		if(mysql_num_rows($verifica) == 0){
			return false;
		} else {

			while($linha = mysql_fetch_assoc($verifica)){
				$retorno = $linha['idcategoria'];
			}
			return $retorno;
		}

	}

	function retornaSlugURl($idcategoria){

		$sql = "select url from slug_categoria where  idcategoria = '".mysql_real_escape_string($idcategoria)."'";
		//echo $sql;
		$verifica = mysql_query($sql) or die('');

		if(mysql_num_rows($verifica) == 0){
			return false;
		} else {

			while($linha = mysql_fetch_assoc($verifica)){
				$retorno = $linha['url'];
			}
			return $retorno;
		}
	}

	function retornaTags(){

		$sql = "SELECT tags. * , tags.nome AS nometags, conteudos.idconteudo AS idconteudotags, conteudos.nome AS conteudonometags
				FROM tags
				INNER JOIN conteudos_tags ON ( tags.idtags = conteudos_tags.idtags )
				INNER JOIN conteudos ON ( conteudos_tags.idconteudo = conteudos.idconteudo )
				WHERE conteudos.idconteudo =  ".$this->idconteudo . "";
					if($this->limite) $sql .= " LIMIT ".$this->limite . "";

		$verifica = mysql_query($sql) or die(header('location: /portal'));

		if(mysql_num_rows($verifica) == 0){
			return false;
		} else {

			while ($linha = mysql_fetch_assoc($verifica)) {
				$linha['nometags'] = utf8_decode(htmlentities($linha['nometags'], ENT_QUOTES, 'UTF-8'));
				$this->retorno[] = $linha;
			}


			return $this->retorno;
		}
		//echo $verifica;
	}
  function ResetaSenha ($usuario){
	// CASO A OPÇÃO PARA RESETAR SEJA UM EMAIL
	if(filter_var($usuario, FILTER_VALIDATE_EMAIL)){
		$email_escape = addslashes($usuario);
		$sql = "SELECT idusuario, email, nome,documento from usuarios_intranet WHERE email = '".$email_escape."' and ativo = 'S' LIMIT 1";
		$seleciona = mysql_query($sql) or die(header('location: http://amase.com.br/intranet'));
		$resultados = mysql_num_rows($seleciona);
		$linha = mysql_fetch_assoc($seleciona);

			if($resultados > 0){
				$senha = '';				$array = array('@','d','i','a','n','!','1','2','#','3','4','&','5','6','7','8','9','w');
				for($i=0; $i<=8; $i++){
					 $senha .= $array[rand(1,18)];
				}

			  	//SQL MUDAR SENHA
			  	$senhanova = GerasenhaSegura($senha);
					$sqlsenha = "update usuarios_intranet set senha = '".$senhanova."' WHERE idusuario = ".$linha['idusuario']." LIMIT 1";
					$executa = mysql_query($sqlsenha) or die(exit());

						if($executa){
							if($linha["email"]){
								    $mensagem  = '<table width="100%" align="center" bgcolor="#f1f1f1" border="0px" cellpadding="0px" cellspacing="10px">
												  <tbody>
													<tr>
													  <td valign="top" align="center"><table style="background-color: rgb(255, 255, 255); border: 1px solid rgb(204, 204, 204);" width="575px" align="center" border="0px" cellpadding="0px" cellspacing="0px">
														  <tbody>
															<tr>
															  <td colspan="6" style="background-color: rgb(238, 238, 238);" valign="middle" width="575" align="center" bgcolor="#fffdf7" height="45"><p style="font: 12px "Lucida Sans Unicode","Lucida Grande",Lucida,Helvetica,Arial,sans-serif; color: rgb(119, 119, 119);">&nbsp;</p></td>
															</tr>
															<tr>
															  <td style="background-color: rgb(255, 255, 255); padding: 0pt 0pt 2px;" width="575">&nbsp;</td>
															  <td valign="bottom" align="center" bgcolor="#ffffff" height="80">
															  <a href="http://amase.com.br" target="_blank">
															  <img src="http://amase.com.br/assets/img/marca-topo.png" alt="http://amase.com.br" style="border-style: none;"  border="0" ></a></td>
															  <td style="background-color: rgb(255, 255, 255); padding: 0pt 0pt 2px;" width="575">&nbsp;</td>
															</tr>
															<tr>
															  <td colspan="6" width="575">&nbsp;</td>
															</tr>
															<tr>
															  <td colspan="6" align="center"><table width="575px" align="center" bgcolor="#ffffff" border="0px" cellpadding="0px" cellspacing="0px">
																  <tbody>
																  <tr>
																	<td colspan="2" width="575" height="30px"><table width="540" align="center" bgcolor="#ffffff" border="0px" cellpadding="8" cellspacing="0px">
																		<tbody>
																		  <tr>
																			<td style="font: 12px "Lucida Sans Unicode","Lucida Grande",Lucida,Helvetica,Arial,sans-serif;"><p><br>
																				</p></td>
																		  </tr>
																		</tbody>
																	  </table></td>
																  </tr>
																	<tr valign="top" align="left">
																	  <td><table width="540" align="center" bgcolor="#ffffff" border="0px" cellpadding="0px" cellspacing="0px">
																		  <tbody>
																			<tr>
																			  <td style="color: rgb(97, 107, 128); font: 20px "Lucida Sans Unicode","Lucida Grande",Lucida,Helvetica,Arial,sans-serif;" bgcolor="#F0F0F0">
																			  <div style="margin:10px;">
																			  Olá <strong>'.$linha["nome"].'</strong>,
																							  <br><br>
																							  Alguém solicitou o envio de uma nova senha.
																							  <br><br>
																							  Acesse:
																							  http://amase.com.br/intranet
																							  <br><br>
																							  Dados de acesso: <strong>'.$linha['documento'].'</strong><br>
																							  Nova Senha de acesso : <strong>'.$senha.'</strong><br/><br/>
																				</div>

																			  </td>
																			</tr>
																			<tr>
																			  <td style="color: rgb(97, 107, 128); font: 12px "Lucida Sans Unicode","Lucida Grande",Lucida,Helvetica,Arial,sans-serif;" bgcolor="#F0F0F0">
																				  <div style="margin:10px;">
																				  <div>******************************<wbr></wbr>****</div>
																				  Estamos à total disposição!<br>
																				  <div>******************************<wbr></wbr>****</div>
																				  <div>
																				  Atenciosamente,<br>
																				  Atendimento Amase<a target="_blank" href="http://amase.com.br">amase.com.br</a>
																				  <br><br>
																				  </div>
																				</td>
																			</tr>
																			<tr>
																			  <td bgcolor="#F0F0F0"><br></td>
																			</tr>
																			<tr>
																			  <td style="font: 20px "Lucida Sans Unicode","Lucida Grande",Lucida,Helvetica,Arial,sans-serif;" bgcolor="#FFFFFF">&nbsp;</td>
																			</tr>
																		  </tbody>
																		</table></td>
																	</tr>
																	<tr>
																	  <td colspan="3" style="background-color: rgb(240, 240, 240);" valign="middle" width="575" align="center" bgcolor="#f0f0f0" height="45"><p style="font: 12px "Lucida Sans Unicode","Lucida Grande",Lucida,Helvetica,Arial,sans-serif; color: rgb(119, 119, 119);">&nbsp;</p></td>
																	</tr>
																	<tr>
																	  <td colspan="3" width="575"><table style="background-color: #b3d4fc; padding: 13px;font-family:MyriadProCondRegular;" width="575px" border="0px" cellpadding="0px" cellspacing="0px">
																		  <tbody>
																			<tr>
																			  <td align="center" style="color:#6b6862;">
																				 :: AMASE - Associação dos magistrados de Sergipe ::
																			   </td>
																			  <td width="15px">&nbsp;</td>
																			</tr>
																		  </tbody>
																		</table></td>
																	</tr>
																  </tbody>
																</table></td>
															</tr>
														  </tbody>
														</table></td>
													</tr>
												  </tbody>
												</table>';

										  $recipient  = $linha["email"];
										  $assunto  = "Esqueci minha senha";


											$mail = new PHPMailer();
											$mail->IsSMTP(); // telling the class to use SMTP
											$mail->SMTPAuth   = true;
											// $mail->SMTPSecure = "ssl";
											$mail->Host       = SMTP_HOST;
											$mail->Username   = SMTP_USERNAME;
											$mail->Password   = SMTP_PASSWORD;
											$mail->Port       = SMTP_PORT;
											$mail->CharSet 	= 'UTF-8';
											$mail->SMTPDebug  = 0;

											// monta e-mail
											$mail->From 		= SMTP_FROM; // Seu e-mail
											$mail->FromName 	= "Site"; // Seu nome
											$mail->Subject 	= $assunto;
											$mail->AddAddress($linha["email"]);
											$mail->AddAddress('toninho@grupoalfama.com.br');

											$mail->Body = $mensagem;
											$mail->IsHTML(true);
											$enviado = $mail->Send();

										  if($enviado){
											  $retorno["sucesso"] = true;
											  $retorno["msg"] = "Uma nova senha foi enviado para o e-mail cadastrado.";
										  }else{
											  $retorno["sucesso"] = false;
											  $retorno["msg"] = "Ocorreu um erro ao enviar uma nova senha favor tentar novamente.";
										  }

							}else{
							 	$retorno["sucesso"] = false;
								$retorno["msg"] = "O usuario informado não pode utilizar essa funcionalida pois não possui email cadastrado, favor contatar o administrador do sistema.";
							}
						}else{
							$retorno["sucesso"] = false;
							$retorno["msg"] = "Ocorreu um erro ao gerar uma nova senha favor tentar novamente";
						}

			}else{
				$retorno["sucesso"] = false;
				$retorno["msg"] = "Usuário não encontrado.";
			}

	// CASO O USUARIO TENHA INFORMADO UM CPF
	}else if(is_numeric($usuario)){
		$matricula_escape = addslashes($usuario);
		$sql = "SELECT idusuario, email, nome,documento from usuarios_intranet WHERE matricula = '".$matricula_escape."' and ativo = 'S' LIMIT 1";
		$seleciona = mysql_query($sql) or die(header('location: http://amase.com.br/intranet'));
		$resultados = mysql_num_rows($seleciona);
		$linha = mysql_fetch_assoc($seleciona);

			if($resultados > 0){
				$array = array('@','d','i','a','n','!','1','2','#','3','4','&','5','6','7','8','9','w');
				for($i=0; $i<=8; $i++){
					 $senha .= $array[rand(1,18)];
				}

			  	//SQL MUDAR SENHA
			  	$senhanova = GerasenhaSegura($senha);
					$sqlsenha = "update usuarios_intranet set senha = '".$senhanova."' WHERE idusuario = ".$linha['idusuario']." LIMIT 1";
					$executa = mysql_query($sqlsenha) or die(exit());

						if($executa){
							if($linha["email"]){
								  $mensagem  = '<table width="100%" align="center" bgcolor="#f1f1f1" border="0px" cellpadding="0px" cellspacing="10px">
												  <tbody>
													<tr>
													  <td valign="top" align="center"><table style="background-color: rgb(255, 255, 255); border: 1px solid rgb(204, 204, 204);" width="575px" align="center" border="0px" cellpadding="0px" cellspacing="0px">
														  <tbody>
															<tr>
															  <td colspan="6" style="background-color: rgb(238, 238, 238);" valign="middle" width="575" align="center" bgcolor="#fffdf7" height="45"><p style="font: 12px "Lucida Sans Unicode","Lucida Grande",Lucida,Helvetica,Arial,sans-serif; color: rgb(119, 119, 119);">&nbsp;</p></td>
															</tr>
															<tr>
															  <td style="background-color: rgb(255, 255, 255); padding: 0pt 0pt 2px;" width="500">&nbsp;</td>
															  <td valign="bottom" align="center" bgcolor="#ffffff" height="78">
															  <a href="' . SITE . '" target="_blank">
															  <img src="https://www.celi.com.br/img/logo.png?1513602910" alt="' . SITE . '" style="border-style: none;"  border="0" ></a>
															  </td>
															  <td style="background-color: rgb(255, 255, 255); padding: 0pt 0pt 2px;" width="575">&nbsp;</td>
															</tr>
															<tr>
															  <td colspan="6" align="center"><table width="575px" align="center" bgcolor="#ffffff" border="0px" cellpadding="0px" cellspacing="0px">
																  <tbody>
																  <tr>
																	  <td colspan="2" width="575" height="30px"><table width="540" align="center" bgcolor="#ffffff" border="0px" cellpadding="8" cellspacing="0px">
																		  <tbody>
																			<tr>
																			  <td style="font: 12px "Lucida Sans Unicode","Lucida Grande",Lucida,Helvetica,Arial,sans-serif;"><p><br>
																				  </p></td>
																			</tr>
																		  </tbody>
																		</table></td>
																	</tr>
																	<tr valign="top" align="left">
																	  <td><table width="540" align="center" bgcolor="#ffffff" border="0px" cellpadding="0px" cellspacing="0px">
																		  <tbody>
																			<tr>
																			  <td style="color: rgb(97, 107, 128); font: 20px "Lucida Sans Unicode","Lucida Grande",Lucida,Helvetica,Arial,sans-serif;" bgcolor="#F0F0F0">
																			  <div style="margin:10px;">
																			  Olá <strong>'.$linha["nome"].'</strong>,
																							  <br><br>
																							  Alguém solicitou o envio de uma nova senha.
																							  <br><br>
																							  Acesse:
																							  ' . SITE . '
																							  <br><br>
																							  Dados de acesso: <strong>'.$linha['email'].'</strong><br>
																							  Nova Senha de acesso : <strong>'.$senha.'</strong><br/><br/>
																				</div>

																			  </td>
																			</tr>
																			<tr>
																			  <td style="color: rgb(97, 107, 128); font: 12px "Lucida Sans Unicode","Lucida Grande",Lucida,Helvetica,Arial,sans-serif;" bgcolor="#F0F0F0">
																				  <div style="margin:10px;">
																				  <div>******************************<wbr></wbr>****</div>
																				  Estamos à total disposição!<br>
																				  <div>******************************<wbr></wbr>****</div>
																				  <div>
																				  Atenciosamente, Celi.
																				  <br>
																				  </div>
																				</td>
																			</tr>
																			<tr>
																			  <td bgcolor="#F0F0F0"><br></td>
																			</tr>
																			<tr>
																			  <td style="font: 20px "Lucida Sans Unicode","Lucida Grande",Lucida,Helvetica,Arial,sans-serif;" bgcolor="#FFFFFF">&nbsp;</td>
																			</tr>
																		  </tbody>
																		</table></td>
																	</tr>
																	<tr>
																	  <td colspan="3" style="background-color: rgb(240, 240, 240);" valign="middle" width="575" align="center" bgcolor="#f0f0f0" height="45"><p style="font: 12px "Lucida Sans Unicode","Lucida Grande",Lucida,Helvetica,Arial,sans-serif; color: rgb(119, 119, 119);">&nbsp;</p></td>
																	</tr>
																  </tbody>
																</table></td>
															</tr>
														  </tbody>
														</table></td>
													</tr>
												  </tbody>
												</table>';



										  $recipient  = $linha["email"];
										  $assunto  = "Esqueci minha senha";


											$mail = new PHPMailer();
											$mail->IsSMTP(); // telling the class to use SMTP
											$mail->SMTPAuth   = true;
											$mail->SMTPSecure = "ssl";
											$mail->Host       = SMTP_HOST;
											$mail->Username   = SMTP_USERNAME;
											$mail->Password   = SMTP_PASSWORD;
											$mail->Port       = SMTP_PORT;
											$mail->CharSet 	= 'UTF-8';
											$mail->SMTPDebug  = 0;

											// monta e-mail
											$mail->From 		= SMTP_FROM; // Seu e-mail
											$mail->FromName 	= "Site "; // Seu nome
											$mail->Subject 	= $assunto;
											$mail->AddAddress($linha["email"]);

											$mail->Body = $mensagem;
											$mail->IsHTML(true);
											$enviado = $mail->Send();

										  if($enviado){
											  $retorno["sucesso"] = true;
											  $retorno["msg"] = "Uma nova senha foi enviado para o e-mail cadastrado.";
										  }else{
											  $retorno["sucesso"] = false;
											  $retorno["msg"] = "Ocorreu um erro ao enviar uma nova senha favor tentar novamente.";
										  }

							}else{
							 	$retorno["sucesso"] = false;
								$retorno["msg"] = "O usuario informado não pode utilizar essa funcionalida pois não possui email cadastrado, favor contatar o administrador do sistema.";
							}
						}else{
							$retorno["sucesso"] = false;
							$retorno["msg"] = "Ocorreu um erro ao gerar uma nova senha favor tentar novamente";
						}

			}else{
				$retorno["sucesso"] = false;
				$retorno["msg"] = "Usuário não encontrado.";
			}
	// CASO NÃO SEJA CPF OU EMAIL SAI E EXIBVI O ERRO
	}else{
		$retorno["sucesso"] = false;
		$retorno["msg"] = "Email ou cpf inválido! Favor tentar novamente.";
	}

	return $retorno;
	}

	function retornaVideos(){

	$this->retorno = NULL;

	  $sql = "SELECT {$this->campos}
			  FROM videos v
			  INNER JOIN videos_categorias vc ON(v.idvideo = vc.idvideo)
			  INNER JOIN categorias ct ON(vc.idcategoria = ct.idcategoria)
			  INNER JOIN categorias_modulos cm ON (cm.idcategoria  = ct.idcategoria)
			  WHERE  ";

		if ($this->idvideo) {
			$sql .= " v.idvideo   = '".$this->idvideo."'  AND";
			$this->contarAcessoVideo();
		}
		if ($this->idvideo_n) {
			$sql .= " v.idvideo   <> '".$this->idvideo_n."'  AND";

		}
		if ($this->idpasta) {
			$sql .= " v.idpasta   IN('".$this->idpasta."')  AND";
		}
		if ($this->idpasta_n) {
			$sql .= " v.idpasta   NOT IN('".$this->idpasta_n."')  AND";
		}
		if ($this->tipo) {
			$sql .= " v.tipo   = '".$this->tipo."'  AND";

		}
		if ($this->idcategoria){	$sql .= "  vc.idcategoria = '".$this->idcategoria."' AND ";}

		if ($this->data_entrada){	$sql .= "  DATE_FORMAT( v.data_entrada,  '%Y-%m-%d' ) =  '".$this->data_entrada."' AND ";}

		if (is_array($this->idsite)){
			$sql .= "  ct.idsite      in( ".join(",",$this->idsite).") AND";
		}elseif ($this->idsite){
			$sql .= "  ct.idsite      = '".$this->idsite."' AND";
		}
		if ($this->busca){
		$busca = preg_replace("/&([a-z])[a-z]+;/i", "$1", htmlentities($this->busca));///$this->acentos_str();
		$sql .= " ((v.nome LIKE '%".$busca."%') OR (v.resumo LIKE '%".$busca."%')) AND  ";
		}
		$sql .= "  v.situacao = 'S'  ";

		$sql .= " AND DATE_FORMAT( v.data_entrada,  '%Y-%m-%d' ) <=  DATE_FORMAT( NOW() ,  '%Y-%m-%d' )  AND (DATE_FORMAT( v.data_saida,  '%Y-%m-%d' ) >=  DATE_FORMAT( NOW() ,  '%Y-%m-%d' ) OR ISNULL(v.data_saida)) ";

		$sql .= " GROUP BY v.idvideo ";
		if(is_null($this->idvideo)) {
			$sqlCount = str_replace($this->campos, "COUNT(*) AS total", $sql);
			$query2 = mysql_query($sqlCount) or die(header('location: /portal'));
			$total = mysql_num_rows($query2);
			$this->total = $total;

		} else {
			$this->total = 1;
		}

		$this->paginas = ceil($this->total/$this->limite);


		if($this->ordem_campo){ $sql .= " ORDER BY ".$this->ordem_campo." ".$this->ordem;}

		//echo $sql;

		/*
		Eu preciso disso? O total ja nao esta em baixo?
		$query = mysql_query($sql) or die(header('location: /portal'));
		$num = mysql_num_rows($query);
		*/

		if ($this->inicio == -1)
		   $this->inicio = 0;
		else
		   $this->inicio = ($this->pagina - 1) * $this->limite;

		if($this->limite) $sql .= " LIMIT ".$this->inicio.", ".$this->limite . "";
	if($_GET['aa']=='sql'){
			echo $sql ;
		}
		$query = mysql_query($sql) or die(header('location: /portal'));

		//echo $sql;
		//echo "<!--SQL: ".$sql."-->";
		if(mysql_num_rows($query) == 0){
			return false;
		} else {
			while ($linha = mysql_fetch_assoc($query)) {

				$linha['nome']		= utf8_decode(htmlentities($linha['nome'], ENT_QUOTES, 'UTF-8'));
				$linha['resumo']	= utf8_decode(htmlentities($linha['resumo'], ENT_QUOTES, 'UTF-8'));
				$this->retorno[] = $linha;

			}

			return $this->retorno;
		}

	}

	function RetornarMidias($idconteudo) {
		$retorno = array();
			$sql = "SELECT idconteudo_midia,idconteudo,legenda,variavel,capa,avatar_nome,avatar_tipo,avatar_servidor ,avatar_tamanho
							FROM conteudos_midias
						  WHERE idconteudo = ".$idconteudo."  AND ativo = 'S' ";
		   $sql .=	"ORDER BY CAPA ASC";
		    if($this->limite_midia) $sql .= " LIMIT ".$this->limite_midia . "";

			$query = mysql_query($sql) or die(header('location: /portal'));

			while($linha = mysql_fetch_assoc($query)){
				$linha['legenda']	=  utf8_decode(htmlentities($linha['legenda'], ENT_QUOTES, 'UTF-8'));
				$retorno[] = $linha;
			}

			return $retorno;

	    }

	function RetornarEstados() {

			$sql = "SELECT e.*
					FROM estados e
					INNER JOIN ext_empreendimentos ee ON (e.idestado = ee.idestado)
					GROUP BY e.idestado";

			$query = mysql_query($sql) or die(header('location: /portal'));

			while($linha = mysql_fetch_assoc($query)){
				$retorno[] = $linha;
			}

			return $retorno;

	    }

	function RetornaDadosUsuario($idusuario) {
		$sql = "SELECT ".$this->campos."
				FROM usuarios_intranet
				WHERE idusuario = ".mysql_real_escape_string($idusuario)." ";
		$query = mysql_query($sql) or die('Erro');
		$linha = mysql_fetch_assoc($query);

		return $linha;
		}

	function VerificaEmailUsuario ($email){
		$sql = "SELECT COUNT(idusuario)  as total FROM usuarios_intranet WHERE email = '".$email."' AND ativo = 'S' ";
		$query = mysql_query($sql) or die('Erro consulta dados');
		$linha = mysql_fetch_assoc($query);
			if($linha["total"] > 0){
				return false;
			}else{
				return true;
			}
		}

	function AlterarDados($idusuario) {
		if ($_POST["senha-nova"]) {

			$sql = "SELECT COUNT(idusuario)  as total FROM usuarios_intranet WHERE idusuario = ".$idusuario." AND ativo = 'S' AND senha = '" . GerasenhaSegura(protect($_POST["senha-antiga"])) . "' ";
					
			$query = mysql_query($sql) or die('Erro consulta dados');
			$linha = mysql_fetch_assoc($query);

			if($linha["total"] == 0) {
				return array('erro' => 'Senha atual inválida');
			}

			if ($_POST["senha-nova"] != $_POST["senha-nova-confirmacao"]) {
				return array('erro' => 'A nova senha e a confirmação de senha não são iguais');
			}
		}

		$sql = "UPDATE usuarios_intranet SET
				nome = '".protect($_POST["nome"])."',
				observacoes = '".protect($_POST["observacoes"])."',
				ramal = '".protect($_POST["ramal"])."',
				setor = '".protect($_POST["setor"])."' ";
		
		if($this->VerificaEmailUsuario(protect($_POST["email"]))){
			$sql .=", email = '".protect($_POST["email"])."' ";
		}
		
		if($_POST["senha-nova"]){
			$senha = GerasenhaSegura(protect($_POST["senha-nova"]));
			$sql .= ", senha = '".$senha."' ";
		}
		
		$sql .= "WHERE idusuario = ".$idusuario." ";
		$query = mysql_query($sql) or die('Erro atualiza dados');
		
		if ($query) {
			return true;
		} else {
			return false;
		}
	}

	function upload($arquivo,$caminho){
		if(!(empty($arquivo))){
			$arquivo1 = $arquivo;
			$arquivo_minusculo = strtolower($arquivo1['name']);
			$caracteres = array("�","~","^","]","[","{","}",";",":","�",",",">","<","-","/","|","@","$","%","�","�","�","�","�","�","�","�","+","=","*","&","(",")","!","#","?","`","�"," ","�");
			$arquivo_tratado = str_replace($caracteres,"",$arquivo_minusculo);
			$destino = $caminho;
			   
			if(move_uploaded_file($arquivo1['tmp_name'],$destino)){
				return true;
			}else{
				return false;
			}
		}
	}

	function RetornaCidades($id = NULL){
			$sql = "SELECT c.*
						FROM cidades c
						INNER JOIN ext_empreendimentos ee ON (c.idcidade = ee.idcidade) ";
				if($id){
				$sql .=" WHERE c.idestado = ".$id." ";
				}else
				if($this->idcidade){
				$sql .=" WHERE c.idcidade = ".$this->idcidade." ";
				}else
				if($id && $this->idcidade){
				$sql .=" WHERE c.idcidade = ".$this->idcidade." AND c.idestado = ".$id." ";
				}
				$sql .=" GROUP BY c.idcidade";
		    $query = mysql_query($sql) or die(header('location: /portal'));
			//echo $sql;
			while($linha = mysql_fetch_assoc($query)){
				$retorno[] = $linha;
			}
		if($id){
			echo json_encode($retorno);
		}else{
			return $retorno;
		}
		}

	function contarAcesso(){
		$sql = "UPDATE  conteudos SET  lido = lido+1 WHERE  idconteudo = '".$this->idconteudo."' LIMIT 1" ;
		mysql_query($sql) or die(header('location: /portal'));
	}

	function contarAcessoVideo(){
		$sql = "UPDATE  videos SET  lido = lido+1 WHERE  idvideo = '".$this->idvideo."' LIMIT 1" ;
		mysql_query($sql) or die(header('location: /portal'));
	}

	function retornaConteudoCategoria(){

		   $campos = "idconteudo, idsite, idcategoria,	chapeu, titulo,	nome_autor, html,  resumo,video_youtube,imagem1, img_legenda1,img_credito1, imagem2, img_legenda2,img_credito2, imagem3, img_legenda3,img_credito3, imagem4, img_legenda4,img_credito4,	hotsite	,de	,ate,datacad, datasalvo,	saiba_mais,	posicao, click, link, est_obra, anda_infra	";

		   $sql = "select $campos from conteudo where idcategoria in(".$this->idcategoria.") and idsite = ".$this->idsite." and aprovado = 1
		   		   AND de <= '".date("Y-m-d H:i:s")."'AND ((ate >= '".date("Y-m-d H:i:s")."') OR (ate = '0000-00-00 00:00:00')) ";
			//	$sql .=" AND (DATE_FORMAT(datacad, '%Y-%c') = ".date("Y-m")." OR DATE_FORMAT(datacad, '%Y-%c') = ".date("Y-m").")";

			 if ($this->destaque == 1){$sql .= " AND destaque = '1'";}
			//echo "<!--".$sql."-->";
		$query = mysql_query($sql) or die(header('location: /portal'));
		$num = mysql_num_rows($query);

		$sqlCount = str_replace("*", "COUNT(*) AS total", $sql);
		$query2 = mysql_query($sqlCount) or die(header('location: /portal'));

		$total = mysql_fetch_array($query2);
		$this->total = ceil($total["total"]/$this->limite);
		$this->paginas = ceil($num/$this->limite);
		if ($this->inicio == -1){
		   $this->inicio = 0;
		}else{
		   $this->inicio = ($this->pagina - 1) * $this->limite;
		}

		   if($this->ordem_campo == "click"){
			   $sql .= " order by click desc ";
			}else{
				 $sql .= " order by idconteudo desc ";
			}

		if($this->limite) $sql .= " LIMIT ".$this->inicio.", ".$this->limite . "";
			 	//echo "<!--".$sql."-->";
			$query = mysql_query($sql) or die(header('location: /portal'));
				if(mysql_num_rows($query) == 0){

					return false;
				} else {
					while ($linha = mysql_fetch_array($query)) {
					$this->retorno[] = $linha;
				}
			return $this->retorno;

			}
	}

	function retornaGaleriaCategoria($idcategoria){
		if ($idcategoria) {
			$sql = "SELECT *
					FROM galerias ga
					INNER JOIN galerias_categorias gc ON (gc.idgaleria = ga.idgaleria )
					WHERE gc.idcategoria  = $idcategoria and status <> 'R' AND ga.ativo = 'S' ";
		if($this->ordem_campo){ $sql .= " ORDER BY ".$this->ordem_campo." ".$this->ordem;}

		$query = mysql_query($sql) or die(header('location: /home'));
		$num = mysql_num_rows($query);
		$this->paginas = ceil($num/$this->limite);

		if ($this->inicio == -1)
		   $this->inicio = 0;
		else
		   $this->inicio = ($this->pagina - 1) * $this->limite;

		if($this->limite) $sql .= " LIMIT ".$this->inicio.", ".$this->limite . "";

		$query = mysql_query($sql) or die(header('location: /home'));

		$sqlCount = str_replace("*", "COUNT(*) AS total", $sql);
		$query2 = mysql_query($sqlCount) or die(header('location: /home'));

		$total = mysql_fetch_assoc($query2);
		$this->total = $total["total"];

			$query = mysql_query($sql) or die(header('location: /home'));
			while ($linha = mysql_fetch_assoc($query)) {
				$retorno[] = $linha;
			}
			$i=0;
			if(!empty($retorno) && count($retorno) > 0){
			foreach($retorno as $ind => $ln){
				$this->retorno[$i] = $ln;
				$sqlf = "SELECT *
							FROM galerias_fotos
						WHERE idgaleria = ".$ln["idgaleria"]." AND 
							ativo = 'S'
	                    ORDER BY ordem";
				 $queryf = mysql_query($sqlf) or die("erro");
				 while ($foto = mysql_fetch_assoc($queryf)) {
					 $this->retorno[$i]["foto"][] = $foto;
				 }

			$i++;
			}
			}
			return $this->retorno;
		}
	}

	function contarAcessoGaleria($idgaleria){
		$sqlg = "UPDATE  galerias SET  lido = lido+1 WHERE  idgaleria = '".$idgaleria."' LIMIT 1" ;
		mysql_query($sqlg) or die(header('location: /home'));
	}

	function retornaGaleriaCategoriaMaisLido($idcategoria){
		if ($idcategoria) {
			$sql = "SELECT *
					FROM galerias ga
					INNER JOIN galerias_categorias gc ON (gc.idgaleria = ga.idgaleria )
					WHERE gc.idcategoria  IN ($idcategoria) and status <> 'R' AND ga.ativo = 'S'
					ORDER BY ga.lido DESC ";

		$query = mysql_query($sql) or die(header('location: /portal'));
		$num = mysql_num_rows($query);
		$this->paginas = ceil($num/$this->limite);

		if ($this->inicio == -1)
		   $this->inicio = 0;
		else
		   $this->inicio = ($this->pagina - 1) * $this->limite;

		if($this->limite) $sql .= " LIMIT ".$this->inicio.", ".$this->limite . "";

		$query = mysql_query($sql) or die(header('location: /portal'));

		$sqlCount = str_replace("*", "COUNT(*) AS total", $sql);
		$query2 = mysql_query($sqlCount) or die(header('location: /portal'));

		$total = mysql_fetch_assoc($query2);
		$this->total = $total["total"];

			$query = mysql_query($sql) or die(header('location: /portal'));
			while ($linha = mysql_fetch_assoc($query)) {
				//$this->retorno[] = $linha;
				$retorno[] = $linha;
			}
			$i=0;
			if(count($retorno) > 0){
			foreach($retorno as $ind => $ln){
				$this->retorno[$i] = $ln;

				$sqlf = "SELECT *
							FROM galerias_fotos
						WHERE idgaleria = ".$ln["idgaleria"]."
	                    ORDER BY idgaleria_foto DESC LIMIT 1";
	 			$queryf = mysql_query($sqlf) or die("erro");
				$foto = mysql_fetch_array($queryf);

				$this->retorno[$i]["foto"] = $foto;
			$i++;
			}
			}
			return $this->retorno;
		}
	}

	function retornaGaleria($idconteudo){
		if ($idconteudo) {
			$sql = "SELECT * FROM galerias ga
					INNER JOIN galerias_fotos g ON ( ga.idgaleria = g.idgaleria )
					INNER JOIN conteudos_galerias cg ON ( g.idgaleria = cg.idgaleria )
					WHERE cg.idconteudo = $idconteudo AND g.ativo = 'S'";
			$query = mysql_query($sql);

			while ($linha = mysql_fetch_array($query)) {
				$this->retorno[] = $linha;
			}
			return $this->retorno;
		}
	}

	function retornarUltimaImagem($idgal){

	 $sql = "SELECT * FROM galerias_fotos WHERE idgaleria = '".$idgal."' ";
	// if($this->privada) $sql .= " AND (privada = '".$this->privada."')";
	$sql .= " ORDER BY idgaleria_foto DESC LIMIT 1";
	 $query = mysql_query($sql) or die("erro");
	 $this->retorno = mysql_fetch_array($query);
	 return $this->retorno;
  	}

	function retornaTodasFotosGaleria($idgaleria){
			$sql = "SELECT g.*
					FROM galerias_fotos g
					WHERE g.idgaleria = '".$idgaleria."' and g.ativo = 'S' and (g.privada = 'N' OR g.privada IS NULL)
					ORDER BY g.ordem ";
					if($this->limite_galeria) $sql .= " LIMIT ".$this->limite_galeria." ";

			$query = mysql_query($sql) or die(header('location: /home'));
			while ($linha = mysql_fetch_assoc($query)) {
				$retorno[] = $linha;
			}
			return $retorno;
	}

	function retornaFotosGaleria($idgaleria) {
		if ($idgaleria) {
			if($this->cotar){
				$this->contarAcessoGaleria($idgaleria);
			}
			$sql = "SELECT ga.idgaleria,ga.nome as nome_galeria,ga.chapeu,g.*
					FROM galerias ga
					INNER JOIN galerias_fotos g ON ( ga.idgaleria = g.idgaleria )
					WHERE  ga.idgaleria = '".$idgaleria."' and g.ativo = 'S' and (g.privada = 'N' OR g.privada IS NULL) ORDER BY g.ordem ";
					if($this->limite) $sql .= " LIMIT ".$this->limite." ";
					//echo $sql;exit;
			$query = mysql_query($sql) or die(header('location: /home'));
			while ($linha = mysql_fetch_assoc($query)) {
				$retorno[] = $linha;
			}
			return $retorno;
		}
	}

	function retornaFotosGaleriaConteudo($idconteudo) {

		if ($idconteudo) {
			$sql = "SELECT ga.idgaleria,ga.nome as nome_galeria,ga.chapeu,g.*
					FROM galerias ga
					INNER JOIN conteudos_galerias cg ON (ga.idgaleria = cg.idgaleria)
					INNER JOIN galerias_fotos g ON (ga.idgaleria = g.idgaleria)
					WHERE  cg.idconteudo = '".$idconteudo."' and g.ativo = 'S' and (g.privada = 'N' OR g.privada IS NULL)
					ORDER BY g.ordem ";
					if($this->limite) $sql .= " LIMIT ".$this->limite." ";
					//echo $sql;
			$query = mysql_query($sql) or die(header('location: /portal'));
			while ($linha = mysql_fetch_assoc($query)) {
				$this->retorno[] = $linha;
			}
			return $this->retorno;
		}
	}

	function retornaCategoria(){

		$sql = "select * from categorias where idcategoria='".$this->idcategoria."'";
		$verifica = mysql_query($sql) or die(header('location: /home'));
		//echo $sql;
		if(mysql_num_rows($verifica) == 0){
			return false;
		} else {
			return mysql_fetch_array($verifica);
		}

	}

	function retornaPalavrasChaves(){

	$this->retorno = NULL;

	  $sql = "SELECT {$this->campos}
			  FROM palavras_chaves pc
			  INNER JOIN palavras_categoria pct ON(pc.idpalavra = pct.idpalavra)
			  WHERE  ";

		if ($this->idpalavra) {
			$sql .= " pc.idpalavra   = '".$this->idpalavra."'  AND";
		}
		if ($this->idcategoria){	$sql .= "  pct.idcategoria = '".$this->idcategoria."' AND ";}

		if (is_array($this->idsite)){
			$sql .= "  pc.idsite      in( ".join(",",$this->idsite).") AND";
		}elseif ($this->idsite){
			$sql .= "  pc.idsite      = '".$this->idsite."' AND";
		}

		$sql .= "  pc.ativo = 'S'  AND pc.situacao = 'S'  ";

		$sql .= " AND DATE_FORMAT( pc.data_entrada,  '%Y-%m-%d' ) <=  DATE_FORMAT( NOW() ,  '%Y-%m-%d' )  AND (DATE_FORMAT( pc.data_saida,  '%Y-%m-%d' ) >=  DATE_FORMAT( NOW() ,  '%Y-%m-%d' ) OR ISNULL(pc.data_saida)) ";
		$sql .= "  GROUP BY   pc.idpalavra ";

		$sql .= " ORDER BY pc.ordem ASC ";

		$query = mysql_query($sql) or die(header('location: /portal'));
		$num = mysql_num_rows($query);
		$this->paginas = ceil($num/$this->limite);

		if ($this->inicio == -1)
		   $this->inicio = 0;
		else
		   $this->inicio = ($this->pagina - 1) * $this->limite;

		if($this->limite) $sql .= " LIMIT ".$this->inicio.", ".$this->limite . "";
		$query = mysql_query($sql) or die(header('location: /portal'));

		/*$sqlCount = str_replace($this->campos, "COUNT(*) AS total", $sql);
		$query2 = mysql_query($sqlCount) or die(header('location: /portal'));

		$total = mysql_fetch_assoc($query2);
		$this->total = $total["total"];

		FOi comentado por nao haver necessidade de colocar total

		*/
		if(mysql_num_rows($query) == 0){
			return false;
		} else {
			while ($linha = mysql_fetch_assoc($query)) {
				$linha['nome']	= utf8_decode(htmlentities($linha['nome'], ENT_QUOTES, 'UTF-8'));
				//$linha['url']	= utf8_decode($linha['url']);
				$this->retorno[] = $linha;

			}

			return $this->retorno;
		}

	}


  	function retornaTipoSite($idcategoria){

		$sql = "SELECT s.site from sites s
				INNER JOIN categorias c ON(s.idsite = c.idsite) where  c.idcategoria = '".$idcategoria."'";
		//echo $sql;
		$verifica = mysql_query($sql) or die(header('location: /portal'));

		if(mysql_num_rows($verifica) == 0){
			return false;
		} else {

			while($linha = mysql_fetch_assoc($verifica)){
				$retorno = $linha['site'];
			}
			return $retorno;
		}

	}

  function cadastrarUserNews($email=false,$nome=false,$categoria=false) {

		if(!$email)$email = NULL;
		if(!$nome)$nome = NULL;
		if(!$categoria)$categoria = NULL;

		//verificaçoes
		$sql_verifica = "select * from newsletter where email = '".$email."' and ativo='S'";
		$query_verifica = mysql_query($sql_verifica) or die(header('location: /portal'));
		$existe = mysql_num_rows($query_verifica);

		if(!$existe ){

			$sql_conteudo = "INSERT INTO newsletter set  data_cad = now(),  ativo = 'S',  nome = '".$nome."',  email = '".$email."';";
			$executa = mysql_query($sql_conteudo) or die(include($_SERVER["DOCUMENT_ROOT"]."/_includes/mysql_error.php"));
			$sql_categoria = "INSERT INTO newsletter_categorias (idnewsletter,idcategoria) values ('".mysql_insert_id()."','".$categoria."');";
			$executa = mysql_query($sql_categoria) or die(include($_SERVER["DOCUMENT_ROOT"]."/_includes/mysql_error.php"));
			return true;
		}else{
			return false;
		}


	}

	function cadastrarEmail() {
			
		mysql_query("START TRANSACTION");
				
		$sql_conteudo = "INSERT INTO contatos 
							SET  
							  data_cad = now(),  
							  ativo = 'S',  
							  nome = '" . $_POST["nome"] . "',  
							  idsite = '1',  
							  email = '" . $_POST["email"] . "', 
							  assunto = '" . $_POST["assunto"] . "'";

		if (!empty($_POST["mensagem"])) {
			$sql_conteudo	.= ", mensagem = '" . $_POST["mensagem"] . "'";
		}

		if (!empty($_POST["telefone"])) {
			$sql_conteudo	.= ", telefone = '" . $_POST["telefone"] . "'";
		}

		if (!empty($_POST["celular"])) {
			$sql_conteudo	.= ", celular = '" . $_POST["celular"] . "'";
		}

		if (!empty($_POST["documento"])) {
			$sql_conteudo	.= ", documento = '" . $_POST["documento"] . "'";
		}

		if (!empty($_POST["sexo"])) {
			$sql_conteudo	.= ", sexo = '" . $_POST["sexo"] . "'";
		}
		
		if (!empty($_POST["endereco"])) {
			$sql_conteudo	.= ", endereco = '" . $_POST["endereco"] . "'";
		}
		
		if($_FILES["arquivo"] && $_FILES["arquivo"]["error"] == 0){
			$extensao = strtolower(strrchr($_FILES["arquivo"]["name"], "."));
			$nome_servidor = date("YmdHis")."_".uniqid().$extensao;		

			// $_SERVER['DOCUMENT_ROOT']."/sgw/storage/talentos/1/".$nome_servidor,
			$enviou = $this->upload($_FILES["arquivo"],$_SERVER['DOCUMENT_ROOT'] . "/../sgw/storage/contato/1/".$nome_servidor);
			if($enviou){
				$sql_conteudo .= ",avatar_nome =  '".$_FILES["arquivo"]["name"]."',
									avatar_servidor = '".$nome_servidor."',
									avatar_tipo = '".$_FILES["arquivo"]["type"]."',
									avatar_tamanho = '".$_FILES["arquivo"]["size"]."'
									";
			}
		
		}

		$executa = mysql_query($sql_conteudo) or die();
		
		$sql_categoria = "INSERT INTO contatos_categorias (idcontato,idcategoria) VALUES ('".mysql_insert_id()."',".$this->idcategoria.");";
		$executa2 = mysql_query($sql_categoria) or die();
		if($executa && $executa2){
			mysql_query("COMMIT");echo "<!--CERTO-->";
			return true;
		}else{
			mysql_query("ROLLBACK");echo "<!--ERRO-->";
			return false;
		}
				
	}

	function RetornaComodos($id){
		$sql = "SELECT c.nome,ce.quantidade,ce.idcomodo
					FROM ext_comodos c 
				INNER JOIN ex_comodo_empreendimento ce ON (c.idcomodo = ce.idcomodo)
				WHERE idempreendimento = {$id} ";
		if ($this->comodo){$sql .= " AND ce.idcomodo = ".$this->comodo." ";}
		if($this->grougby)
		{
		  $sql .= " GROUP BY ".$this->grougby." ";
		}
		$sql .= " ORDER BY c.nome ";
		if($this->limite) $sql .= " LIMIT 0, ".$this->limite . "";
		// echo "<!-- esta aqui <pre>",print_r($sql,1),"</pre> -->";
		$query = mysql_query($sql);		
		while ($linha = mysql_fetch_assoc($query)) {						
			$retorno[] = $linha;					
		}	
		return $retorno;	
	  }

	function RetornarEmpreendimento() {

		$sql = "SELECT ".$this->campos."
				  FROM ext_empreendimentos e
					LEFT JOIN ext_empreendimentos_categorias ec ON (e.idempreendimento = ec.idempreendimento)
					LEFT JOIN categorias c ON (ec.idcategoria = c.idcategoria)
				  INNER JOIN cidades ci ON (ci.idcidade = e.idcidade)
					LEFT JOIN estados est ON (e.idestado = est.idestado)
				  LEFT JOIN ext_empreendimento_estagio ee ON (e.idempreendimento = ee.idempreendimento AND idestagioobra = 14 )
				  LEFT JOIN ex_comodo_empreendimento ce ON (e.idempreendimento = ce.idempreendimento AND ce.idcomodo = 1)
				  WHERE e.ativo = 'S' ";
					//alterar para iniciar com o idconteudo

		if ($this->id) {
			$sql .= " AND e.idempreendimento   = '".$this->id."' ";
			$sqlconta = "UPDATE  ext_empreendimentos SET lido = lido+1 WHERE  idempreendimento = '".$this->id."' LIMIT 1" ;
			mysql_query($sqlconta);
		}

		if (!empty($this->in)) {
			$sql .= ' AND ' . $this->in; 
		}

		if (!empty($this->slug)){$sql .= ' AND e.slug = "' . mysql_real_escape_string($this->slug) . '" ';}

		if (!empty($this->idcidadeIndex)){$sql .= " AND e.idcidade     IN (".implode(",",$this->idcidadeIndex).") ";}
		if (!empty($this->idcidade)){$sql .= " AND e.idcidade     = ".$this->idcidade." ";}
		if (!empty($this->idestado)){$sql .= " AND e.idestado     = ".$this->idestado." ";}
		if (!empty($this->idsite))     {$sql .= " AND c.idsite    = ".$this->idsite." ";}
		if (!empty($this->idcategoria)){$sql .= " AND ec.idcategoria IN (".$this->idcategoria.") ";}
		if (!empty($this->idtipoexclui)){$sql .= " AND e.andamento <> '".$this->idtipoexclui."' ";}
		if (!empty($this->idexclui)){$sql .= " AND e.idempreendimento <> '".$this->idexclui."' ";}
		if(!empty($this->idtipo)) {$sql .= " AND e.tipo           IN  (".implode(",",$this->idtipo).") ";}
		if(!empty($this->iddormitorio)) {$sql .= " AND ce.quantidade  IN (".implode(",",$this->iddormitorio).")";}
		if(!empty($this->idestagio)) {$sql .= " AND e.andamento   = '".$this->idestagio."' ";}
		$sql .= " GROUP BY e.idempreendimento ";
		//$sql .= " AND DATE_FORMAT( c.data_entrada,  '%Y-%m-%d' ) <=  DATE_FORMAT( NOW() ,  '%Y-%m-%d' )  AND (DATE_FORMAT( c.data_saida,  '%Y-%m-%d' ) <=  DATE_FORMAT( NOW() ,  '%Y-%m-%d' ) OR ISNULL(c.data_saida)) ";

		if($this->ordem_campo){ $sql .= " ORDER BY ".$this->ordem_campo." ".$this->ordem;}

		$query = mysql_query($sql);


		$num = mysql_num_rows($query);
		$this->paginas = ceil($num/$this->limite);

		if ($this->inicio == -1)
			$this->inicio = 0;
		else
			$this->inicio = ($this->pagina - 1) * $this->limite;

		if($this->limite) $sql .= " LIMIT ".$this->inicio.", ".$this->limite . "";

		$query = mysql_query($sql);

		$sqlCount = str_replace("*", "COUNT(*) AS total", $sql);
		$query2 = mysql_query($sqlCount);

		$total = mysql_fetch_assoc($query2);
		$this->total = $total["total"];


		if(mysql_num_rows($query) == 0){
			return false;
		} else {
			while ($linha = mysql_fetch_assoc($query)) {
				$this->retorno[] = $linha;
			}

			return $this->retorno;
		}

	}

	function RetornaEstagioObra ($idempreendimento){
		$retorno = array();
		$sql = "SELECT e.nome,e.idestagioobra,eo.percentual
			FROM ext_estagio_obra e
			LEFT JOIN ext_empreendimento_estagio eo ON (eo.idestagioobra = e.idestagioobra)
			WHERE idempreendimento = ".$idempreendimento." AND ativo = 'S' AND eo.percentual IS NOT NULL
			ORDER BY ordem ASC";
		$query = mysql_query($sql);

		while($linha = mysql_fetch_assoc($query)){
				$retorno[] = $linha;
		}
		return $retorno;
	}

	function retornaGaleriaExte($idconteudo,$idcategoria = NULL,$fotos = false){
		if ($idconteudo) {
			$sql = "SELECT ".$this->campos."
					FROM galerias ga
					INNER JOIN galerias_fotos g ON ( ga.idgaleria = g.idgaleria )
					INNER JOIN ext_empreendimentos_galerias cg ON ( g.idgaleria = cg.idgaleria )
					INNER JOIN galerias_categorias gc ON (gc.idgaleria = cg.idgaleria)
					INNER JOIN categorias cat ON (cat.idcategoria = gc.idcategoria)
					WHERE cg.idempreendimento = $idconteudo AND g.ativo = 'S'";
			if(!empty($this->idgaleria)){
				$sql .= " ga.idgaleria = ".$this->idgaleria." ";
			}
			if($idcategoria){
				$sql .= " AND gc.idcategoria = ".$idcategoria." ";
			}
			if ($idconteudo) {
				$sql .=" GROUP BY ga.idgaleria";
				}
			if($this->ordem_campo){ $sql .= " ORDER BY ".$this->ordem_campo." ".$this->ordem;}
			$query = mysql_query($sql);
			$cont = 0;

			$retorno = array();

			while ($linha = mysql_fetch_assoc($query)) {
				$sqlf = "SELECT idgaleria,legenda,descricao,nome FROM galerias_fotos WHERE idgaleria = ".$linha["idgaleria"]." AND ativo = 'S' ";
				$sqlf .=' ORDER BY ordem ASC ';
				if($this->limite) $sqlf .= " LIMIT 0, ".$this->limite . "";

				// echo '<!--'.$sqlf.'-->';
				if($_GET['w'] == 1){
					echo "<pre>",print_r($sql,1),"</pre>";exit;
				}
				$queryf = mysql_query($sqlf);
				unset($f);
				while($linha_foto = mysql_fetch_assoc($queryf)){
					$f[] = $linha_foto;
				}

				$linha["fotos"] = $f;
				$retorno[$cont] = $linha;
				$cont++;
			}
			return $retorno;
		}
	}

	function RetornaMesAnoFotos($idgaleria){
		if($idgaleria){
		$sql = "SELECT Month(data_entrada) as mes, Day(data_entrada) as day,Year(data_entrada) as ano, idgaleria
					FROM galerias
					WHERE `idgaleria` = {$idgaleria} AND ativo = 'S'
					ORDER BY mes ASC";

		$query = mysql_query($sql);
		while ($linha = mysql_fetch_assoc($query)) {
			if($linha["mes"] < 10){
				$linha["mes"] = "0".$linha["mes"];
			}
			$retorno[] = $linha;
		}
		}
		return $retorno;
	}
}

?>
