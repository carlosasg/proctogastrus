<?php
    class Lead
    {
        private $urlConexao = null;
        private $login      = null;
        private $token      = null;
        private $post       = null;
        private $conexao    = null;
        
        public function __construct($urlConexao, $login, $token)
        {
            $this->urlConexao   = $urlConexao;
            $this->login        = $login;
            $this->token        = $token;

            $this->post         = array(
                "acao"                  => "salvar_editar",
                "permitir_alteracao"    => "true"
            );
        }

        public function getPost()
        {
            return $this->post;
        }

        public function getHttpHeader()
        {
            return array(
                'email: ' . $this->login,
                'token: ' . $this->token,
                'Content-Type: application/json',
                'Content-Length: ' . strlen($this->post)
            );   
        }

        public function enviar($post)
        {
            $this->post = json_encode(array_merge($this->post, $post));
            $this->conexao = curl_init($this->urlConexao);

            curl_setopt($this->conexao, CURLOPT_URL, $this->urlConexao);
			curl_setopt($this->conexao, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($this->conexao, CURLOPT_POST, true);
			curl_setopt($this->conexao, CURLOPT_POSTFIELDS, $this->post);
			curl_setopt($this->conexao, CURLOPT_HTTPHEADER, $this->getHttpHeader());

            $result = curl_exec($this->conexao);
            return json_decode($result);
        }
    }
    
?>